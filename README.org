#+TITLE: lua-ugui
#+AUTHOR: DVLS
#+OPTIONS: toc:nil

* lua-ugui

  Pure Lua port of Achim Döbler's µGUI - generic GUI module for embedded 
  systems.

  Source code available at: https://gitlab.com/dvls/lua-ugui 

** What is lua-ugui

   lua-ugui is pure Lua graphic module mainly aimed at embedded systems. It is
   a port of the great C library developed by Achim Döbler: [[https://embeddedlightning.com/ugui/][µGUI]].

   The module should be platform-independent and it can be used with any
   system capable of displaying graphics and running a Lua interpreter.

   The original goal of the project was to provide a [[https://www.seeedstudio.com/RePhone-Kit-Create-p-2552.html][RePhone Kit Create]] 
   running a Lua 5.1 interpreter based on LoBo's
   [[https://github.com/loboris/RePhone_on_Linux][RePhone development on Linux]] project with a graphical user toolkit.
   If interested more information about it can be found at the end of this
   document.

   The Lua port has a few extra supporting functions (e.g. convertion
   between 24-bit a 16-bit RGB colors), also some effort has been made to
   make the implementation more "Lua-like" and less "C-like", which may 
   (or may not) improve execution time. For instance, C-structures have been
   replaced by Lua tables.

** License

   Copyright (C) 2020 Daniel Vicente Lühr Sierra

   This program is distributed under the GNU General Public License
   version 3 or later. See LICENSE file for more details.

** Additional credits / Acknowledgements

   - The original µGUI library is Copyright 2015 by Achim Döbler.
   - The fonts included in the original µGUI were sourced from [[https://www.mikrocontroller.net/topic/54860][mikrocontroller.net]] by [[https://www.mikrocontroller.net/user/show/benedikt][Benedikt K.]]
   - The predefined colors in the original µGUI were obtained from [[https://www.rapidtables.com/web/color/RGB_Color.html][RapidTables.com - RGB Color Codes Chart]].
   - lua-ugui has been tested on a GNU/Linux system using the [[http://tekui.neoscientists.org/][tekUI]] module, Copyright (C) 2008 - 2015 Timm S. Müller, Franciska Schulze, Tobias Schwinger
   - lua-ugui has been tested on a RePhone Kit Create using
     LoBo's  [[https://github.com/loboris/RePhone_on_Linux][RePhone development on Linux]].
   - Didericis' [[https://github.com/Didericis/png-lua][PNGLua]] has been used to load png images for testing image related functions.
   - The following images were used:
   - [[https://www.lua.org/images/][Lua Logo]] Copyright © 1998 [[http://www.lua.org/][Lua.org]]. Graphic design by Alexandre Nakonechnyj. 
   - Flag of Valdivia (the beautiful city where I live) [[https://commons.wikimedia.org/w/index.php?title=File:Flag_of_Valdivia,_Chile.svg&oldid=402690903][File:Flag of Valdivia, Chile.svg]], by Wikimedia Commons contributors (last visited September 7, 2020). 

** Installation

   Currently the module has no sophisticated packaging method.
   Just drop the main module file, =ugui.lua= for the standard
   implementation or =uguic.lua= for the compact version,
   into your project directory, 
   well as any font file =font*.lua= required.

** Usage
*** standard version (=ugui.lua=)
    To use it, the module needs to be loaded. Also, load required font(s).

    #+BEGIN_SRC lua
    ugui = require "ugui"
    font_8x8 = require "ugui-font8x8"
    #+END_SRC
    
    Then the main "native" drawing function, to paint a pixel, should be 
    defined and the GUI initialized:
    

    #+BEGIN_SRC lua
    function nativeDraw(x,y,c)
      -- code to paint pixel (x,y) with color c
      -- ...
    end
    mainGUI = {}
    ugui.UG_Init(mainGUI, nativeDraw, xSize, ySize)
    #+END_SRC


    For a more complete example check the files =demo-ugui-tekui.lua= and
    =demo-ugui-rephone.lua=. Also check the [[https://dvls.gitlab.io/lua-ugui/][manual]] generated with ldoc and the
    original µGUI manual.

*** compact version (=uguic.lua=)

    Just like the standard implementation, load the module and required font(s).

    #+BEGIN_SRC lua
    uguic = require("uguic")
    font_8x12 = require "ugui-font8x12"
    #+END_SRC

    Then, define the main "native" drawing function to paint pixels, and
    initialize the GUI:

    #+BEGIN_SRC lua
    function nativeDraw(x,y,c)
      -- code to paint pixel (x,y) with color c
      -- ...
    end
    mainGUI = {}
    uguic.UG_GUI("init", {g=mainGUI, p=nativeDraw, x=xSize, y=ySize})
    #+END_SRC

    Check the demo files =demo-uguic-tekui.lua= and =demo-uguic-rephone.lua=
    for more detailed examples. Also check the [[https://dvls.gitlab.io/lua-ugui/][manual]] generated with ldoc and the
    original µGUI manual.

** Examples and screenshots

   During development, the module has been tested using the tekUI module.
   The "target" platform is a RePhone running Lua 5.1

   (note: if you are reading this =README= file from the ldoc generated
   documentation, the screenshots are not yet visible. Please, check
   the source code's git repository for now).

*** lua-ugui standard (=ugui.lua=) tekUI Initialization code

    Excerpt from =demo-ugui-tekui.lua=.

    #+BEGIN_SRC lua
      -- load lua-ugui
      ugui = require "ugui"
      -- load tekUI
      ui = require "tek.ui"

      -- load fonts
      font_4x6 = require "ugui-font4x6"
      font_12x16 = require "ugui-font12x16"
      font_8x8 = require "ugui-font8x8"

      -- setup tekUI screen
      visual = require "tek.lib.visual"
      v = visual.open { Title="test", Width = 240, Height=240,
			MinWidth=240, MinHeight=240,
			MaxWidth=240, MaxHeight=240 }
      pen = v:allocPen(0,255,255,255)
      v:drawRect(0,0,239,239,pen)
      v:fillRect(0,0,239,239,pen)

      -- define native function to paint pixels using tekUI's primitive
      function upsf(x,y,c)
	 local red,green,blue = ugui.rgb32split(c)
	 local pen = v:allocPen(0,red,green,blue)
	 v:drawPoint(x,y,pen)
      end

      -- define native accelerated drawing method
      local function tek_drawline(x1, y1, x2, y2, c)
	 local red, green, blue = ugui.rgb32split(c)
	 local pen = v:allocPen(0,red,green,blue)
	 v:drawLine(x1, y1, x2, y2, pen)
	 return true
      end   

      -- Initialize GUI
      mainGUI = {}
      ugui.UG_Init(mainGUI, upsf,240,240)

      -- Register and enable accelerated method
      ugui.UG_DriverRegister("DRIVER_DRAW_LINE", tek_drawline)

      -- tekUI input processing
      -- react to mouse
      v:setInput(0x0400)
      function getInput()
	 visual.wait()
	 local msg = visual.getMsg()
	 if msg then
	    local msg_code, mxw, myw, mxs, mys = msg[3], msg[4], msg[5], msg[11], msg[12]
	    ugui.UG_TouchUpdate(mxw, myw, 1)
	    ugui.UG_Update()
	    ugui.UG_TouchUpdate(-1, -1, 0)
	    ugui.UG_Update()
	 end
      end


    #+END_SRC


    

*** lua-ugui standard (=ugui.lua=) LuaRephone Initialization code

    The whole lua-ugui standard module does not fit into LuaRephone. 
    Only the basic
    graphic primitives (classic functions) have been tested.

    The following is an Exerpt from   =demo-ugui-rephone.lua=.

    #+BEGIN_SRC lua
      -- load lua-ugui
      ugui = require "ugui"
      -- load fonts
      font_4x6 = require "ugui-font4x6"
      font_12x16 = require "ugui-font12x16"
      font_8x8 = require "ugui-font8x8"

      -- Initialize GUI
      mainGUI = {}
      ugui.UG_Init(mainGUI, upsf,240,240)

      -- initialize RePhone's LCD
      res = lcd.init(8)
      lcd.clear()

      -- define native function to paint pixels using RePhone's primitive
      function upsf(x,y,c)
	 local rgb16 = ugui.rgb32to16(c)
	 lcd.putpixel(x,y,rgb16)
      end

      -- define native accelerated drawing method
      function rephone_drawline(x1, y1, x2, y2, c)
	 local rgb16 = ugui.rgb32to16(c)
	 lcd.line(x1, y1, x2, y2, rgb16)
	 return true
      end

      -- Register and enable accelerated method
      ugui.UG_DriverRegister("DRIVER_DRAW_LINE", rephone_drawline)

    #+END_SRC



*** lua-ugui standard (=ugui.lua=) Predefined colors

    #+BEGIN_SRC lua
      ugui.UG_FillScreen(ugui.C.DARK_GRAY)
      local row = 0
      local col = 0
      for k,v in pairs(ugui.C) do
	 ugui.UG_FillFrame(col,row,col+20,row+20,v)
	 col = col + 20
	 if col >= 240 then
	    col = 0
	    row = row + 20
	    if row >= 240 then return end
	 end
      end
    #+END_SRC

    #+CAPTION: tekUI: Colors predefined in the library.
    #+NAME: screenshot:tekui:defined:colors
    [[./images/screenshots-v0.1/screenshot_tekui_showAllColors.png]]
    /tekUI: Colors predefined in the library./

    Note that the LuaRephone implementation uses RGB565 16-bit colorspace,
    while µGUI predefined colors are RGB888 24-bit.

    #+CAPTION: RePhone: Colors predefined in the library.
    #+NAME: screenshot:tekui:defined:colors
    [[./images/screenshots-v0.1/screenshot_rephone_showAllColors.png]]
    /RePhone: Colors predefined in the library./

*** lua-ugui standard (=ugui.lua=) Showcase of Classic functions

    #+BEGIN_SRC lua 
      -- Clear Screen
      ugui.UG_FillScreen(ugui.C.WHITE)
      -- Draw functions
      ugui.UG_DrawMesh(10,10,80,60,ugui.C.FOREST_GREEN)
      ugui.UG_DrawFrame(85,10,155,60,ugui.C.FOREST_GREEN)
      ugui.UG_DrawRoundFrame(160,10,230,60,10,ugui.C.FOREST_GREEN)
      ugui.UG_DrawCircle(45,90,25,ugui.C.FOREST_GREEN)
      ugui.UG_DrawArc(85,65,20,0x80,ugui.C.FOREST_GREEN)
      ugui.UG_DrawArc(85,65,20,0x40,ugui.C.RED)
      ugui.UG_DrawArc(155,65,20,0x20,ugui.C.FOREST_GREEN)
      ugui.UG_DrawArc(155,65,20,0x10,ugui.C.RED)
      ugui.UG_DrawArc(85,115,20,0x02,ugui.C.FOREST_GREEN)
      ugui.UG_DrawArc(85,115,20,0x01,ugui.C.RED)
      ugui.UG_DrawArc(155,115,20,0x08,ugui.C.FOREST_GREEN)
      ugui.UG_DrawArc(155,115,20,0x04,ugui.C.RED)
      ugui.UG_DrawLine(160,65,230,100,ugui.C.FOREST_GREEN)
      ugui.UG_DrawLine(160,115,200,65,ugui.C.RED)
      -- Fill functions
      ugui.UG_FillCircle(45,145,25,ugui.C.MEDIUM_PURPLE)
      ugui.UG_FillFrame(85,120,155,170,ugui.C.SEA_GREEN)
      ugui.UG_FillRoundFrame(160,120,230,170,10,ugui.C.DARK_ORANGE)
      -- Text functions
      ugui.UG_FontSelect(font_8x8)
      ugui.UG_SetForecolor(ugui.C.BLACK)
      -- transparent text background (new feature)
      ugui.UG_SetBackcolor(nil)
      ugui.UG_PutString(20,20,"Mesh")
      ugui.UG_PutString(90,20,"Frame")
      ugui.UG_PutString(170,20,"Round\nFrame")
      ugui.UG_PutString(30,75,"Cir\ncle")
      ugui.UG_PutString(105,85,"Arcs")
      ugui.UG_PutString(170,100,"Lines")
      ugui.UG_SetForecolor(ugui.C.YELLOW)
      ugui.UG_PutString(30,130,"Fill\ned\ncir\ncle")
      ugui.UG_PutString(90,125,"Filled\nframe")
      ugui.UG_PutString(170,130,"Filled\nround\nframe")
      -- Console
      ugui.UG_FontSelect(font_12x16)
      ugui.UG_ConsoleSetBackcolor(ugui.C.GAINSBORO)
      ugui.UG_ConsoleSetForecolor ( 0xDB4B4B )
      ugui.UG_ConsoleSetArea (10,175,230,230)
      ugui.UG_ConsolePutString("Console Area\n------------\n")
      ugui.UG_ConsolePutString("Screen: "..ugui.UG_GetXDim())
      ugui.UG_ConsolePutString("x"..ugui.UG_GetYDim())
    #+END_SRC


    #+CAPTION: tekUI: Showcase of classic functions (graphic primitives) v0.1
    [[./images/screenshots-v0.1/screenshot_tekui_classicFunctions.png]]
    /tekUI: Showcase of classic functions (graphic primitives) v0.1/

    #+CAPTION: tekUI: Showcase of classic functions (graphic primitives) v1.0
    [[./images/screenshots-v1.0/screenshot-ugui-tekui-classic.png]]
    /tekUI: Showcase of classic functions (graphic primitives) v1.0/

    #+CAPTION: RePhone: Showcase of classic functions (graphic primitives)
    [[./images/screenshots-v0.1/screenshot_rephone_classicFunctions.png]]
    /RePhone: Showcase of classic functions (graphic primitives)/


*** lua-ugui standard (=ugui.lua=) Window example

    #+begin_src lua
      mainWindow = {}
      local function mainWindow_cb (msg)
	 print("Msg type: ", msg.type)
	 print("Msg id: ", msg.id)
	 print("Msg sub_id", msg.sub_id)
      end

      ugui.UG_WindowCreate(mainWindow, mainWindow_cb)
      -- Window title setup
      ugui.UG_WindowSetTitleHeight(mainWindow, 20)
      ugui.UG_WindowSetTitleTextFont(mainWindow, font_12x16)
      ugui.UG_WindowSetTitleText(mainWindow, "Window title")
      ugui.UG_WindowSetTitleTextAlignment(mainWindow, "ALIGN_CENTER")
      -- Button widget
      button = {}
      ugui.UG_ButtonCreate(mainWindow, button, "btn01", 10,50,100,100)
      ugui.UG_ButtonSetFont(mainWindow, "btn01", font_12x16)
      ugui.UG_ButtonSetText(mainWindow, "btn01", "Ok")
      ugui.UG_ButtonShow(mainWindow, "btn01")
      -- Textbox widget
      tb = {}
      ugui.UG_TextboxCreate(mainWindow, tb, "txt01",	10, 110, 230,210)
      ugui.UG_TextboxSetFont(mainWindow, "txt01", font_12x16)
      ugui.UG_TextboxSetText(mainWindow, "txt01", "Textbox text")
      ugui.UG_TextboxSetForeColor(mainWindow, "txt01", ugui.C.BLUE)
      ugui.UG_TextboxSetAlignment(mainWindow, "txt01", "ALIGN_BOTTOM_RIGHT")
      ugui.UG_WindowShow(mainWindow)
      ugui.UG_Update()
    #+end_src

    #+CAPTION: tekUI: Window example v0.1
    [[./images/screenshots-v0.1/screenshot_tekui_windowCreation.png]]
    /tekUI: Window example v0.1/

    #+CAPTION: tekUI: Window example v1.0
    [[./images/screenshots-v1.0/screenshot-ugui-tekui-window.png]]
    /tekUI: Window example v1.0/



*** lui-ugui compact (=uguic.lua=) tekUI Initialization code

    Excerpt from =demo-uguic-tekui.lua=
    #+BEGIN_SRC lua
      -- load lua-ugui
      uguic = require("uguic")

      -- load tekUI
      ui = require "tek.ui"
      visual = require "tek.lib.visual"


      -- load fonts
      font_4x6 = require "ugui-font4x6"
      font_6x10 = require "ugui-font6x10"
      font_8x8 = require "ugui-font8x8"
      font_8x12 = require "ugui-font8x12"
      font_12x16 = require "ugui-font12x16"

      -- setup tekUI screen
      v_width = 240
      v_height = 240
      v = visual.open { Title="uguic.lua demo", Width = v_width, Height=v_height,
			MinWidth=v_width, MinHeight=v_height,
			MaxWidth=v_width, MaxHeight=v_height }

      -- define native function to paint pixels using tekUI's primitive
      function upsf(x,y,c)
	 local rgb = uguic.splitIntegerBits(c,{8,8,8})
	 local pen = v:allocPen(0,rgb[1],rgb[2],rgb[3])
	 v:drawPoint(x,y,pen)
      end

      -- define native accelerated drawing method
      local function tek_drawline(params)
	 local x1, y1 = params.x1, params.y1
	 local x2, y2 = params.x2, params.y2
	 local c = params.c
	 local rgb = uguic.splitIntegerBits(c,{8,8,8})
	 local pen = v:allocPen(0,rgb[1],rgb[2],rgb[3])
	 v:drawLine(x1, y1, x2, y2, pen)
	 return true
      end

      -- Initialize GUI
      mainGUI = {}
      uguic.UG_GUI("init", {g=mainGUI, p=upsf, x=240, y=240})

      -- Register and enable accelerated method
      uguic.UG_Driver("register",
		      {driver_type="DRAW_LINE", driver=tek_drawline})

      -- tekui input processing
      -- react to mouse
      v:setInput(0x0400)
      function getInput()
	 visual.wait()
	 local msg = visual.getMsg()
	 while msg do
	    print("msg available")
	    local msg_type, msg_code, mxw, myw, mxs, mys = msg[2], msg[3], msg[4], msg[5], msg[11], msg[12]
	    print("msg:", msg_type, msg_code, mxw, myw, mxs, mys)
	    if msg_type == 1024 then
	       -- MouseButton
	       print("msg is mousebutton")
	       if msg_code == 1 or msg_code == 4 or msg_code == 16 then
		  uguic.UG_TouchUpdate(mxw, myw, "PRESSED")
		  uguic.UG_Update()
	       elseif msg_code == 2 or msg_code == 8 or msg_code == 32 then
		  uguic.UG_TouchUpdate(-1, -1, "RELEASED")
		  uguic.UG_Update()
	       end
	    else
	       print("msg is something else")
	    end
	    msg = visual.getMsg()
	 end
      end
    #+END_SRC

*** lui-ugui compact (=uguic.lua=) LuaRephone Initialization code

    lua-ugui compact does fit into LuaRePhone. Therefore, all 
    features can be tested.

    The following code is an excerpt from =demo-uguic-rephone.lua=

    #+BEGIN_SRC lua
      -- load lua-ugui
      uguic = require ("uguic")

      -- load fonts
      -- font_4x6 = require "ugui-font4x6"
      font_8x8 = require "ugui-font8x8"
      -- font_6x10 = require "ugui-font6x10"
      font_8x12 = require "ugui-font8x12"
      -- font_12x16 = require "ugui-font12x16"

      -- initialize RePhone LCD screen
      res = lcd.init(8)
      lcd.clear()

      -- define native function to paint pixels using RePhone's primitive
      function upsf(x,y,c)
	 local rgb16 = uguic.rgb32to16(c)
	 lcd.putpixel(x,y,rgb16)
      end

      -- define native accelerated drawing method
      function rephone_drawline(params)
	 local x1, y1 = params.x1, params.y1
	 local x2, y2 = params.x2, params.y2
	 local c = params.c
	 local rgb16 = uguic.rgb32to16(c)
	 lcd.line(x1, y1, x2, y2, rgb16)
	 return true
      end

      -- Initialize GUI
      mainGUI = {}
      uguic.UG_GUI("init", {g=mainGUI, p=upsf, x=240, y=240})

      -- Register and enable accelerated method
      uguic.UG_Driver("register", {driver_type="DRAW_LINE",
				   driver=rephone_drawline})
    #+END_SRC


*** lui-ugui compact (=uguic.lua=) Showcase of Classic functions

    #+BEGIN_SRC lua
      function showClassic(bmp)
	 -- bmp is a bitmap object ({p, width, height, bpp, colors})
   
	 -- Clear Screen
	 uguic.UG_Fill("Screen", {c=uguic.C.WHITE})
   
	 -- Draw functions
	 uguic.UG_Draw("Mesh", {x1=10, y1=10, x2=80,y2=60,
				c=uguic.C.FOREST_GREEN})
	 uguic.UG_Draw("Frame", {x1=85, y1=10, x2=155, y2=60,
				 c=uguic.C.FOREST_GREEN})
	 uguic.UG_Draw("RoundFrame", {x1=160, y1=10, x2=230, y2=60,r=10,
				      c=uguic.C.FOREST_GREEN})
	 uguic.UG_Draw("Circle", {x0=35, y0=90, r=25,
				  c=uguic.C.FOREST_GREEN})
	 uguic.UG_Draw("Arc", {x0=75, y0=65, r=20, s=0x80,
			       c=uguic.C.FOREST_GREEN})
	 uguic.UG_Draw("Arc", {x0=75, y0=65, r=20, s=0x40,
			       c=uguic.C.RED})
	 uguic.UG_Draw("Arc", {x0=125, y0=65, r=20, s=0x20,
			       c=uguic.C.FOREST_GREEN})
	 uguic.UG_Draw("Arc", {x0=125, y0=65, r=20, s=0x10,
			       c=uguic.C.RED})
	 uguic.UG_Draw("Arc", {x0=75, y0=115, r=20, s=0x02,
			       c=uguic.C.FOREST_GREEN})
	 uguic.UG_Draw("Arc", {x0=75, y0=115, r=20, s=0x01,
			       c=uguic.C.RED})
	 uguic.UG_Draw("Arc", {x0=125, y0=115, r=20, s=0x08,
			       c=uguic.C.FOREST_GREEN})
	 uguic.UG_Draw("Arc", {x0=125, y0=115, r=20, s=0x04,
			       c=uguic.C.RED})
	 uguic.UG_Draw("Line", {x1=130, y1=65, x2=185, y2=100,
				c=uguic.C.FOREST_GREEN})
	 uguic.UG_Draw("Line", {x1=130, y1=115, x2=170, y2=65,
				c=uguic.C.RED})
	 uguic.UG_Draw("Triangle", {x0=180, y0=65,
				    x1=230, y1=75,
				    x2=195, y2=115,
				    c=uguic.C.FOREST_GREEN})
	 if bmp then
	    uguic.UG_Draw("BMP", {x0=10, y0=175, bmp=bmp})
	    uguic.UG_Draw("Frame", {x1=10, y1=175,
				    x2=10+bmp.width, y2=175+bmp.height,
				    c=uguic.C.BLACK})
	 end
	 -- Fill functions
	 uguic.UG_Fill("Circle", {x0=45, y0=145, r=25,
		      c=uguic.C.MEDIUM_PURPLE})
	 uguic.UG_Fill("Frame", {x1=85, y1=120, x2=155, y2=170,
				 c=uguic.C.SEA_GREEN})
	 uguic.UG_Fill("RoundFrame", {x1=160, y1=120, x2=230, y2=170, r=10,
				      c=uguic.C.DARK_ORANGE})
   
	 -- Text functions
	 uguic.UG_Text("selectFont", {font=font_8x8})
	 uguic.UG_Text("setForecolor", {c=uguic.C.BLACK})
	 -- uguic.UG_Text("SetBackcolor", {c=uguic.C.WHITE})
	 -- transparent text background ", {new feature})
	 uguic.UG_Text("setBackcolor", {nil})
	 uguic.UG_Text("putString", {x=20, y=20, str="Mesh"})
	 uguic.UG_Text("putString", {x=90, y=20, str="Frame"})
	 uguic.UG_Text("putString", {x=170, y=20, str="Round\nFrame"})
	 uguic.UG_Text("putString", {x=20, y=75, str="Cir\ncle"})
	 uguic.UG_Text("putString", {x=85, y=85, str="Arcs"})
	 uguic.UG_Text("putString", {x=140, y=100, str="Lines"})
	 uguic.UG_Text("putString", {x=185, y=70, str="Tri\nangle"})
	 if bmp then
	    uguic.UG_Text("putString", {x=15, y=2+175+bmp.height, str="BMP"}) end
	 uguic.UG_Text("setForecolor", {c=uguic.C.YELLOW})
	 uguic.UG_Text("putString", {x=30, y=130, str="Fill\ned\ncir\ncle"})
	 uguic.UG_Text("putString", {x=90, y=125, str="Filled\nframe"})
	 uguic.UG_Text("putString", {x=170, y=130, str="Filled\nround\nframe"})
	 uguic.UG_Text("setBackcolor", {c=uguic.C.WHITE})
   
	 -- Console
	 if bmp then
	    uguic.UG_Text("selectFont", {font=font_8x12})
	    console_xs = 90
	 else
	    uguic.UG_Text("selectFont", {font=font_8x12})
	    console_xs = 10
	 end
	 uguic.UG_Text("selectFont", {font=font_8x8})
	 uguic.UG_Console("setBackcolor", {c=uguic.C.GAINSBORO})
	 uguic.UG_Console("setForecolor ", {c=0xDB4B4B})
	 uguic.UG_Console("setArea", {xs=console_xs, ys=175, xe=230, ye=230})
	 uguic.UG_Console("putString", {str="Console Area\n------------\n"})
	 local guiDim = uguic.UG_GUI("getDim")
	 uguic.UG_Console("putString", {str="Screen: "..guiDim.x})
	 uguic.UG_Console("putString", {str="x"..guiDim.y})
      end
    #+END_SRC

    #+CAPTION: tekUI: Showcase of classic functions (graphic primitives) v1.0
    [[./images/screenshots-v1.0/screenshot-uguic-tekui-classic.png]]
    /tekUI: Showcase of classic functions (graphic primitives) v1.0/

    #+CAPTION: RePhone: Showcase of classic functions (graphic primitives) v1.0
    [[./images/screenshots-v1.0/screenshot-uguic-rephone-classic.png]]
    /RePhone: Showcase of classic functions (graphic primitives) v1.0/


*** lui-ugui compact (=uguic.lua=) Window example

    #+BEGIN_SRC lua
      function showWindow(bmp)
	 print("FillScreen:", uguic.UG_Fill("Screen", {c=uguic.C.WHITE}))
	 mainWindow = {}
	 local function mainWindow_cb (msg)
	    print("Msg type: ", msg.msg_type)
	    print("Msg src type: ", msg.src_type)
	    print("Msg src id", msg.src_id)
	 end
	 -- create window and configure its title
	 print("Create:",
	       uguic.UG_Window(mainWindow, "create", {cb=mainWindow_cb}))
	 print("TitleHeight:",
	       uguic.UG_WindowSet(mainWindow, "TitleHeight", 16))
	 print("TitleFont:",
	       uguic.UG_WindowSet(mainWindow, "TitleTextFont", font_8x12))
	 print("TitleText:",
	       uguic.UG_WindowSet(mainWindow, "TitleText", "Window title"))
	 -- Button widget
	 button_id = "btn01"
	 print("ButtonCreate:",
	       uguic.UG_Button(mainWindow, "create", button_id,
			       {xs=10, ys=50, xe=100, ye=100}))
	 print("ButtonFont:",
	       uguic.UG_ButtonSet(mainWindow, button_id, "Font", font_8x12))
	 print("ButtonText:",
	       uguic.UG_ButtonSet(mainWindow, button_id, "Text", "Ok"))
	 -- Textbox widget
	 textbox_id = "txt01"
	 print("TextboxCreate:",
	       uguic.UG_Textbox(mainWindow, "create", textbox_id,
				{xs=10, ys=110, xe=230, ye=210}))
	 print("TextboxFont:",
	       uguic.UG_TextboxSet(mainWindow, textbox_id, "Font", font_8x12))
	 print("TextboxText:",
	       uguic.UG_TextboxSet(mainWindow, textbox_id, "Text", "Textbox text"))
	 print("TextboxForeColor:",
	       uguic.UG_TextboxSet(mainWindow, textbox_id,
				   "ForeColor", uguic.C.BLUE))
	 print("TextboxAlign:",
	       uguic.UG_TextboxSet(mainWindow, textbox_id,
				   "Alignment", {v="BOTTOM", h="RIGHT"}))
	 -- Image widget
	 if bmp then
	    image_id = "img01"
	    bmp_x = 230-bmp.width
	    bmp_y = 10
	    print("ImageCreate:",
		  uguic.UG_Image(mainWindow, "create", image_id,
				 {xs=bmp_x, ys=bmp_y, xe=bmp.width, ye=bmp.height}))
	    print("ImageBMP:",
		  uguic.UG_ImageSet(mainWindow, image_id, "BMP", bmp))
	 end
	 -- Show window and update screen
	 print("Show:", uguic.UG_Window(mainWindow, "show"))
	 print("(update)", uguic.UG_Update())
      end
    #+END_SRC


    #+CAPTION: tekUI: Showcase of classic functions (graphic primitives) v1.0
    [[./images/screenshots-v1.0/screenshot-uguic-tekui-window.png]]
    /tekUI: Showcase of classic functions (graphic primitives) v1.0/

    #+CAPTION: RePhone: Showcase of classic functions (graphic primitives) v1.0
    [[./images/screenshots-v1.0/screenshot-uguic-rephone-window.png]]
    /RePhone: Showcase of classic functions (graphic primitives) v1.0/


** ToDo

   - Documentation continuous improvement (it is never complete, ...)
   - More testing (and bugfixing)
   - More examples
   - More fonts
   - Extend the functionality (Add other functions, more accelerated/hardware drivers)
   - Optimize / improve the implementation (make more use of Lua language features)

** A little bit of history
*** (log date: 2020-08-23)
    I developed the first release of this module over a few weeks working on
    my spare time, during the 2020 Covid-19 worldwide crisis.

    During this time my "smart"-phone stopped working, which made me quite 
    happy, since I had been waiting for it to happen. I had previously decided 
    that I was going to stop using a cellphone when that moment came. 
  
    However, there was one problem. There are still a few companies which rely
    on sms for user authentication, so I needed some way to keep receiving
    those messages.

    I grabbed an old [[https://www.seeedstudio.com/RePhone-Kit-Create-p-2552.html][RePhone Kit Create]] I had acquired by supporting their 
    kickstart campaign but I had been unable to try due to lack of time (or a
    good excuse to spend time on it).

    The RePhone line of products seems to be discontinnued but it is still a
    great DIY, open source, IoT platform. It came with a very basic firmware,
    which allow it to use it as a simple phone, 
    but its full power and flexibility can be unleashed by writing your own
    firmware using either Eclipse IDE, Arduino IDE, Javascript or Lua.

    I discarded Eclipse IDE and Javascript, since I have never felt confortable
    using either.

    Using Arduino IDE was a good option, but it was too much of a hassle to 
    setup the RePhone target on my GNU/Linux development box.
    On the other hand, I have had learning Lua in my TODO list for a long time, 
    but I hadn't found a good project to jump into it, so this was it. 
    I used LoBo's [[https://github.com/loboris/RePhone_on_Linux][RePhone development on Linux]] project, and I got my 
    RePhone running a Lua 5.1 interpreter in a few minutes.
    And, although it provided primitive drawing functions and touchscreen
    functionality, building a complex GUI was going to be a major challenge.

    Therefore, I started looking for a Lua GUI module which I could use. 
    There were many options but most of them required recompiling the Lua 
    interpreter (since they were either partly written in C/C++ or wrappers to 
    a C/C++ graphic toolkit).

    Eventualy, I found Achim Döbler's [[https://embeddedlightning.com/ugui/][µGUI]]. It was written in C, but it is so
    simple, well coded, with a platform-independent design, and independent of 
    any other C library, that porting it to Lua would be a challenging but 
    feasible task.

    For testing the library in my development GNU/Linux system I used an 
    existing Lua GUI module, [[http://tekui.neoscientists.org/][tekUI]] which is also a great package. I didn't use
    it directly on the RePhone only because it was not a pure Lua 
    implementation.

    When I finished the first release, I found out that the Lua stack on the 
    RePhone cannot handle the full module. There might be a workaround, but
    this problem has set my new challenge which will be to write a more compact
    version of the module to accomodate LuaRephone's limited stack size.

    Finally, I just want to say that learning Lua through this project 
    has been really fun. If you are going to use this module, I wish you enjoy
    it as much as I have while writing it.

*** (log date: 2020-09-07)
    I have spent a couple of weeks developing the compact version of the module. 
    The compact version has some additions and improvements which I have not 
    ported back to the standard implementation (yet). 

    I have done preliminary tests of 
    the basic functionality and, although it is not bug free, I am happy enough 
    to make this release v1.0 of the module. I have already planed a few 
    improvements which would be part of v2.0, while the next point releases for
    the v1.x series will be just bug fixes found when testing it more 
    exhaustively.

    The compact version manage to load completely in RePhone's memory, but it
    is still too tight, and quite easily eats up all of it. Fonts and Images
    are the most memory hungry bits which I have detected. These will require
    some serious optimization to be really usable on the RePhone (or any 
    device with low memory available).

** Additional notes about lua-ugui on the RePhone                  :noexport:
*** Memory usage
   
    | Condition       | Used memory | Total memory | Available | C heap |    ?? |
    |-----------------+-------------+--------------+-----------+--------+-------|
    | Init            |       70636 |       577024 | 506388    |  65536 |  8192 |
    | uguic           |      269243 |       577024 | 307781    |  65536 | 20480 |
    | font8x8         |      137257 |       577024 | 439767    |  65536 | 20480 |
    | font6x10        |      148927 |       577024 | 428097    |  65536 | 20480 |
    | font8x12        |      161743 |       577024 | 415281    |  65536 | 20480 |
    | uguic + 3 fonts |      368394 |       577024 | 208630    |  65536 | 20480 |
    #+TBLFM: $4=$3-$2



