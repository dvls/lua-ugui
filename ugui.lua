#!/usr/bin/env lua

-- lua-ugui
-- Copyright (C) 2020  Daniel Vicente Lühr Sierra <dvls@trianguloaustral.cl>
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

--- µGUI port to lua.
-- Source code available at:
-- <a href="https://gitlab.com/dvls/lua-ugui">
-- https://gitlab.com/dvls/lua-ugui
-- </a>
-- @license GNU General Public License version 3 or later
-- <a href="https://www.gnu.org/licenses/gpl.html">
-- https://www.gnu.org/licenses/gpl.html
-- </a>
-- @copyright 2020
-- @release 1.2
-- @author Daniel Vicente Lühr Sierra 
-- <a href="mailto:dvls@trianguloaustral.cl">
-- &lt;dvls@trianguloaustral.cl&gt;
-- </a>
-- @module ugui

-- @todo SetStyle and GetStyle functions are not "symmetric" (parameters and results are not interchangeable)


-- ---------------------------------------------------------- --
-- µGUI COLORS                                                --
-- Source: http://www.rapidtables.com/web/color/RGB_Color.htm --
-- ---------------------------------------------------------- --

-- local C_MAROON                     = 0x800000
-- local C_DARK_RED                   = 0x8B0000
-- local C_BROWN                      = 0xA52A2A
-- local C_FIREBRICK                  = 0xB22222
-- local C_CRIMSON                    = 0xDC143C
-- local C_RED                        = 0xFF0000
-- local C_TOMATO                     = 0xFF6347
-- local C_CORAL                      = 0xFF7F50
-- local C_INDIAN_RED                 = 0xCD5C5C
-- local C_LIGHT_CORAL                = 0xF08080
-- local C_DARK_SALMON                = 0xE9967A
-- local C_SALMON                     = 0xFA8072
-- local C_LIGHT_SALMON               = 0xFFA07A
-- local C_ORANGE_RED                 = 0xFF4500
-- local C_DARK_ORANGE                = 0xFF8C00
-- local C_ORANGE                     = 0xFFA500
-- local C_GOLD                       = 0xFFD700
-- local C_DARK_GOLDEN_ROD            = 0xB8860B
-- local C_GOLDEN_ROD                 = 0xDAA520
-- local C_PALE_GOLDEN_ROD            = 0xEEE8AA
-- local C_DARK_KHAKI                 = 0xBDB76B
-- local C_KHAKI                      = 0xF0E68C
-- local C_OLIVE                      = 0x808000
-- local C_YELLOW                     = 0xFFFF00
-- local C_YELLOW_GREEN               = 0x9ACD32
-- local C_DARK_OLIVE_GREEN           = 0x556B2F
-- local C_OLIVE_DRAB                 = 0x6B8E23
-- local C_LAWN_GREEN                 = 0x7CFC00
-- local C_CHART_REUSE                = 0x7FFF00
-- local C_GREEN_YELLOW               = 0xADFF2F
-- local C_DARK_GREEN                 = 0x006400
-- local C_GREEN                      = 0x00FF00
-- local C_FOREST_GREEN               = 0x228B22
-- local C_LIME                       = 0x00FF00
-- local C_LIME_GREEN                 = 0x32CD32
-- local C_LIGHT_GREEN                = 0x90EE90
-- local C_PALE_GREEN                 = 0x98FB98
-- local C_DARK_SEA_GREEN             = 0x8FBC8F
-- local C_MEDIUM_SPRING_GREEN        = 0x00FA9A
-- local C_SPRING_GREEN               = 0x00FF7F
-- local C_SEA_GREEN                  = 0x2E8B57
-- local C_MEDIUM_AQUA_MARINE         = 0x66CDAA
-- local C_MEDIUM_SEA_GREEN           = 0x3CB371
-- local C_LIGHT_SEA_GREEN            = 0x20B2AA
-- local C_DARK_SLATE_GRAY            = 0x2F4F4F
-- local C_TEAL                       = 0x008080
-- local C_DARK_CYAN                  = 0x008B8B
-- local C_AQUA                       = 0x00FFFF
-- local C_CYAN                       = 0x00FFFF
-- local C_LIGHT_CYAN                 = 0xE0FFFF
-- local C_DARK_TURQUOISE             = 0x00CED1
-- local C_TURQUOISE                  = 0x40E0D0
-- local C_MEDIUM_TURQUOISE           = 0x48D1CC
-- local C_PALE_TURQUOISE             = 0xAFEEEE
-- local C_AQUA_MARINE                = 0x7FFFD4
-- local C_POWDER_BLUE                = 0xB0E0E6
-- local C_CADET_BLUE                 = 0x5F9EA0
-- local C_STEEL_BLUE                 = 0x4682B4
-- local C_CORN_FLOWER_BLUE           = 0x6495ED
-- local C_DEEP_SKY_BLUE              = 0x00BFFF
-- local C_DODGER_BLUE                = 0x1E90FF
-- local C_LIGHT_BLUE                 = 0xADD8E6
-- local C_SKY_BLUE                   = 0x87CEEB
-- local C_LIGHT_SKY_BLUE             = 0x87CEFA
-- local C_MIDNIGHT_BLUE              = 0x191970
-- local C_NAVY                       = 0x000080
-- local C_DARK_BLUE                  = 0x00008B
-- local C_MEDIUM_BLUE                = 0x0000CD
-- local C_BLUE                       = 0x0000FF
-- local C_ROYAL_BLUE                 = 0x4169E1
-- local C_BLUE_VIOLET                = 0x8A2BE2
-- local C_INDIGO                     = 0x4B0082
-- local C_DARK_SLATE_BLUE            = 0x483D8B
-- local C_SLATE_BLUE                 = 0x6A5ACD
-- local C_MEDIUM_SLATE_BLUE          = 0x7B68EE
-- local C_MEDIUM_PURPLE              = 0x9370DB
-- local C_DARK_MAGENTA               = 0x8B008B
-- local C_DARK_VIOLET                = 0x9400D3
-- local C_DARK_ORCHID                = 0x9932CC
-- local C_MEDIUM_ORCHID              = 0xBA55D3
-- local C_PURPLE                     = 0x800080
-- local C_THISTLE                    = 0xD8BFD8
-- local C_PLUM                       = 0xDDA0DD
-- local C_VIOLET                     = 0xEE82EE
-- local C_MAGENTA                    = 0xFF00FF
-- local C_ORCHID                     = 0xDA70D6
-- local C_MEDIUM_VIOLET_RED          = 0xC71585
-- local C_PALE_VIOLET_RED            = 0xDB7093
-- local C_DEEP_PINK                  = 0xFF1493
-- local C_HOT_PINK                   = 0xFF69B4
-- local C_LIGHT_PINK                 = 0xFFB6C1
-- local C_PINK                       = 0xFFC0CB
-- local C_ANTIQUE_WHITE              = 0xFAEBD7
-- local C_BEIGE                      = 0xF5F5DC
-- local C_BISQUE                     = 0xFFE4C4
-- local C_BLANCHED_ALMOND            = 0xFFEBCD
-- local C_WHEAT                      = 0xF5DEB3
-- local C_CORN_SILK                  = 0xFFF8DC
-- local C_LEMON_CHIFFON              = 0xFFFACD
-- local C_LIGHT_GOLDEN_ROD_YELLOW    = 0xFAFAD2
-- local C_LIGHT_YELLOW               = 0xFFFFE0
-- local C_SADDLE_BROWN               = 0x8B4513
-- local C_SIENNA                     = 0xA0522D
-- local C_CHOCOLATE                  = 0xD2691E
-- local C_PERU                       = 0xCD853F
-- local C_SANDY_BROWN                = 0xF4A460
-- local C_BURLY_WOOD                 = 0xDEB887
-- local C_TAN                        = 0xD2B48C
-- local C_ROSY_BROWN                 = 0xBC8F8F
-- local C_MOCCASIN                   = 0xFFE4B5
-- local C_NAVAJO_WHITE               = 0xFFDEAD
-- local C_PEACH_PUFF                 = 0xFFDAB9
-- local C_MISTY_ROSE                 = 0xFFE4E1
-- local C_LAVENDER_BLUSH             = 0xFFF0F5
-- local C_LINEN                      = 0xFAF0E6
-- local C_OLD_LACE                   = 0xFDF5E6
-- local C_PAPAYA_WHIP                = 0xFFEFD5
-- local C_SEA_SHELL                  = 0xFFF5EE
-- local C_MINT_CREAM                 = 0xF5FFFA
-- local C_SLATE_GRAY                 = 0x708090
-- local C_LIGHT_SLATE_GRAY           = 0x778899
-- local C_LIGHT_STEEL_BLUE           = 0xB0C4DE
-- local C_LAVENDER                   = 0xE6E6FA
-- local C_FLORAL_WHITE               = 0xFFFAF0
-- local C_ALICE_BLUE                 = 0xF0F8FF
-- local C_GHOST_WHITE                = 0xF8F8FF
-- local C_HONEYDEW                   = 0xF0FFF0
-- local C_IVORY                      = 0xFFFFF0
-- local C_AZURE                      = 0xF0FFFF
-- local C_SNOW                       = 0xFFFAFA
-- local C_BLACK                      = 0x000000
-- local C_DIM_GRAY                   = 0x696969
-- local C_GRAY                       = 0x808080
-- local C_DARK_GRAY                  = 0xA9A9A9
-- local C_SILVER                     = 0xC0C0C0
-- local C_LIGHT_GRAY                 = 0xD3D3D3
-- local C_GAINSBORO                  = 0xDCDCDC
-- local C_WHITE_SMOKE                = 0xF5F5F5
-- local C_WHITE                      = 0xFFFFFF

--- Table of colors
-- @table colors
local C={
MAROON                  =  0x800000,-- RGB888: 0x800000 <div style="display: inline-flex; background-color: #800000 ; min-width: 4em">&nbsp;</div>
DARK_RED                =  0x8B0000,-- RGB888: 0x8B0000
BROWN                   =  0xA52A2A,-- RGB888: 0xA52A2A
FIREBRICK               =  0xB22222,-- RGB888: 0xB22222
CRIMSON                 =  0xDC143C,-- RGB888: 0xDC143C
RED                     =  0xFF0000,-- RGB888: 0xFF0000
TOMATO                  =  0xFF6347,-- RGB888: 0xFF6347
CORAL                   =  0xFF7F50,-- RGB888: 0xFF7F50
INDIAN_RED              =  0xCD5C5C,-- RGB888: 0xCD5C5C
LIGHT_CORAL             =  0xF08080,-- RGB888: 0xF08080
DARK_SALMON             =  0xE9967A,-- RGB888: 0xE9967A
SALMON                  =  0xFA8072,-- RGB888: 0xFA8072
LIGHT_SALMON            =  0xFFA07A,-- RGB888: 0xFFA07A
ORANGE_RED              =  0xFF4500,-- RGB888: 0xFF4500
DARK_ORANGE             =  0xFF8C00,-- RGB888: 0xFF8C00
ORANGE                  =  0xFFA500,-- RGB888: 0xFFA500
GOLD                    =  0xFFD700,-- RGB888: 0xFFD700
DARK_GOLDEN_ROD         =  0xB8860B,-- RGB888: 0xB8860B
GOLDEN_ROD              =  0xDAA520,-- RGB888: 0xDAA520
PALE_GOLDEN_ROD         =  0xEEE8AA,-- RGB888: 0xEEE8AA
DARK_KHAKI              =  0xBDB76B,-- RGB888: 0xBDB76B
KHAKI                   =  0xF0E68C,-- RGB888: 0xF0E68C
OLIVE                   =  0x808000,-- RGB888: 0x808000
YELLOW                  =  0xFFFF00,-- RGB888: 0xFFFF00
YELLOW_GREEN            =  0x9ACD32,-- RGB888: 0x9ACD32
DARK_OLIVE_GREEN        =  0x556B2F,-- RGB888: 0x556B2F
OLIVE_DRAB              =  0x6B8E23,-- RGB888: 0x6B8E23
LAWN_GREEN              =  0x7CFC00,-- RGB888: 0x7CFC00
CHART_REUSE             =  0x7FFF00,-- RGB888: 0x7FFF00
GREEN_YELLOW            =  0xADFF2F,-- RGB888: 0xADFF2F
DARK_GREEN              =  0x006400,-- RGB888: 0x006400
GREEN                   =  0x00FF00,-- RGB888: 0x00FF00
FOREST_GREEN            =  0x228B22,-- RGB888: 0x228B22
LIME                    =  0x00FF00,-- RGB888: 0x00FF00
LIME_GREEN              =  0x32CD32,-- RGB888: 0x32CD32
LIGHT_GREEN             =  0x90EE90,-- RGB888: 0x90EE90
PALE_GREEN              =  0x98FB98,-- RGB888: 0x98FB98
DARK_SEA_GREEN          =  0x8FBC8F,-- RGB888: 0x8FBC8F
MEDIUM_SPRING_GREEN     =  0x00FA9A,-- RGB888: 0x00FA9A
SPRING_GREEN            =  0x00FF7F,-- RGB888: 0x00FF7F
SEA_GREEN               =  0x2E8B57,-- RGB888: 0x2E8B57
MEDIUM_AQUA_MARINE      =  0x66CDAA,-- RGB888: 0x66CDAA
MEDIUM_SEA_GREEN        =  0x3CB371,-- RGB888: 0x3CB371
LIGHT_SEA_GREEN         =  0x20B2AA,-- RGB888: 0x20B2AA
DARK_SLATE_GRAY         =  0x2F4F4F,-- RGB888: 0x2F4F4F
TEAL                    =  0x008080,-- RGB888: 0x008080
DARK_CYAN               =  0x008B8B,-- RGB888: 0x008B8B
AQUA                    =  0x00FFFF,-- RGB888: 0x00FFFF
CYAN                    =  0x00FFFF,-- RGB888: 0x00FFFF
LIGHT_CYAN              =  0xE0FFFF,-- RGB888: 0xE0FFFF
DARK_TURQUOISE          =  0x00CED1,-- RGB888: 0x00CED1
TURQUOISE               =  0x40E0D0,-- RGB888: 0x40E0D0
MEDIUM_TURQUOISE        =  0x48D1CC,-- RGB888: 0x48D1CC
PALE_TURQUOISE          =  0xAFEEEE,-- RGB888: 0xAFEEEE
AQUA_MARINE             =  0x7FFFD4,-- RGB888: 0x7FFFD4
POWDER_BLUE             =  0xB0E0E6,-- RGB888: 0xB0E0E6
CADET_BLUE              =  0x5F9EA0,-- RGB888: 0x5F9EA0
STEEL_BLUE              =  0x4682B4,-- RGB888: 0x4682B4
CORN_FLOWER_BLUE        =  0x6495ED,-- RGB888: 0x6495ED
DEEP_SKY_BLUE           =  0x00BFFF,-- RGB888: 0x00BFFF
DODGER_BLUE             =  0x1E90FF,-- RGB888: 0x1E90FF
LIGHT_BLUE              =  0xADD8E6,-- RGB888: 0xADD8E6
SKY_BLUE                =  0x87CEEB,-- RGB888: 0x87CEEB
LIGHT_SKY_BLUE          =  0x87CEFA,-- RGB888: 0x87CEFA
MIDNIGHT_BLUE           =  0x191970,-- RGB888: 0x191970
NAVY                    =  0x000080,-- RGB888: 0x000080
DARK_BLUE               =  0x00008B,-- RGB888: 0x00008B
MEDIUM_BLUE             =  0x0000CD,-- RGB888: 0x0000CD
BLUE                    =  0x0000FF,-- RGB888: 0x0000FF
ROYAL_BLUE              =  0x4169E1,-- RGB888: 0x4169E1
BLUE_VIOLET             =  0x8A2BE2,-- RGB888: 0x8A2BE2
INDIGO                  =  0x4B0082,-- RGB888: 0x4B0082
DARK_SLATE_BLUE         =  0x483D8B,-- RGB888: 0x483D8B
SLATE_BLUE              =  0x6A5ACD,-- RGB888: 0x6A5ACD
MEDIUM_SLATE_BLUE       =  0x7B68EE,-- RGB888: 0x7B68EE
MEDIUM_PURPLE           =  0x9370DB,-- RGB888: 0x9370DB
DARK_MAGENTA            =  0x8B008B,-- RGB888: 0x8B008B
DARK_VIOLET             =  0x9400D3,-- RGB888: 0x9400D3
DARK_ORCHID             =  0x9932CC,-- RGB888: 0x9932CC
MEDIUM_ORCHID           =  0xBA55D3,-- RGB888: 0xBA55D3
PURPLE                  =  0x800080,-- RGB888: 0x800080
THISTLE                 =  0xD8BFD8,-- RGB888: 0xD8BFD8
PLUM                    =  0xDDA0DD,-- RGB888: 0xDDA0DD
VIOLET                  =  0xEE82EE,-- RGB888: 0xEE82EE
MAGENTA                 =  0xFF00FF,-- RGB888: 0xFF00FF
ORCHID                  =  0xDA70D6,-- RGB888: 0xDA70D6
MEDIUM_VIOLET_RED       =  0xC71585,-- RGB888: 0xC71585
PALE_VIOLET_RED         =  0xDB7093,-- RGB888: 0xDB7093
DEEP_PINK               =  0xFF1493,-- RGB888: 0xFF1493
HOT_PINK                =  0xFF69B4,-- RGB888: 0xFF69B4
LIGHT_PINK              =  0xFFB6C1,-- RGB888: 0xFFB6C1
PINK                    =  0xFFC0CB,-- RGB888: 0xFFC0CB
ANTIQUE_WHITE           =  0xFAEBD7,-- RGB888: 0xFAEBD7
BEIGE                   =  0xF5F5DC,-- RGB888: 0xF5F5DC
BISQUE                  =  0xFFE4C4,-- RGB888: 0xFFE4C4
BLANCHED_ALMOND         =  0xFFEBCD,-- RGB888: 0xFFEBCD
WHEAT                   =  0xF5DEB3,-- RGB888: 0xF5DEB3
CORN_SILK               =  0xFFF8DC,-- RGB888: 0xFFF8DC
LEMON_CHIFFON           =  0xFFFACD,-- RGB888: 0xFFFACD
LIGHT_GOLDEN_ROD_YELLOW =  0xFAFAD2,-- RGB888: 0xFAFAD2
LIGHT_YELLOW            =  0xFFFFE0,-- RGB888: 0xFFFFE0 
SADDLE_BROWN            =  0x8B4513,-- RGB888: 0x8B4513
SIENNA                  =  0xA0522D,-- RGB888: 0xA0522D
CHOCOLATE               =  0xD2691E,-- RGB888: 0xD2691E 
PERU                    =  0xCD853F,-- RGB888: 0xCD853F 
SANDY_BROWN             =  0xF4A460,-- RGB888: 0xF4A460 
BURLY_WOOD              =  0xDEB887,-- RGB888: 0xDEB887 
TAN                     =  0xD2B48C,-- RGB888: 0xD2B48C 
ROSY_BROWN              =  0xBC8F8F,-- RGB888: 0xBC8F8F 
MOCCASIN                =  0xFFE4B5,-- RGB888: 0xFFE4B5 
NAVAJO_WHITE            =  0xFFDEAD,-- RGB888: 0xFFDEAD 
PEACH_PUFF              =  0xFFDAB9,-- RGB888: 0xFFDAB9
MISTY_ROSE              =  0xFFE4E1,-- RGB888: 0xFFE4E1
LAVENDER_BLUSH          =  0xFFF0F5,-- RGB888: 0xFFF0F5 
LINEN                   =  0xFAF0E6,-- RGB888: 0xFAF0E6 
OLD_LACE                =  0xFDF5E6,-- RGB888: 0xFDF5E6
PAPAYA_WHIP             =  0xFFEFD5,-- RGB888: 0xFFEFD5
SEA_SHELL               =  0xFFF5EE,-- RGB888: 0xFFF5EE
MINT_CREAM              =  0xF5FFFA,-- RGB888: 0xF5FFFA
SLATE_GRAY              =  0x708090,-- RGB888: 0x708090
LIGHT_SLATE_GRAY        =  0x778899,-- RGB888: 0x778899
LIGHT_STEEL_BLUE        =  0xB0C4DE,-- RGB888: 0xB0C4DE
LAVENDER                =  0xE6E6FA,-- RGB888: 0xE6E6FA
FLORAL_WHITE            =  0xFFFAF0,-- RGB888: 0xFFFAF0
ALICE_BLUE              =  0xF0F8FF,-- RGB888: 0xF0F8FF
GHOST_WHITE             =  0xF8F8FF,-- RGB888: 0xF8F8FF
HONEYDEW                =  0xF0FFF0,-- RGB888: 0xF0FFF0
IVORY                   =  0xFFFFF0,-- RGB888: 0xFFFFF0
AZURE                   =  0xF0FFFF,-- RGB888: 0xF0FFFF 
SNOW                    =  0xFFFAFA,-- RGB888: 0xFFFAFA
BLACK                   =  0x000000,-- RGB888: 0x000000
DIM_GRAY                =  0x696969,-- RGB888: 0x696969
GRAY                    =  0x808080,-- RGB888: 0x808080
DARK_GRAY               =  0xA9A9A9,-- RGB888: 0xA9A9A9 
SILVER                  =  0xC0C0C0,-- RGB888: 0xC0C0C0
LIGHT_GRAY              =  0xD3D3D3,-- RGB888: 0xD3D3D3
GAINSBORO               =  0xDCDCDC,-- RGB888: 0xDCDCDC
WHITE_SMOKE             =  0xF5F5F5,-- RGB888: 0xF5F5F5 
WHITE                   =  0xFFFFFF,-- RGB888: 0xFFFFFF
}


local gui = {}

--- Return current GUI object.
-- @return GUI object
local function get_gui()
   return gui
end


-- local NUMBER_OF_DRIVERS = 1

-- Table of drivers
local DRIVERS = {
   "DRIVER_FILL_SCREEN",
   "DRIVER_DRAW_LINE",
   "DRIVER_DRAW_FRAME",
   "DRIVER_FILL_FRAME",
   "DRIVER_DRAW_TRIANGLE",
   "DRIVER_DRAW_CIRCLE",
   "DRIVER_FILL_CIRCLE"
}

--- Get value of specified bit
-- @param number Number
-- @param bit Bit position (starting from 0 as LSB)
local function getbit (number, bit)
   local highbits = math.floor(number/2^(bit+1))
   local lowbits = number - highbits*2^(bit+1)
   return math.floor(lowbits/2^bit)
end

--- Logarithm in base 2
-- @param number Number
-- @treturn number Logarithm in base 2 of Number
local function log2 (number)
   return math.log(number)/math.log(2)
end

--- Split a 32 bit RGB (888) or ARGB(8888) color.
-- @param rgb32 32 bit RGB (888) color (0x00RRGGBB) or ARGB (0xAARRGGBB)
-- @treturn[1] int red (0xRR)
-- @treturn[1] int green (0xGG)
-- @treturn[1] int blue (0xBB)
-- @treturn[2] int red (0xRR)
-- @treturn[2] int green (0xGG)
-- @treturn[2] int blue (0xBB)
-- @treturn[2] int alpha (0xAA)
local function rgb32split(rgb32)
   local rgb = rgb32
   if rgb32 > 0xFFFFFF then -- with alpha channel
      local alpha = math.floor(rgb32/(2^24))
      local rgb = rgb32 - alpha*(2^16)
   end
   local red = math.floor(rgb/(2^16))
   local gb = rgb - red*(2^16)
   local green = math.floor(gb/(2^8))
   local blue = gb - green*(2^8)
   if rgb > 0xFFFFFF then -- with alpha channel
      return red, green, blue, alpha
   else
      return red, green, blue
   end
end

--- Split a 16 bit RGB (565) color.
-- @param rgb16 16 bit RGB (565) color (bits: rrrr rggg gggb bbbb)
-- @treturn int red
-- @treturn int green
-- @treturn int blue
local function rgb16split(rgb16)
   local rgb = rgb16
   local red = math.floor(rgb/(2^11))
   local gb = rgb - red * (2^11)
   local green = math.floor(gb/(2^5))
   local blue = gb - green * (2^5)
   return red, green, blue
end

--- Transform a 32 bit RGB (888) color to 16 bit RGB (565) color.
-- @param rgb32 32 bit RGB (888) color (0xRRGGBB)
-- @treturn int 16 bit RGB (565) color (bits: rrrr rggg gggb bbbb)
local function rgb32to16(rgb32)
   local red8, green8, blue8 = rgb32split(rgb32)
   local red = red8/(2^8-1)
   local green = green8/(2^8-1)
   local blue = blue8/(2^8-1)
   local red5 = math.floor( (2^5-1)*red )
   local green6 = math.floor( (2^6-1)*green )
   local blue5 = math.floor( (2^5-1)*blue )
   return red5 * 2^(5+6) + green6 * 2^5 + blue5
end

--- Transform a 16 bit RGB (565) color to 32 bit RGB (888) color.
-- @param rgb16 16 bit RGB (565) color (bits: rrrr rggg gggb bbbb)
-- @treturn int 32 bit RGB (888) color (0xRRGGBB)
local function rgb16to32(rgb16)
   local red5, green6, blue5 = rgb16split(rgb16)
   local red = red5/(2^5-1)
   local green = green6/(2^6-1)
   local blue = blue5/(2^5-1)
   local red8 = math.floor( (2^8-1)*red )
   local green8 = math.floor( (2^8-1)*green )
   local blue8 = math.floor( (2^8-1)*blue )
   return red8 * 2^16 + green8 * 2^8 + blue8
end



--- µGUI Core structure.
-- @field pset User pset-function
-- @field x_dim X-Dimension (= X-Resolution) of the display
-- @field y_dim Y-Dimension (= Y-Resolution) of the display
-- @field touch Touch object holding latest touch data to be processed
-- @field next_window Next window
-- @field active_window Active window
-- @field last_window Last window
-- @field console Console structure
-- @field font Font structure
-- @field fore_color Fore color
-- @field back_color Back color
-- @field desktop_color Desktop color
-- @field state GUI state
-- @field driver List of hardware drivers
-- @table UG_GUI

--- BITMAP structure
-- @field p BMP data
-- @field width BMP width
-- @field height BMP height
-- @field bpp Bits per pixel
-- <ul>
-- <li>BMP_BPP_1</li>
-- <li>BMP_BPP_2</li>
-- <li>BMP_BPP_4</li>
-- <li>BMP_BPP_8</li>
-- <li>BMP_BPP_16</li>
-- <li>BMP_BPP_32</li>
-- </ul>
-- @field colors
-- <ul>
-- <li>BMP_RGB888</li>
-- <li>BMP_RGB565</li>
-- <li>BMP_RGB555</li>
-- </ul>
-- @table UG_BMP


--- Classic functions.
-- @section classicfnc

--- This function initializes the GUI module.
-- Furthermore it links the user pset function to the μ GUI core.
-- @param g GUI structure
-- @param p User pset-function
-- @param x X-Dimension (= X-Resolution) of the display
-- @param y Y-Dimension (= Y-Resolution) of the display
-- @treturn bool Return true
-- @usage ugui = require "ugui"
-- mainGui = {} -- GUI object
-- local function UserPixelSetFunction(x, y, c)
--   -- Your code
-- end
--
-- UG_Init(gui, UserPixelSetFunction, 320, 240)
local function UG_Init(g, p, x, y)
   g.pset = p
   g.x_dim = x
   g.y_dim = y
   g.touch = {xp=-1, yp=-1, state=0}
   g.console = {}
   g.console.x_start = 4
   g.console.y_start = 4
   g.console.x_end = g.x_dim - g.console.x_start-1
   g.console.y_end = g.y_dim - g.console.x_start-1
   g.console.x_pos = g.console.x_end
   g.console.y_pos = g.console.y_end
   g.console.fore_color = C.WHITE
   g.console.back_color = C.BLACK
   g.font = {}
   g.font.char_h_space = 1
   g.font.char_v_space = 1
   g.font.char_width = nil
   g.font.char_height = nil
   g.font.p = nil
   g.desktop_color = 0x5E8BEf
   g.fore_color = C.WHITE
   g.back_color = C.BLACK
   g.state = {}
   g.next_window = nil
   g.active_window = nil
   g.last_window = nil

   -- Clear drivers
   g.driver = {}
   for i = 1, #DRIVERS, 1 do
      g.driver[DRIVERS[i]]={}
      g.driver[DRIVERS[i]].driver = nil
      g.driver[DRIVERS[i]].state = {DRIVER_REGISTERED=false,
				    DRIVER_ENABLED=false}
   end


   gui = g
   -- return 1
   return true
   
end

--- Select GUI.
-- With this function you can switch between different GUIs / displays.
-- @param g GUI structure
-- @treturn bool Return true
-- @usage
-- gui_oled = {} -- Global GUI object (OLED)
-- gui_tft = {} -- Global GUI object (TFT)
--
-- UG_Init(gui_oled, OLEDPixelSetFunction, 128, 64)
-- UG_Init(gui_tft, OLEDPixelSetFunction, 480, 272)
-- UG_SelectGUI(gui_oled)
-- -- ...
-- UG_SelectGUI(gui_tft)
-- -- ...
--
local function UG_SelectGUI(g)
   gui = g
   -- return 1
   return true
   
end

--- Select font.
-- With this function you can select a font.
-- Fonts have to be defined or loaded separately.
-- Some fonts are distributed as modules
-- @param font Font object
-- @usage
-- font_12x16 = require "ugui-font12x16"
-- ugui.UG_FontSelect(font_12x16)
local function UG_FontSelect(font)
   gui.font.p = font.p
   gui.font.char_width = font.char_width
   gui.font.char_height = font.char_height
end

--- Draws a line between two points.
-- @param x1 X start position of the line
-- @param y1 Y start position of the line
-- @param x2 X end position of the line
-- @param y2 Y end position of the line
-- @param c color
local function UG_DrawLine( x1, y1, x2, y2, c )
   local n, dx, dy, sgndx, sgndy, dxabs, dyabs, x, y, drawx, drawy

   -- @todo check why this was needed. It stops drawing lines towards top-right
   -- if ( x2 < x1 ) then
   --    n = x2
   --    x2 = x1
   --    x1 = n
   -- end
   -- if ( y2 < y1 ) then
   --    n = y2
   --    y2 = y1
   --    y1 = n
   -- end
   
   if gui.driver.DRIVER_DRAW_LINE.state.DRIVER_ENABLED then
      if gui.driver.DRIVER_DRAW_LINE.driver(x1,y1,x2,y2,c) then return end
   end

   dx = x2 - x1
   dy = y2 - y1
   if (dx>0) then dxabs = dx else dxabs = -dx end
   if (dy>0) then dyabs = dy else dyabs = -dy end
   if (dx>0) then sgndx = 1 else sgndx = -1 end
   if (dy>0) then sgndy = 1 else sgndy = -1 end
   x = math.floor(dyabs / 2^1)
   y = math.floor(dxabs / 2^1)
   drawx = x1
   drawy = y1

   gui.pset(drawx, drawy,c)

   if( dxabs >= dyabs ) then
      for n=0, dxabs-1, 1 do
         y = y + dyabs
         if( y >= dxabs ) then
            y = y - dxabs;
            drawy = drawy + sgndy;
         end
         drawx = drawx + sgndx;
         gui.pset(drawx, drawy,c)
      end
   else
      for n=0, dyabs-1, 1 do
	 x = x + dxabs
	 if( x >= dyabs ) then
	    x = x - dyabs
            drawx = drawx + sgndx
	 end
	 drawy = drawy + sgndy
         gui.pset(drawx, drawy,c)
      end
   end
   
end

--- Draws an arc with a selected color.
-- @param x0 X center position of the arc
-- @param y0 Y center position of the arc
-- @param r Radius of the arc
-- @param s Selected sectors
-- @param c Color
local function UG_DrawArc( x0, y0, r, s, c )
   local x,y,xd,yd,e

   if ( x0<0 ) then return end
   if ( y0<0 ) then return end
   if ( r<=0 ) then return end

   xd = 1 - (r * 2^1)
   yd = 0
   e = 0
   x = r
   y=0

   while ( x >= y ) do
      -- Q1
      if getbit(s,0) > 0 then gui.pset(x0 + x, y0 - y, c) end
      if getbit(s,1) > 0 then gui.pset(x0 + y, y0 - x, c) end
      -- Q2
      if getbit(s,2) > 0 then gui.pset(x0 - y, y0 - x, c) end
      if getbit(s,3) > 0 then gui.pset(x0 - x, y0 - y, c) end
      -- Q3
      if getbit(s,4) > 0 then gui.pset(x0 - x, y0 + y, c) end
      if getbit(s,5) > 0 then gui.pset(x0 - y, y0 + x, c) end
      -- Q4
      if getbit(s,6) > 0 then gui.pset(x0 + y, y0 + x, c) end
      if getbit(s,7) > 0 then gui.pset(x0 + x, y0 + y, c) end

      y = y + 1
      e = e + yd
      yd = yd + 2
      if ( ((e * 2^1) + xd) > 0 ) then
	 x = x - 1
	 e = e + xd
	 xd = xd + 2
      end
   end
         
end

--- Fills a rectangular area with a selected color.
-- @param x1 X start position of the frame
-- @param y1 Y start position of the frame
-- @param x2 X end position of the frame
-- @param y2 Y end position of the frame
-- @param c Color
local function UG_FillFrame( x1, y1, x2, y2, c )
   local n,m

   if ( x2 < x1 ) then
      n = x2
      x2 = x1
      x1 = n
   end
   if ( y2 < y1 ) then
      n = y2
      y2 = y1
      y1 = n
   end

   if gui.driver.DRIVER_FILL_FRAME.state.DRIVER_ENABLED then
      if gui.driver.DRIVER_FILL_FRAME.driver(x1,y1,x2,y2,c) then return end
   end

   for m=y1, y2, 1 do
      for n=x1, x2, 1 do
	 gui.pset(n,m,c)
      end
   end
      
end

--- Fills the whole screen with the selected color.
-- @param c Color
local function UG_FillScreen( c )
   if gui.driver.DRIVER_FILL_SCREEN.state.DRIVER_ENABLED then
      if gui.driver.DRIVER_FILL_SCREEN.driver(c) then return end
   end
   
   UG_FillFrame(0,0,gui.x_dim-1,gui.y_dim-1,c)
end

--- Fills a rectangular area with a selected color.
-- The rectangular area has rounded corners.
-- @param x1 X start position of the frame
-- @param y1 Y start position of the frame
-- @param x2 X end position of the frame
-- @param y2 Y end position of the frame
-- @param r Corner radius
-- @param c Color
local function UG_FillRoundFrame( x1, y1, x2, y2, r, c )
   local x,y,xd
   if x2 < x1 then
      x = x2
      x2 = x1
      x1 = x
   end
   if y2 < y1 then
      y = y2
      y2 = y1
      y1 = y
   end

   if ( r<=0 ) then return end

   xd = 3 - r*2^1
   x = 0
   y = r

   UG_FillFrame(x1 + r, y1, x2 - r, y2, c)

   while x <= y do
      if( y > 0 ) then
	 UG_DrawLine(x2 + x - r, y1 - y + r, x2+ x - r, y + y2 - r, c)
	 UG_DrawLine(x1 - x + r, y1 - y + r, x1- x + r, y + y2 - r, c)
      end
      if( x > 0 ) then
	 UG_DrawLine(x1 - y + r, y1 - x + r, x1 - y + r, x + y2 - r, c)
	 UG_DrawLine(x2 + y - r, y1 - x + r, x2 + y - r, x + y2 - r, c)
      end
      if ( xd < 0 ) then xd = xd + (x * 2^2) + 6 else
	 xd = xd + ( (x-y) * 2^2 ) + 10
	 y = y - 1;
      end
      x = x + 1	 
   end
   
end

--- Draws a rectangular mesh with a selected color.
-- @param x1 X start position of the mesh
-- @param y1 Y start position of the mesh
-- @param x2 X end position of the mesh
-- @param y2 Y end position of the mesh
-- @param c Color
-- @param[opt=2] mesh_step for the mesh in pixels
local function UG_DrawMesh( x1, y1, x2, y2, c , mesh_step)
   local mesh_step = mesh_step or 2
   local n,m
   if ( x2 < x1 ) then
      n = x2
      x2 = x1
      x1 = n
   end
   if ( y2 < y1 ) then
      n = y2
      y2 = y1
      y1 = n
   end

   for m = y1, y2, mesh_step do
      for n = x1, x2, mesh_step do
	 gui.pset(n,m,c)
      end
   end
   
end

--- Draws a frame with a selected color.
-- @param x1 X start position of the mesh
-- @param y1 Y start position of the mesh
-- @param x2 X end position of the mesh
-- @param y2 Y end position of the mesh
-- @param c Color
local function UG_DrawFrame( x1, y1, x2, y2, c )
   if gui.driver.DRIVER_DRAW_FRAME.state.DRIVER_ENABLED then
      if gui.driver.DRIVER_DRAW_FRAME.driver(x1,y1,x2,y2,c) then return end
   end
   UG_DrawLine(x1,y1,x2,y1,c)
   UG_DrawLine(x1,y2,x2,y2,c)
   UG_DrawLine(x1,y1,x1,y2,c)
   UG_DrawLine(x2,y1,x2,y2,c)
end

--- Draws a frame with a selected color.
-- The frame has rounded corners.
-- @param x1 X start position of the frame
-- @param y1 Y start position of the frame
-- @param x2 X end position of the frame
-- @param y2 Y end position of the frame
-- @param r Corner radius
-- @param c Color
local function UG_DrawRoundFrame( x1, y1, x2, y2, r, c )
   local n
   if x2 < x1 then
      n = x2
      x2 = x1
      x1 = n
   end
   if y2 < y1 then
      n = y2
      y2 = y1
      y1 = n
   end
   
   if ( r > x2 ) then return end
   if ( r > y2 ) then return end

   UG_DrawLine(x1+r, y1, x2-r, y1, c)
   UG_DrawLine(x1+r, y2, x2-r, y2, c)
   UG_DrawLine(x1, y1+r, x1, y2-r, c)
   UG_DrawLine(x2, y1+r, x2, y2-r, c)
   UG_DrawArc(x1+r, y1+r, r, 0x0C, c)
   UG_DrawArc(x2-r, y1+r, r, 0x03, c)
   UG_DrawArc(x1+r, y2-r, r, 0x30, c)
   UG_DrawArc(x2-r, y2-r, r, 0xC0, c)
   
end

--- Draws a pixel with a selected color.
-- @param x0 X position of the pixel
-- @param y0 Y position of the pixel
-- @param c Color
local function UG_DrawPixel( x0, y0, c )
   gui.pset(x0,y0,c)
end

--- Draws a circle with a selected color and radius.
-- @param x0 X position of the pixel
-- @param y0 Y position of the pixel
-- @param r Radius of the circle
-- @param c Color
local function UG_DrawCircle( x0, y0, r, c )
   local x,y,xd,yd,e
   
   if gui.driver.DRIVER_DRAW_CIRCLE.state.DRIVER_ENABLED then
      if gui.driver.DRIVER_DRAW_CIRCLE.driver(x0,y0,r,c) then return end
   end

   if ( x0<0 ) then return end
   if ( y0<0 ) then return end
   if ( r<=0 ) then return end

   xd = 1 - (r * 2^1)
   yd = 0
   e = 0
   x = r
   y = 0

   while ( x >= y ) do
      gui.pset(x0 - x, y0 + y, c)
      gui.pset(x0 - x, y0 - y, c)
      gui.pset(x0 + x, y0 + y, c)
      gui.pset(x0 + x, y0 - y, c)
      gui.pset(x0 - y, y0 + x, c)
      gui.pset(x0 - y, y0 - x, c)
      gui.pset(x0 + y, y0 + x, c)
      gui.pset(x0 + y, y0 - x, c)

      y = y + 1
      e = e + yd
      yd = yd + 2
      if ( ((e * 2^1) + xd) > 0 ) then
	 x = x - 1
	 e = e + xd
	 xd = xd +2
      end
   end
   
end

--- Fills a circle with a selected color.
-- @param x0 X position of the pixel
-- @param y0 Y position of the pixel
-- @param r Radius of the circle
-- @param c Color
local function UG_FillCircle( x0, y0, r, c )
   local x,y,xd
   
   if gui.driver.DRIVER_FILL_CIRCLE.state.DRIVER_ENABLED then
      if gui.driver.DRIVER_FILL_CIRCLE.driver(x0,y0,r,c) then return end
   end
   
   xd = 3 - r*2^1
   x = 0
   y = r

   while ( x <= y ) do
     if( y > 0 ) then
        UG_DrawLine(x0 - x, y0 - y,x0 - x, y0 + y, c)
        UG_DrawLine(x0 + x, y0 - y,x0 + x, y0 + y, c)
     end
     if( x > 0 ) then
        UG_DrawLine(x0 - y, y0 - x,x0 - y, y0 + x, c)
        UG_DrawLine(x0 + y, y0 - x,x0 + y, y0 + x, c)
     end
     if ( xd < 0 ) then xd = xd + (x * 2^2) + 6 else
        xd = xd + ((x - y) * 2^2) + 10
        y = y - 1
     end
     x = x + 1
   end
   UG_DrawCircle(x0, y0, r,c)
   
end

--- Draws a single char.
-- @param chr Char
-- @param x X start position of the char
-- @param y Y start position of the char
-- @param fc Fore color of the char
-- @param bc Back color of the char
local function UG_PutChar( chr, x, y, fc, bc )
   local xo,yo
   local bt
   local p

   bt = string.byte(chr) + 1

   -- if bt == 0xF6 then bt = 0x94 -- ö
   -- elseif bt == 0xD6 then bt = 0x99 -- Ö
   -- elseif bt == 0xFC then bt = 0x81 -- ü
   -- elseif bt == 0xDC then bt = 0x9A -- Ü
   -- elseif bt == 0xE4 then bt = 0x84 -- ä
   -- elseif bt == 0xC4 then bt = 0x8E -- Ä
   -- elseif bt == 0xB5 then bt = 0xE6 -- µ
   -- elseif bt == 0xB0 then bt = 0xF8 -- °
   -- end
   
   p = gui.font.p
   local char_width = gui.font.char_width
   local char_height = gui.font.char_height
   local number_of_bytes = math.ceil(char_width/8)
   local symbol = p[bt]
   yo = y
   for row = 1, char_height, 1 do
      xo = x
      -- io.write("["..row.."]: ")
      for bit = 0, char_width-1, 1 do
	 local row_byte = math.floor((char_width-1)/8) - math.floor(bit/8)
	 local index = number_of_bytes * row - row_byte 
	 -- io.write(index.."/"..row_byte..","..(bit%8).." ("..xo..","..yo..") | ")
	 local value = getbit(symbol[index], bit%8 )
	 if value > 0 then gui.pset(xo,yo,fc) else
	    if bc then gui.pset(xo,yo,bc) end
	 end
	 xo = xo + 1
      end
      yo = yo + 1
      -- io.write("\n")
   end

end

--- Draws a string.
-- @param x X start position of the string
-- @param y Y start position of the string
-- @param str String
local function UG_PutString(x, y, str)
   local xp, yp
   local chr

   xp, yp = x, y
   for chr_pos = 1, str:len(), 1 do
      chr = str:sub(chr_pos, chr_pos)
      -- if yp > gui.y_dim -1 then return end
      if chr == "\n" then
	 xp = gui.x_dim
	 -- chr_pos = chr_pos + 1
	 -- goto continue
	 -- end
      else -- when not using continue
	 if ( xp + gui.font.char_width > gui.x_dim - 1 ) then
	    xp = x
	    yp = yp + gui.font.char_height + gui.font.char_v_space
	 end

	 UG_PutChar(chr, xp, yp, gui.fore_color, gui.back_color)

	 xp = xp + gui.font.char_width + gui.font.char_h_space
	 -- chr_pos = chr_pos + 1
      end
      -- chr_pos = chr_pos + 1
      -- ::continue::
   end
   
end

--- Adds a string to the console.
-- @param str String
local function UG_ConsolePutString(str)
   local chr
   local cw = gui.font.char_width
   local ch = gui.font.char_height
   local chs = gui.font.char_h_space
   local cvs = gui.font.char_v_space
   
   for chr_pos = 1, str:len(), 1 do
      chr = str:sub(chr_pos, chr_pos)
      if chr == "\n" then
	 gui.console.x_pos = gui.x_dim
      else
	 gui.console.x_pos = gui.console.x_pos + cw + chs
	 if gui.console.x_pos + cw > gui.console.x_end then
	    gui.console.x_pos = gui.console.x_start
	    gui.console.y_pos = gui.console.y_pos + ch + cvs
	 end
	 if gui.console.y_pos + ch > gui.console.y_end then
	    gui.console.x_pos = gui.console.x_start
	    gui.console.y_pos = gui.console.y_start
	    UG_FillFrame(gui.console.x_start, gui.console.y_start,
			 gui.console.x_end, gui.console.y_end,
			 gui.console.back_color)
	 end
	 UG_PutChar(chr, gui.console.x_pos, gui.console.y_pos,
		    gui.console.fore_color, gui.console.back_color)
      end
      chr_pos = chr_pos + 1
   end
end

--- Defines the active console area.
-- @param xs start position of the console
-- @param ys start position of the console
-- @param xe end position of the console
-- @param ye end position of the console
local function UG_ConsoleSetArea( xs, ys, xe, ye )
   gui.console.x_start = xs
   gui.console.y_start = ys
   gui.console.x_end = xe
   gui.console.y_end = ye
end

--- Defines the fore color of the console.
-- @param c Fore color
local function UG_ConsoleSetForecolor( c )
   gui.console.fore_color = c
end

--- Defines the back color of the console.
-- @param c Back color
local function UG_ConsoleSetBackcolor( c )
   gui.console.back_color = c
end

--- Defines the fore color of the string.
-- @param c Fore color
local function UG_SetForecolor( c )
   gui.fore_color = c
end

--- Defines the back color of the string.
-- @param c Back color
local function UG_SetBackcolor( c )
   gui.back_color = c
end

--- Returns the X-Dimension of the display.
-- @treturn int X-Dimension
local function UG_GetXDim()
   return gui.x_dim
end

--- Returns the Y-Dimension of the display.
-- @treturn int Y-Dimension
local function UG_GetYDim()
   return gui.y_dim
end

--- Defines the horizontal space between each char.
-- @param s Horizontal space
local function UG_FontSetHSpace( s )
   gui.font.char_h_space = s
end

--- Defines the vertical space between each char.
-- @param s Vertical space
local function UG_FontSetVSpace( s )
   gui.font.char_v_space = s
end

local pal_window = {
   0x646464, -- Frame 0
   0x646464,
   0x646464,
   0x646464,
   0xFFFFFF, -- Frame 1
   0xFFFFFF,
   0x696969,
   0x696969,
   0xE3E3E3, -- Frame 2
   0xE3E3E3,
   0xA0A0A0,
   0xA0A0A0
}

local pal_button_pressed = {
   0x646464, -- Frame 0
   0x646464,
   0x646464,
   0x646464,
   0xA0A0A0, -- Frame 1
   0xA0A0A0,
   0xA0A0A0,
   0xA0A0A0,
   0xF0F0F0, -- Frame 2
   0xF0F0F0,
   0xF0F0F0,
   0xF0F0F0
}

local pal_button_released = {
   0x646464, -- Frame 0
   0x646464,
   0x646464,
   0x646464,
   0xFFFFFF, -- Frame 1
   0xFFFFFF,
   0x696969,
   0x696969,
   0xE3E3E3, -- Frame 2
   0xE3E3E3,
   0xA0A0A0,
   0xA0A0A0
}

--- Driver Functions.
-- @section driverfnc

--- Registers a driver (and enables it).
-- @param driver_type Driver type
-- @param driver Driver function
local function UG_DriverRegister( driver_type, driver )
 
   if ( gui.driver[driver_type] ) then
      gui.driver[driver_type].driver = driver;
      gui.driver[driver_type].state["DRIVER_REGISTERED"]=true
      gui.driver[driver_type].state["DRIVER_ENABLED"]=true
   end

end

--- Enables a driver.
-- @param driver_type Driver type
local function UG_DriverEnable( driver_type )
   
   if ( gui.driver[driver_type] ) then
      if ( gui.driver[driver_type].state["DRIVER_REGISTERED"] ) then
	 gui.driver[driver_type].state["DRIVER_ENABLED"] = true;
      end
   end

end
--- Disables a driver.
-- @param driver_type Driver type
local function UG_DriverDisable( driver_type )

   if ( gui.driver[driver_type] ) then
      if ( gui.driver[driver_type].state["DRIVER_REGISTERED"] ) then
	 gui.driver[driver_type].state["DRIVER_ENABLED"] = false
      end
   end

end

--- Internal Functions
-- @section internalfnc

local function _UG_AlignDecode( align )
   local decodedAlignment = {
      ALIGN_H_LEFT = false,
      ALIGN_H_CENTER = false,
      ALIGN_H_RIGHT = false,
      ALIGN_V_TOP = false,
      ALIGN_V_CENTER = false,
      ALIGN_V_BOTTOM = false
   }
   if align == "ALIGN_BOTTOM_RIGHT" then
      decodedAlignment["ALIGN_V_BOTTOM"] = true
      decodedAlignment["ALIGN_H_RIGHT"] = true
   elseif align == "ALIGN_BOTTOM_CENTER" then
      decodedAlignment["ALIGN_V_BOTTOM"] = true
      decodedAlignment["ALIGN_H_CENTER"] = true
   elseif align == "ALIGN_BOTTOM_LEFT" then
      decodedAlignment["ALIGN_V_BOTTOM"] = true
      decodedAlignment["ALIGN_H_LEFT"] = true
   elseif align == "ALIGN_CENTER_RIGHT" then
      decodedAlignment["ALIGN_V_CENTER"] = true
      decodedAlignment["ALIGN_H_RIGHT"] = true
   elseif align == "ALIGN_CENTER" then
      decodedAlignment["ALIGN_V_CENTER"] = true
      decodedAlignment["ALIGN_H_CENTER"] = true
   elseif align == "ALIGN_CENTER_LEFT" then
      decodedAlignment["ALIGN_V_CENTER"] = true
      decodedAlignment["ALIGN_H_LEFT"] = true
   elseif align == "ALIGN_TOP_RIGHT" then
      decodedAlignment["ALIGN_V_TOP"] = true
      decodedAlignment["ALIGN_H_RIGHT"] = true
   elseif align == "ALIGN_TOP_CENTER" then
      decodedAlignment["ALIGN_V_TOP"] = true
      decodedAlignment["ALIGN_H_CENTER"] = true
   elseif align == "ALIGN_TOP_LEFT" then
      decodedAlignment["ALIGN_V_TOP"] = true
      decodedAlignment["ALIGN_H_LEFT"] = true
   end
   return decodedAlignment
end

local function _UG_PutText( txt )
   if not txt.font.p then return end
   local str = txt.str
   local xs, ys, xe, ye = txt.a.xs, txt.a.ys, txt.a.xe, txt.a.ye
   local align = _UG_AlignDecode(txt.align)
   local char_width = txt.font.char_width
   local char_height = txt.font.char_height
   local char_h_space = txt.h_space
   local char_v_space = txt.v_space
   local xp, yp

   -- save current font
   local current_font = gui.font
   -- select font in txt object
   UG_FontSelect(txt.font)

   -- Return if basic conditions not met
   if not str then return end
   if (ye -ys) < txt.font.char_height then return end

   -- count rows
   local _, rc = str:gsub('\n', '\n')
   rc = rc + 1

   yp = 0

   -- calculate yp
   if align["ALIGN_V_CENTER"] or align["ALIGN_V_BOTTOM"] then
      yp = ye - ys + 1
      yp = yp - char_height * rc
      yp = yp - char_v_space * (rc - 1)
      if yp < 0 then return end
   end
   if align["ALIGN_V_CENTER"] then yp = math.floor(yp/2) end
   yp = yp + ys

   -- process each line in string
   for line in string.gmatch(str, "[^\n]+") do
      sl = line:len()
      -- calculate xp
      xp = xe - xs + 1
      xp = xp - char_width * sl
      xp = xp - char_h_space * (sl-1)
      if xp < 0  then return end

      if align["ALIGN_H_LEFT"] then xp = 0
      elseif align["ALIGN_H_CENTER"] then xp = math.floor(xp/2) end
      xp = xp + xs

      -- process each char in the line
      for chr_pos = 1, sl, 1 do
	 local chr = str:sub(chr_pos, chr_pos)
	 UG_PutChar(chr, xp, yp, txt.fc, txt.bc)
	 xp = xp + char_width + char_h_space
      end
      yp = yp + char_height + char_v_space
   end

   -- reset font back
   if current_font then UG_FontSelect(current_font) end
   
end

-- local function _UG_GetFreeObject( wnd )
-- end

local function _UG_SearchObject( wnd, obj_type, id )

   local obj = wnd.objlst[id]
   if (not obj.state["OBJ_STATE_FREE"]) and obj.state["OBJ_STATE_VALID"] then
      if obj.type == obj_type and obj.id == id then
	 return obj
      end
   end
   return nil
end

local function _UG_DeleteObject( wnd, obj_type, id )

   local obj = _UG_SearchObject( wnd, obj_type, id )

   -- Object found?
   if obj then
      -- We don't want to delete a visible or busy object!
      if obj.state["OBJ_STATE_VISIBLE"] or obj.state["OBJ_STATE_UPDATE"] then
	 return false
      end
      table.remove(wnd.objlst, id)
      return true
   end
   return false
   
end

local function _UG_ProcessTouchData( wnd )
   local xp, yp = gui.touch.xp, gui.touch.yp
   local tchstate = gui.touch.state
   local objstate, objtouch
   
   for k, obj in pairs(wnd.objlst) do
      objstate = obj.state
      objtouch = obj.touch_state
      -- local xs, ys = obj.a_abs.xs, obj.a_abs.ys,
      -- local xe, ye = obj.a_abs.xe, obj.a_abs.ye
      if ( (not objstate["OBJ_STATE_FREE"]) and
	    objstate["OBJ_STATE_VALID"] and
	    objstate["OBJ_STATE_VISIBLE"] and
	    (not objstate["OBJ_STATE_REDRAW"]) )
      then
	 -- Process touch data
	 if (tchstate > 0) and (xp ~= -1) then
	    if not objtouch["OBJ_TOUCH_STATE_IS_PRESSED"] then
	       objtouch["OBJ_TOUCH_STATE_PRESSED_OUTSIDE_OBJECT"] = true
	       objtouch["OBJ_TOUCH_STATE_CHANGED"] = true
	       objtouch["OBJ_TOUCH_STATE_RELEASED_ON_OBJECT"]= false
	       objtouch["OBJ_TOUCH_STATE_RELEASED_OUTSIDE_OBJECT"] = false
	       objtouch["OBJ_TOUCH_STATE_CLICK_ON_OBJECT"] = false
	    end
	    objtouch["OBJ_TOUCH_STATE_IS_PRESSED_ON_OBJECT"] = false
	    -- @todo Isn't the following the same as and'ing the conditions?
	    -- if (xp >= xs) and (xp <= xe) and (yp >= ys) and (yp <= ya) then
	    if xp >= obj.a_abs.xs then
	       if xp <= obj.a_abs.xe then
		  if yp >= obj.a_abs.ys then
		     if yp <= obj.a_abs.ye then
			objtouch["OBJ_TOUCH_STATE_IS_PRESSED_ON_OBJECT"] = true
			if not objtouch["OBJ_TOUCH_STATE_IS_PRESSED"] then
			   objtouch["OBJ_TOUCH_STATE_PRESSED_OUTSIDE_OBJECT"] = false
			   objtouch["OBJ_TOUCH_STATE_PRESSED_ON_OBJECT"] = true
			end
		     end
		  end
	       end
	    end
	    objtouch["OBJ_TOUCH_STATE_IS_PRESSED"] = true
	 elseif objtouch["OBJ_TOUCH_STATE_IS_PRESSED"] then
	    if objtouch["OBJ_TOUCH_STATE_IS_PRESSED_ON_OBJECT"] then
	       if objtouch["OBJ_TOUCH_STATE_PRESSED_ON_OBJECT"] then
		  objtouch["OBJ_TOUCH_STATE_CLICK_ON_OBJECT"] = true
	       end
	       objtouch["OBJ_TOUCH_STATE_RELEASED_ON_OBJECT"] = true
	    else
	       objtouch["OBJ_TOUCH_STATE_RELEASED_OUTSIDE_OBJECT"] = true
	    end
	    if objtouch["OBJ_TOUCH_STATE_IS_PRESSED"] then
	       objtouch["OBJ_TOUCH_STATE_CHANGED"] = true
	    end
	    objtouch["OBJ_TOUCH_STATE_PRESSED_OUTSIDE_OBJECT"] = false
	    objtouch["OBJ_TOUCH_STATE_PRESSED_ON_OBJECT"] = false
	    objtouch["OBJ_TOUCH_STATE_IS_PRESSED"] = false
	 end
      end
      -- Is this necessary in lua? Haven't we modified the original object?
      obj.touch_state = objtouch
   end
   
end

local function _UG_UpdateObjects( wnd )
   local objstate
   local objtouch
   
   -- Check each object, if it needs to be updated?
   for k,obj in pairs(wnd.objlst) do
      objstate = obj.state
      objtouch = obj.touch_state
      if ( (not objstate["OBJ_STATE_FREE"]) and
	    objstate["OBJ_STATE_VALID"])
      then
	 if objstate["OBJ_STATE_UPDATE"] then
	    obj.update(wnd,obj)
	 end
	 if ( objstate["OBJ_STATE_VISIBLE"] and
	      objstate["OBJ_STATE_TOUCH_ENABLE"] ) then
	    if ( objtouch["OBJ_TOUCH_STATE_CHANGED"] or
		 objtouch["OBJ_TOUCH_STATE_IS_PRESSED"] ) then
	       obj.update(wnd,obj)
	    end
	 end
      end
   end
   
end

local function _UG_HandleEvents( wnd )

   local msg = {}
   local objstate
   
   -- @todo Handle window-related events

   -- Handle object related events
   msg.type="MSG_TYPE_OBJECT"
   for k, obj in pairs(wnd.objlst) do
      objstate = obj.state
      if ((not objstate["OBJ_STATE_FREE"]) and objstate["OBJ_STATE_VALID"]) then
	 if obj.event ~= "OBJ_EVENT_NONE" then
	    msg.src = obj
	    msg.id = obj.type
	    msg.sub_id = obj.id

	    wnd.cb(msg)

	    obj.event = "OBJ_EVENT_NONE"
	 end
      end
   end
      
end

local function _UG_WindowDrawTitle( wnd )
   local txt = {}
   local xs, ys, xe, ye
   if (wnd and wnd.state["WND_STATE_VALID"]) then
      xs, ys, xe, ye = wnd.xs, wnd.ys, wnd.xe, wnd.ye

      -- 3D style?
      if wnd.style["WND_STYLE_3D"] then
	 xs = xs + 3
	 ys = ys + 3
	 xe = xe - 3
	 ye = ye - 3
      end

      -- Is the window active or inactive?
      if ( wnd == gui.active_window ) then
	 txt.bc = wnd.title.bc
	 txt.fc = wnd.title.fc
      else
	 txt.bc = wnd.title.ibc
	 txt.fc = wnd.title.ifc
      end

      -- Draw title
      UG_FillFrame(xs, ys, xe, ys+wnd.title.height-1, txt.bc)

      -- Draw title text
      txt.str = wnd.title.str
      txt.font = wnd.title.font
      txt.a = {}
      txt.a.xs = xs + 3
      txt.a.ys = ys
      txt.a.xe = xe
      txt.a.ye = ys + wnd.title.height - 1
      txt.align = wnd.title.align
      txt.h_space = wnd.title.h_space
      txt.v_space = wnd.title.v_space
      _UG_PutText( txt )

      -- Draw line
      UG_DrawLine(xs, ys+wnd.title.height, xe, ys+wnd.title.height,
		  pal_window[11+1])
      return true
   end
   return false
end

local function _UG_DrawObjectFrame(xs, ys ,xe, ye, p)
   -- Frame 0
   UG_DrawLine(xs, ys  , xe-1, ys  , p[1])
   UG_DrawLine(xs, ys+1, xs  , ye-1, p[2])
   UG_DrawLine(xs, ye  , xe  , ye  , p[3])
   UG_DrawLine(xe, ys  , xe  , ye-1, p[4])
   -- Frame 1
   UG_DrawLine(xs+1, ys+1, xe-2, ys+1, p[5])
   UG_DrawLine(xs+1, ys+2, xs+1, ye-2, p[6])
   UG_DrawLine(xs+1, ye-1, xe-1, ye-1, p[7])
   UG_DrawLine(xe-1, ys+1, xe-1, ye-2, p[8])
   -- Frame 2
   UG_DrawLine(xs+2, ys+2, xe-3, ys+2, p[9])
   UG_DrawLine(xs+2, ys+3, xs+2, ye-3, p[10])
   UG_DrawLine(xs+2, ye-2, xe-2, ye-2, p[11])
   UG_DrawLine(xe-2, ys+2, xe-2, ye-3, p[12])
end

local function _UG_WindowUpdate( wnd )
   local xs, ys, xe, ye = wnd.xs, wnd.ys, wnd.xe, wnd.ye

   wnd.state["WND_STATE_UPDATE"] = false
   -- Is the window visible?
   if wnd.state["WND_STATE_VISIBLE"] then
      -- 3D style?
      if ( wnd.style["WND_STYLE_3D"] and
	   not wnd.state["WND_STATE_REDRAW_TITLE"] ) then
	 _UG_DrawObjectFrame(xs, ys, xe, ye, pal_window)
	 xs = xs + 3
	 ys = ys + 3
	 xe = xe - 3
	 ye = ye - 3
      end
      -- Show title bar?
      if wnd.style["WND_STYLE_SHOW_TITLE"] then
	 _UG_WindowDrawTitle( wnd )
	 ys = ys + wnd.title.height + 1
	 if wnd.state["WND_STATE_REDRAW_TITLE"] then
	    wnd.state["WND_STATE_REDRAW_TITLE"] = false
	    return
	 end
      end
      -- Draw window area?
      UG_FillFrame(xs, ys, xe, ye, wnd.bc)

      -- Force each object to be updated!
      for k,obj in pairs(wnd.objlst) do
	 if ( (not obj.state["OBJ_STATE_FREE"]) and
	       obj.state["OBJ_STATE_VALID"] and
	       obj.state["OBJ_STATE_VISIBLE"] )
	 then
	    obj.state["OBJ_STATE_UPDATE"] = true
	    obj.state["OBJ_STATE_REDRAW"] = true
	 end
      end
   else
      UG_FillFrame(wnd.xs, wnd.xs, wnd.xe, wnd.ye,
		   gui.desktop_color)
   end	    
   
end

--- Miscellaneous Structures and Functions
-- @section miscfnc

--- Update Function.
-- μGUI controls the refreshing process of each window
-- including all daughter objects. This is all
-- done by the function Update Function. Therefore, this
-- function has to be called periodically either in
-- an ISR or in background.
--
-- Note: If the user forgets to call this function, nothing will happen.
local function UG_Update()
   local wnd

   -- Is somebody waiting for this update?
   if gui.state["UG_SATUS_WAIT_FOR_UPDATE"] then
      gui.state["UG_SATUS_WAIT_FOR_UPDATE"] = false end

   -- Keep track of the windows
   if gui.next_window ~= gui.active_window then
      if gui.next_window then
	 gui.last_window = gui.active_window
         gui.active_window = gui.next_window

	 -- Do we need to draw an inactive title?
	 if (gui.last_window
		and gui.last_window.style["WND_STYLE_SHOW_TITLE"]
		and gui.last_window.state["WND_STATE_VISIBLE"])
	 then
	    -- Do both windows differ in size/position?
	    if ( (gui.last_window.xs ~= gui.active_window.xs) or
		  (gui.last_window.xe ~= gui.active_window.xe) or
		  (gui.last_window.ys ~= gui.active_window.ys) or
		  (gui.last_window.ye ~= gui.active_window.ye) )
	    then
	       -- Redraw title of the last window
	       _UG_WindowDrawTitle ( gui.last_window )
	    end
	 end
	 gui.active_window.state["WND_STATE_REDRAW_TITLE"] = false
	 gui.active_window.state["WND_STATE_UPDATE"] = true
	 gui.active_window.state["WND_STATE_VISIBLE"] = true
      end
   end

   -- Is there an active window?
   if gui.active_window then
      wnd = gui.active_window

      -- Does the window need to be updated?
      if wnd.state["WND_STATE_UPDATE"] then
	 -- Do it!
	 _UG_WindowUpdate( wnd )
      end

      -- Is the window visible?
      if wnd.state["WND_STATE_VISIBLE"] then
	 _UG_ProcessTouchData( wnd )
	 _UG_UpdateObjects( wnd )
	 _UG_HandleEvents( wnd )
      end
   end
end

--- Wait for update.
local function UG_WaitForUpdate()
   gui.state["UG_SATUS_WAIT_FOR_UPDATE"] = true
   while gui.state["UG_SATUS_WAIT_FOR_UPDATE"] do
   end
end

--- DrawBMP.
-- @param xp X coordinate
-- @param yp Y coordinate
-- @param bmp BMP data
local function UG_DrawBMP(xp, yp, bmp)
   local x,y,xs
   local rgb
   local p
   local i
   
   if not bmp.p then return end

   -- Only 16 BPP supported so far
   if bmp.bpp == "BMP_BPP_16" then
      p = bmp.p
   else return end

   --xs = xp
   i = 1
   for y = 0, bmp.height-1, 1 do
      --xp = xs
      for x = 0, bmp.width-1, 1 do
	 rgb = rgb16to32(p[i])
	 -- UG_DrawPixel(xp+x, yp+y, rgb)
	 gui.pset(xp+x, yp+y, rgb)
	 i=i+1
      end
   end
   
end

--- Touch Update
-- μ GUI supports any touch technology (like analog resistive or
-- projected capacitive) as long as it provides input data in X/Y format.
-- It is very simple to connect a touch device to μ GUI. The only
-- thing the user needs to do is to call UG_TouchUpdate().
-- This function transfers the raw touch data to the μ GUI touch
-- processor which then will detect, validate and track all touch events.
-- @param xp X coordinate
-- @param yp Y coordinate
-- @param state State
local function UG_TouchUpdate(xp, yp, state)
   -- gui.touch.xp = xp
   -- gui.touch.yp = yp
   -- gui.touch.state = state
   gui.touch = {xp = xp, yp = yp, state = state}
end

--- Touch structure
-- @field state <ul><li>TOUCH_STATE_PRESSED</li><li>TOUCH_STATE_RELEASED</li></ul>
-- @field xp X coordinate
-- @field yp Y coordinate
-- @table UG_TOUCH


--- Window Structures and Functions.
-- @section windowfnc

--- Window structure.
-- @field objlst List of objects
-- @field state Window state
-- <ul>
-- <li>WND_STATE_FREE</li>
-- <li>WND_STATE_VALID</li>
-- <li>WND_STATE_BUSY</li>
-- <li>WND_STATE_VISIBLE</li>
-- <li>WND_STATE_ENABLE</li>
-- <li>WND_STATE_UPDATE</li>
-- <li>WND_STATE_REDRAW_TITLE</li>
-- </ul>
-- @field bc Back color
-- @field fc Fore color
-- @field xs
-- @field ys
-- @field xe
-- @field ye
-- @field style
-- <ul>
-- <li>WND_STYLE_3D</li>
-- <li>WND_STYLE_HIDE_TITLE</li>
-- <li>WND_STYLE_SHOW_TITLE</li>
-- </ul>
-- @field title Title object
-- @field cb Callback function to process events' messages
-- @table S_WINDOW

--- Title structure.
-- @field str Title string
-- @field font Title font
-- @field h_space Horizontal space
-- @field v_space Vertical space
-- @field align
-- <ul>
-- <li>ALIGN_NONE</li>
-- <li>ALIGN_BOTTOM_RIGHT</li>
-- <li>ALIGN_BOTTOM_CENTER</li>
-- <li>ALIGN_BOTTOM_LEFT</li>
-- <li>ALIGN_CENTER_RIGHT</li>
-- <li>ALIGN_CENTER</li>
-- <li>ALIGN_CENTER_LEFT</li>
-- <li>ALIGN_TOP_RIGHT</li>
-- <li>ALIGN_TOP_CENTER</li>
-- <li>ALIGN_TOP_LEFT</li>
-- </ul>
-- @field fc Fore color
-- @field bc Back color
-- @field ifc Inactive fore color
-- @field ibc Inactive back color
-- @field height Title height
-- @table UG_TITLE

--- Message structure passed to Window's cb.
-- @field type Type of message
-- <ul>
-- <li>MSG_TYPE_NONE</li>
-- <li>MSG_TYPE_WINDOW</li>
-- <li>MSG_TYPE_OBJECT</li>
-- </ul>
-- @field id Message id
-- <ul> If message type is MSG_TYPE_OBJECT id is one of:
-- <li>OBJ_TYPE_NONE</li>
-- <li>OBJ_TYPE_BUTTON</li>
-- <li>OBJ_TYPE_TEXTBOX</li>
-- <li>OBJ_TYPE_IMAGE</li>
-- </ul>
-- @field sub_id Source id (e.g. object's id)
-- @field event (not used)
-- @field src Source object
-- @table UG_MESSAGE

--- Clear a window. (internal function)
-- @param wnd The window object
-- @treturn bool Result of function
local function _UG_WindowClear( wnd )
   if (wnd) then
      if (wnd.state["WND_STATE_VISIBLE"]) then

	 wnd.state["WND_STATE_VISIBLE"] = false
	 UG_FillFrame(wnd.xs, wnd.ys, wnd.xe, wnd.ye, gui.desktop_color)

	 if (wnd ~= gui.active_window) then
	    -- If the current window is visible update it!
	    if (gui.active_window.state["WND_STATE_VISIBLE"]) then
	       gui.active_window.state["WND_STATE_REDRAW_TITLE"] = false
	       gui.active_window.state["WND_STATE_UPDATE"] = true
	    end
	 end
      end
      return true
   end
   return false
   
end

--- Creates a window.
-- @param wnd The window object
-- @param cb The callback function of the window
-- @treturn bool Result of function
local function UG_WindowCreate( wnd, cb )
   -- Initialize objects (?)
   -- Not necessary.
   
   -- Initialize wnd
   -- wnd.objcnt = objcnt
   wnd.objlst = {}
   wnd.state = {}
   wnd.state["WND_STATE_VALID"] = true
   wnd.fc = 0x000000
   wnd.bc = 0xF0F0F0
   wnd.xs = 0
   wnd.ys = 0
   wnd.xe = UG_GetXDim()-1
   wnd.ye = UG_GetYDim()-1
   wnd.cb = cb
   wnd.style = {}
   wnd.style["WND_STYLE_3D"] = true
   wnd.style["WND_STYLE_SHOW_TITLE"] = true
   wnd.title = {}

   -- Initialize wnd title-bar
   wnd.title.str = ""
   wnd.title.font = nil
   wnd.title.h_space = 2
   wnd.title.v_space = 2
   wnd.title.align = "ALIGN_CENTER_LEFT"
   wnd.title.fc = C.WHITE
   wnd.title.bc = C.BLUE
   wnd.title.ifc = C.WHITE
   wnd.title.ibc = C.GRAY
   wnd.title.height = 15

   return true
   
end

--- Deletes a window.
-- @param wnd Window object
-- @treturn bool Result of function
local function UG_WindowDelete( wnd )
   -- Does this really delete the window? shouldn't just wnd = nil do it?
   if ( wnd == gui.active_window ) then return false end

   -- Only delete valid windows
   if ( wnd and (wnd.state["WND_STATE_VALID"]) ) then
      -- wnd.state = 0
      for k, v in pairs(wnd.state) do wnd.state[k] = false end
      wnd.cb = nil
      -- wnd.objcnt = 0
      wnd.objlst = nil
      wnd.xs = 0
      wnd.ys = 0
      wnd.xe = 0
      wnd.ye = 0
      -- wnd.style = 0
      for k, v in pairs(wnd.style) do wnd.style[k] = false end
      return true
   end
   return false

end

--- Shows a window.
-- @param wnd Window object
-- @treturn bool Result of function
local function UG_WindowShow( wnd )
   if (wnd) then
      -- Force an update, even if this is the active window!
      wnd.state["WND_STATE_VISIBLE"] = true
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = false
      gui.next_window = wnd
      return true
   end
   return false
   
end

--- Hides a window.
-- @param wnd Window object
-- @treturn bool Result of function
local function UG_WindowHide( wnd )
   if (wnd) then
      if wnd == gui.active_window then
	 -- Is there an old window which just lost the focus?
	 if (gui.last_window and
	     gui.last_window.state["WND_STATE_VISIBLE"]) then
	    if ((gui.last_window.xs > wnd.xs)
		  or (gui.last_window.ys > wnd.ys)
		  or (gui.last_window.xe < wnd.xe)
		  or (gui.last_window.ye < wnd.ye))
	    then
	       _UG_WindowClear( wnd )
	    end
	    gui.next_window = gui.last_window
	 else
	    gui.active_window.state["WND_STATE_VISIBLE"] = false
	    gui.active_window.state["WND_STATE_UPDATE"] = true
	 end
      else	    
	 -- If the old window is visible, clear it!
	 _UG_WindowClear( wnd )
      end
      return true
   end
   return false   
	       
end

--- Changes the size of a window.
-- Note: All objects which don’t fit into the window after
-- resizing it will be hidden.
-- @param wnd Window to resize
-- @param xs X start position of the window
-- @param ys Y start position of the window
-- @param xe X end position of the window
-- @param ye Y end position of the window
-- @treturn bool Result of the function 
local function UG_WindowResize( wnd, xs, ys, xe, ye)
   local pos
   local xmax, ymax = UG_GetXDim() - 1, UG_GetYDim() -1

   if ( (wnd) and (wnd.state["WND_STATE_VALID"]) ) then
      if ( (xs < 0) or (ys < 0) ) then return false end
      if ( (xe > xmax) or (ye > ymax) ) then return false end
      pos = xe-xs
      if ( pos < 10 ) then return false end
      pos = ye-ys
      if ( pos < 10 ) then return false end

      -- ... and if everything is OK move the window!
      wnd.xs, wnd.ys, wnd.xe, wnd.ye = xs, ys, xe, ye

      if (wnd.state["WND_STATE_VISIBLE"] and (gui.active_window == wnd)) then
	 if wnd.ys > 0 then
	    UG_FillFrame(0, 0, xmax, wnd.ys-1, gui.desktop_color)
	    pos = wnd.ye + 1
	 end
	 if pos <= ymax then
	    UG_FillFrame(0, pos, xmax, ymax, gui.desktop_color) end
	 if wnd.xs > 0 then
	    UG_FillFrame(0, wnd.ys, wnd.xs-1, wnd.ye, gui.desktop_color) end
	 pos = wnd.xe + 1
	 if pos <= xmax then
	    UG_FillFrame(pos, wnd.ys, xmax, wnd.ye, gui.desktop_color) end
	 wnd.state["WND_STATE_REDRAW_TITLE"] = false
	 wnd.state["WND_STATE_UPDATE"] = true
      end
      return true
   end
   return false
   
end

--- Swaps fore- and back color of the window title.
-- @param wnd Window object
-- @treturn bool Result of function 
local function UG_WindowAlert( wnd )
   local title_text_color = UG_WIndowGetTitleTextColor( wnd )
   local title_color = UG_WIndowGetTitleColor( wnd )
   if ( not UG_WindowSetTitleTextColor(wnd, title_color) ) then
      return false end
   if ( not UG_WindowSetTitleColor(wnd, title_text_color) ) then
      return false end
   return true
   
end

--- Changes the fore color of the window.
-- The fore color of a window is the default fore color of all
-- objects.
-- @param wnd Window object
-- @param fc New fore color
-- @treturn bool Result of the function
local function UG_WindowSetForeColor( wnd, fc )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.fc = fc
      wnd.state["WND_STATE_UPDATE"] = true
      return true
   end
   return false
   
end

--- Changes the back color of the window.
-- @param wnd Window object
-- @param bc New back color
-- @treturn bool Result of the function
local function UG_WindowSetBackColor( wnd, bc )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.bc = bc
      wnd.state["WND_STATE_UPDATE"] = true
      return true
   end
   return false
   
end

--- Changes the text color of the window title.
-- @param wnd Window object
-- @param c New text color of the window title
-- @treturn bool Result of the function
local function UG_WindowSetTitleTextColor( wnd, c )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.fc = c
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = true
      return true
   end
   return false
end

--- Changes the color of the window title.
-- @param wnd Window object
-- @param c New color of the window title
-- @treturn bool Result of the function
local function UG_WindowSetTitleColor( wnd, c )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.bc = c
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = true
      return true
   end
   return false
end

--- Changes the inactive text color of the window title.
-- @param wnd Window object
-- @param c New inactive text color of the window title
-- @treturn bool Result of the function
local function UG_WindowSetTitleInactiveTextColor( wnd, c )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.ifc = c
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = true
      return true
   end
   return false
end

--- Changes the inactive color of the window title.
-- @param wnd Window object
-- @param c New inactive color of the window title
-- @treturn bool Result of the function
local function UG_WindowSetTitleInactiveColor( wnd, c )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.ibc = c
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = true
      return true
   end
   return false
end

--- Changes the text of the window title.
-- @param wnd Window object
-- @param str Title text
-- @treturn bool Result of the function
local function UG_WindowSetTitleText( wnd, str )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.str = str
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = true
      return true
   end
   return false
end

--- Changes the text font of the window title.
-- @param wnd Window object
-- @param font Title text
-- @treturn bool Result of the function
local function UG_WindowSetTitleTextFont( wnd, font )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.font = font
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = true
      if wnd.title.height <= (font.char_height + 1) then
	 wnd.title.height = font.char_height + 2
	 wnd.state["WND_STATE_REDRAW_TITLE"] = false
      end
      return true
   end
   return false
   
end

--- Changes the horizontal space between the characters in the window title.
-- @param wnd Window object
-- @param hs Horizontal space
-- @treturn bool Result of the function
local function UG_WindowSetTitleTextHSpace( wnd, hs )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.h_space = hs
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = true
      return true
   end
   return false
end

--- Changes the vertical space between the characters in the window title.
-- @param wnd Window object
-- @param vs Vertical space
-- @treturn bool Result of the function
local function UG_WindowSetTitleTextVSpace( wnd, vs )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.v_space = vs
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = true
      return true
   end
   return false
end

--- Changes the alignment of the text in the window title.
-- The following alignments are available:
-- <ul>
-- <li>ALIGN_NONE</li>
-- <li>ALIGN_BOTTOM_RIGHT</li>
-- <li>ALIGN_BOTTOM_CENTER</li>
-- <li>ALIGN_BOTTOM_LEFT</li>
-- <li>ALIGN_CENTER_RIGHT</li>
-- <li>ALIGN_CENTER</li>
-- <li>ALIGN_CENTER_LEFT</li>
-- <li>ALIGN_TOP_RIGHT</li>
-- <li>ALIGN_TOP_CENTER</li>
-- <li>ALIGN_TOP_LEFT</li>
-- </ul>
-- @param wnd Window object
-- @param align Text alignment
-- @treturn bool Result of the function
local function UG_WindowSetTitleTextAlignment( wnd, align )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.align = align
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = true
      return true
   end
   return false
end

--- Changes the height of the window title.
-- @param wnd Window object
-- @param height Title height
-- @treturn bool Result of the function
local function UG_WindowSetTitleHeight( wnd, height )
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.title.height = height
      wnd.state["WND_STATE_UPDATE"] = true
      wnd.state["WND_STATE_REDRAW_TITLE"] = false
      return true
   end
   return false
end

--- Changes the x start position of the window.
-- @param wnd Window object
-- @param xs X start position of the window
-- @treturn bool Result of the function
local function UG_WindowSetXStart ( wnd, xs )

   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.xs = xs
      return UG_WindowResize(wnd, wnd.xs, wnd.ys, wnd.xe, wnd.ye)
   end
   return false
   
end

--- Changes the y start position of the window.
-- @param wnd Window object
-- @param ys Y start position of the window
-- @treturn bool Result of the function
local function UG_WindowSetYStart ( wnd, ys )

   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.ys = ys
      return UG_WindowResize(wnd, wnd.xs, wnd.ys, wnd.xe, wnd.ye)
   end
   return false
   
end

--- Changes the x end position of the window.
-- @param wnd Window object
-- @param xe X end position of the window
-- @treturn bool Result of the function
local function UG_WindowSetXEnd ( wnd, xe )

   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.xe = xe
      return UG_WindowResize(wnd, wnd.xs, wnd.ys, wnd.xe, wnd.ye)
   end
   return false
   
end

--- Changes the y end position of the window.
-- @param wnd Window object
-- @param ye Y end position of the window
-- @treturn bool Result of the function
local function UG_WindowSetYEnd ( wnd, ye )

   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      wnd.ye = ye
      return UG_WindowResize(wnd, wnd.xs, wnd.ys, wnd.xe, wnd.ye)
   end
   return false
   
end

--- Changes the style of the window.
-- The following styles are available:
-- @param wnd Window object
-- @tparam bool style_3D 3D window style (true: 3D, false: 2D)
-- @tparam bool style_showtitle Show window title (true: show, false: hide)
-- @treturn bool Result of the function
local function UG_WindowSetStyle ( wnd, style_3D, style_showtitle )
   
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      -- 3D or 2D?
      wnd.style["WND_STYLE_3D"] = (style_3D or false)
      -- Show title bar?
      wnd.style["WND_STYLE_SHOW_TITLE"] = (style_showtitle or false)
      wnd.state["WND_STATE_UPDATE"] = true
      return true
   end
   return false

end

--- Returns the fore color of the window.
-- @param wnd Window object
-- @return Fore color of the window
local function UG_WindowGetForeColor( wnd )
   local c = C.BLACK
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then c = wnd.fc end
   return c
end

--- Returns the back color of the window.
-- @param wnd Window object
-- @return Back color of the window
local function UG_WindowGetBackColor( wnd )
   local c = C.BLACK
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then c = wnd.bc end
   return c
end

--- Returns the text color of the window title.
-- @param wnd Window object
-- @return Text color of the window title
local function UG_WindowGetTitleTextColor( wnd )
   local c = C.BLACK
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then c = wnd.title.fc end
   return c
end

--- Returns the title color of the window.
-- @param wnd Window object
-- @return Title color of the window
local function UG_WindowGetTitleColor( wnd )
   local c = C.BLACK
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then c = wnd.title.bc end
   return c
end

--- Returns the inactive text color of the window title.
-- @param wnd Window object
-- @return Inactive text color of the window title
local function UG_WindowGetTitleInactiveTextColor( wnd )
   local c = C.BLACK
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then c = wnd.title.ifc end
   return c
end

--- Returns the inactive color of the window title.
-- @param wnd Window object
-- @return Inactive color of the window title
local function UG_WindowGetTitleInactiveColor( wnd )
   local c = C.BLACK
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then c = wnd.title.ibc end
   return c
end

--- Returns the title text of the window.
-- @param wnd Window object
-- @treturn[1] string Title text of the window
-- @treturn[2] nil If title text not set or window is invalid
local function UG_WindowGetTitleText( wnd )

   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      return wnd.title.str
   end
   return nil

end

--- Returns the font of the title text of the window.
-- @param wnd Window object
-- @return[1] Font of the title text of the window
-- @treturn[2] nil If font not set or window is invalid
local function UG_WindowGetTitleTextFont( wnd )

   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      return wnd.title.font
   end
   return nil, "Window is not valid"

end

--- Returns the horizontal space between the characters in the window title.
-- @param wnd Window object
-- @treturn number Horizontal space between the characters in the window title
local function UG_WindowGetTitleTextHSpace( wnd )

   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      return wnd.title.h_space
   end
   return 0

end

--- Returns the vertical space between the characters in the window title.
-- @param wnd Window object
-- @treturn number Vertical space between the characters in the window title
local function UG_WindowGetTitleTextVSpace( wnd )

   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      return wnd.title.v_space
   end
   return 0

end

--- Returns the alignment of the text in the window title.
-- @param wnd Window object
-- @treturn string Alignment of the text in the window title
local function UG_WindowGetTitleTextAlignment( wnd )
   local align = "ALIGN_NONE"
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      align = wnd.title.align
   end
   return (align or "ALIGN_NONE")

end

--- Returns the height of the window title.
-- @param wnd Window object
-- @treturn int Height of the window title
local function UG_WindowGetTitleHeight( wnd )
   local h = 0
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      h = wnd.title.height
   end
   return (h or 0)
end

--- Returns the x start position of the window.
-- @param wnd Window object
-- @treturn int X start position of the window
local function UG_WindowGetXStart( wnd )
   local xs = -1
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then xs = wnd.xs end
   return (xs or -1)
end

--- Returns the y start position of the window.
-- @param wnd Window object
-- @treturn int Y start position of the window
local function UG_WindowGetYStart( wnd )
   local ys = -1
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then ys = wnd.ys end
   return (ys or -1)
end

--- Returns the x end position of the window.
-- @param wnd Window object
-- @treturn int X end position of the window
local function UG_WindowGetXEnd( wnd )
   local xe = -1
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then xe = wnd.xe end
   return (xe or -1)
end

--- Returns the y end position of the window.
-- @param wnd Window object
-- @treturn int Y end position of the window
local function UG_WindowGetYEnd( wnd )
   local ye = -1
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then ye = wnd.ye end
   return (ye or -1)
end

--- Returns the style of the window.
-- @param wnd Window object
-- @treturn bool 3D style (true) or 2D style (false)
-- @treturn bool Window title style (true: shown, false: hiden)
local function UG_WindowGetStyle( wnd )
   local style_3D, style_showtitle = false, false
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      style_3D = (wnd.style["STYLE_SHOW_3D"] or false)
      style_showtitle = (wnd.style["STYLE_SHOW_TITLE"] or false)
   end
   return style_3D, style_showtitle
end

--- Writes the area of the window to a variable.
-- @param wnd Window object
-- @treturn[1] tab Table with area's X start (xs),
-- Y start (ys), X end (xe) and Y end (ye) coordinates, respectively
-- @treturn[2] bool False if function fails
local function UG_WindowGetArea( wnd )
   local a = {xs=0, ys=0, xe=0, ye=0}
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      a.xs = wnd.xs
      a.ys = wnd.ys
      a.xe = wnd.xe
      a.ye = wnd.ye
      if (wnd.style["WND_STYLE_3D"]) then
	 a.xs = a.xs + 3
	 a.ys = a.ys + 3
	 a.xe = a.xe + 3
	 a.ye = a.ye + 3
      end
      if (wnd.style["WND_STYLE_SHOW_TITLE"]) then
	 a.ys = a.ys + wnd.title.height + 1
      end
      return a
   end
   return false
end

--- Returns the inner width of the window.
-- @param wnd Window object
-- @treturn int Inner width of the window
local function UG_WindowGetInnerWidth( wnd )
   local w = 0
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      w = wnd.xe - wnd.xs
      if wnd.style["WND_STYLE_3D"] then w = w - 6 end
      if ( w < 0 ) then w = 0 end
   end
   return (w or 0)
end

--- Returns the outer width of the window.
-- @param wnd Window object
-- @treturn int Outer width of the window
local function UG_WindowGetOuterWidth( wnd )
   local w = 0
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      w = wnd.xe - wnd.xs
      if ( w < 0 ) then w = 0 end
   end
   return (w or 0)
end

--- Returns the inner height of the window.
-- @param wnd Window object
-- @treturn int Inner height of the window
local function UG_WindowGetInnerHeight( wnd )
   local h = 0
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      h = wnd.ye - wnd.ys
      if wnd.style["WND_STYLE_3D"] then h = h - 6 end
      if ( h < 0 ) then h = 0 end
   end
   return (h or 0)
end

--- Returns the outer height of the window.
-- @param wnd Window object
-- @treturn int Outer height of the window
local function UG_WindowGetOuterHeight( wnd )
   local h = 0
   if ( wnd and wnd.state["WND_STATE_VALID"] ) then
      h = wnd.ye - wnd.ys
      if ( h < 0 ) then h = 0 end
   end
   return (h or 0)
end

--- Object structures
-- @section objsec

--- Object structure.
-- @field state Object state
-- <ul>
-- <li>OBJ_STATE_FREE</li>
-- <li>OBJ_STATE_VALID</li>
-- <li>OBJ_STATE_BUSY</li>
-- <li>OBJ_STATE_VISIBLE</li>
-- <li>OBJ_STATE_ENABLE</li>
-- <li>OBJ_STATE_UPDATE</li>
-- <li>OBJ_STATE_REDRAW</li>
-- <li>OBJ_STATE_TOUCH_ENABLE</li>
-- <li>OBJ_STATE_INIT = OBJ_STATE_FREE, OBJ_STATE_VALID</li>
-- </ul>
-- @field touch_state Object touch state
-- <ul>
-- <li>OBJ_TOUCH_STATE_CHANGED</li>
-- <li>OBJ_TOUCH_STATE_PRESSED_ON_OBJECT</li>
-- <li>OBJ_TOUCH_STATE_PRESSED_OUTSIDE_OBJECT</li>
-- <li>OBJ_TOUCH_STATE_RELEASED_ON_OBJECT</li>
-- <li>OBJ_TOUCH_STATE_RELEASED_OUTSIDE_OBJECT</li>
-- <li>OBJ_TOUCH_STATE_IS_PRESSED_ON_OBJECT</li>
-- <li>OBJ_TOUCH_STATE_IS_PRESSED</li>
-- <li>OBJ_TOUCH_STATE_CLICK_ON_OBJECT</li>
-- <li>OBJ_TOUCH_STATE_INIT = all other states off</li>
-- </ul>
-- @field update Object-specific update function
-- @field a_abs Absolute area of the object
-- @field a_rel Relative area of the object
-- @field type Object type
-- <ul><li>OBJ_TYPE_NONE</li><li>OBJ_TYPE_BUTTON</li>
-- <li>OBJ_TYPE_TEXTBOX</li><li>OBJ_TYPE_IMAGE</li></ul>
-- @field id Object ID
-- @field event Object-specific events
-- <ul>Standard object events:
-- <li>OBJ_EVENT_NONE</li>
-- <li>OBJ_EVENT_CLICKED</li>
-- </ul>
-- @field data Object-specific data
-- @table UG_OBJECT

--- Button object
-- @field state Button state
-- <ul>
-- <li>BTN_STATE_RELEASED = All other states off</li>
-- <li>BTN_STATE_PRESSED</li>
-- <li>BTN_STATE_ALWAYS_REDRAW</li>
-- </ul>
-- @field style Button style
-- <ul>
-- <li>BTN_STYLE_3D (not 2D style)</li>
-- <li>BTN_STYLE_TOGGLE_COLORS</li>
-- <li>BTN_STYLE_USE_ALTERNATE_COLORS</li>
-- </ul>
-- @table UG_BUTTON

--- Textbox object
-- @field str Text string
-- @field font Textbox font
-- @field style Textbox style (reserved)
-- @field fc Fore color
-- @field bc Back color
-- @field align Text alignment
-- <ul>
-- <li>ALIGN_NONE</li>
-- <li>ALIGN_BOTTOM_RIGHT</li>
-- <li>ALIGN_BOTTOM_CENTER</li>
-- <li>ALIGN_BOTTOM_LEFT</li>
-- <li>ALIGN_CENTER_RIGHT</li>
-- <li>ALIGN_CENTER</li>
-- <li>ALIGN_CENTER_LEFT</li>
-- <li>ALIGN_TOP_RIGHT</li>
-- <li>ALIGN_TOP_CENTER</li>
-- <li>ALIGN_TOP_LEFT</li>
-- </ul>
-- @field h_space Horizontal space
-- @field v_space Vertical space
-- @table UG_TEXTBOX

--- Image object
-- @field img Image
-- @field type Image type
-- <ul>
-- <li>IMG_TYPE_BMP</li>
-- </ul>
-- @table UG_IMAGE

--- Button Functions
-- @section buttonfnc

--- Private Button update function
local function _UG_ButtonUpdate( wnd, obj )
   local d
   local txt = {}
   local color	    
   
   -- Get object-specific data
   local btn = obj.data

   -- Object touch section
   if obj.touch_state["OBJ_TOUCH_STATE_CHANGED"] then
      -- Handle 'click' event
      if obj.touch_state["OBJ_TOUCH_STATE_CLICK_ON_OBJECT"] then
	 obj.event = "BTN_EVENT_CLICKED"
         obj.state["OBJ_STATE_UPDATE"] = true
      end
      -- Is the button pressed down?
      if obj.touch_state["OBJ_TOUCH_STATE_PRESSED_ON_OBJECT"] then
	 btn.state["BTN_STATE_PRESSED"] = true
         obj.state["OBJ_STATE_UPDATE"] = true
	 -- Can we release the button?
      elseif btn.state["BTN_STATE_PRESSED"] then
	 btn.state["BTN_STATE_PRESSED"] = false
	 obj.state["OBJ_STATE_UPDATE"] = true
      end
      obj.touch_state["OBJ_TOUCH_STATE_CHANGED"] = false
   end

   -- Object update section
   if obj.state["OBJ_STATE_UPDATE"] then
      if obj.state["OBJ_STATE_VISIBLE"] then
	 -- Full redraw necessary?
	 if (obj.state["OBJ_STATE_REDRAW"]
	     or btn.state["BTN_STATE_ALWAYS_REDRAW"]) then
	    local a = UG_WindowGetArea(wnd)
	    obj.a_abs.xs = obj.a_rel.xs + a.xs
            obj.a_abs.ys = obj.a_rel.ys + a.ys
            obj.a_abs.xe = obj.a_rel.xe + a.xs
            obj.a_abs.ye = obj.a_rel.ye + a.ys
	    if (obj.a_abs.ye >= wnd.ye) then return end
	    if (obj.a_abs.xe >= wnd.xe) then return end

	    -- 3D or 2D style?
	    if btn.style["BTN_STYLE_3D"] then d = 3 else d = 1 end

	    txt.bc = btn.bc
	    txt.fc = btn.fc

	    if btn.state["BTN_STATE_PRESSED"] then
	       -- "toogle" style?
	       if btn.style["BTN_STYLE_TOGGLE_COLORS"] then
		  -- Swap colors
		  txt.bc = btn.fc
		  txt.fc = btn.bc
		  -- Use alternate colors?
	       elseif btn.style["BTN_STYLE_USE_ALTERNATE_COLORS"] then
		  txt.bc = btn.abc
		  txt.fc = btn.afc
	       end
	    end
	    UG_FillFrame(obj.a_abs.xs+d, obj.a_abs.ys+d,
			 obj.a_abs.xe-d, obj.a_abs.ye-d, txt.bc)

	    -- Draw button text
	    txt.a = {}
	    txt.a.xs = obj.a_abs.xs+d
            txt.a.ys = obj.a_abs.ys+d
            txt.a.xe = obj.a_abs.xe-d
            txt.a.ye = obj.a_abs.ye-d
	    txt.align = "ALIGN_CENTER"
	    txt.font = btn.font
	    txt.h_space = 2
            txt.v_space = 2
            txt.str = btn.str;
            _UG_PutText( txt )
	    obj.state["OBJ_STATE_REDRAW"] = false
	 end
	 -- Draw button frame
	 if btn.style["BTN_STYLE_3D"] then
	    -- 3D
	    if btn.state["BTN_STATE_PRESSED"] then
	       color = pal_button_pressed
	    else
	       color = pal_button_released
	    end
	    _UG_DrawObjectFrame(obj.a_abs.xs,obj.a_abs.ys,
			       obj.a_abs.xe,obj.a_abs.ye,color)
	 else
	    -- 2D
	    if btn.state["BTN_STATE_PRESSED"] then
	       color = btn.abc
	    else
	       color = btn.afc
	    end
	    UG_DrawFrame(obj.a_abs.xs,obj.a_abs.ys,
			 obj.a_abs.xe,obj.a_abs.ye, color)
	 end
      else
	 UG_FillFrame(obj.a_abs.xs, obj.a_abs.ys,
		      obj.a_abs.xe, obj.a_abs.ye, wnd.bc)
      end
      obj.state["OBJ_STATE_UPDATE"] = false
   end
   
   
end

--- Creates a button.
-- @param wnd Window object which contains the button
-- @param btn Button object
-- @param id Button ID
-- @param xs X start position of the button
-- @param ys Y start position of the button
-- @param xe X end position of the button
-- @param ye Y end position of the button
-- @treturn bool Result of the function
local function UG_ButtonCreate( wnd, btn, id, xs, ys, xe, ye )

   -- Initialize object-specific parameters
   btn.state = {BTN_STATE_PRESSED = false,
		BTN_STATE_ALWAYS_REDRAW = false}
   btn.bc = wnd.bc
   btn.fc = wnd.fc
   btn.abc = wnd.bc
   btn.afc = wnd.fc
   btn.style = {BTN_STYLE_3D = true}
   btn.font = nil
   btn.str = "-"

   -- Initialize standard object parameters
   
   local obj = {
      update = _UG_ButtonUpdate, 
      touch_state = {OBJ_TOUCH_STATE_CHANGED=false,
		     OBJ_TOUCH_STATE_PRESSED_ON_OBJECT=false,
		     OBJ_TOUCH_STATE_PRESSED_OUTSIDE_OBJECT=false,
		     OBJ_TOUCH_STATE_RELEASED_ON_OBJECT=false,
		     OBJ_TOUCH_STATE_RELEASED_OUTSIDE_OBJECT=false,
		     OBJ_TOUCH_STATE_IS_PRESSED_ON_OBJECT=false,
		     OBJ_TOUCH_STATE_IS_PRESSED=false,
		     OBJ_TOUCH_STATE_CLICK_ON_OBJECT=false}, -- TOUCH_STATE_INIT
      type = "OBJ_TYPE_BUTTON",
      event = "OBJ_EVENT_NONE",
      a_rel = {xs=xs, ys=ys, xe=xe, ye=ye},
      a_abs = {xs=-1, ys=-1, xe=-1, ye=-1},
      id = id,
      state = {OBJ_STATE_VISIBLE=true,
	       OBJ_STATE_REDRAW=true,
	       OBJ_STATE_VALID=true,
	       OBJ_STATE_TOUCH_ENABLE=true},
      data = btn
   }

   -- Update function: Do your thing!
   obj.state["OBJ_STATE_FREE"] = false

   -- Insert object in window's object list
   wnd.objlst[id] = obj

   return true
   
end

--- Deletes a button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @treturn bool Result of the function
local function UG_ButtonDelete( wnd, id )
   return _UG_DeleteObject( wnd, "OBJ_TYPE_BUTTON", id )
end

--- Shows a button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @treturn bool Result of the function
local function UG_ButtonShow( wnd, id )
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )

   if obj then
      obj.state["OBJ_STATE_VISIBLE"] = true
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Hides a button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @treturn bool Result of the function
local function UG_ButtonHide( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )

   if obj then
      local btn = obj.data
      btn.state["BTN_STATE_PRESSED"] = false
      -- OBJ_TOUCH_STATE_INIT
      obj.touch_state = {OBJ_TOUCH_STATE_CHANGED=false,
			 OBJ_TOUCH_STATE_PRESSED_ON_OBJECT=false,
			 OBJ_TOUCH_STATE_PRESSED_OUTSIDE_OBJECT=false,
			 OBJ_TOUCH_STATE_RELEASED_ON_OBJECT=false,
			 OBJ_TOUCH_STATE_RELEASED_OUTSIDE_OBJECT=false,
			 OBJ_TOUCH_STATE_IS_PRESSED_ON_OBJECT=false,
			 OBJ_TOUCH_STATE_IS_PRESSED=false,
			 OBJ_TOUCH_STATE_CLICK_ON_OBJECT=false}
      obj.event = "OBJ_EVENT_NONE"
      obj.state["OBJ_STATE_VISIBLE"] = false
      obj.state["OBJ_STATE_UPDATE"] = true
      return true
   end
   return false   
   
end

--- Changes the fore color of the button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @param fc Fore color
-- @treturn bool Result of the function
local function UG_ButtonSetForeColor( wnd, id, fc )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      btn.fc = fc
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Changes the back color of the button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @param bc Back color
-- @treturn bool Result of the function
local function UG_ButtonSetBackColor( wnd, id, bc )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      btn.bc = bc
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end


--- Changes the alternate fore color of the button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @param afc Alternate fore color
-- @treturn bool Result of the function
local function UG_ButtonSetAlternateForeColor( wnd, id, afc )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      btn.afc = afc
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Changes the alternate back color of the button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @param abc Alternate back color
-- @treturn bool Result of the function
local function UG_ButtonSetAlternateBackColor( wnd, id, abc )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      btn.abc = abc
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Changes the button text.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @param str Button text
-- @treturn bool Result of the function
local function UG_ButtonSetText( wnd, id, str )
  
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      btn.str = str
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Changes the button font.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @param font Button font
-- @treturn bool Result of the function
local function UG_ButtonSetFont( wnd, id, font )
  
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      btn.font = font
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Changes the button style.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @tparam bool stl_3D 3D button style (true: 3D, false: 2D)
-- @tparam bool stl_toggle Toggle colors
-- @tparam bool stl_alt Use alternate colors
-- @treturn bool Result of the function
local function UG_ButtonSetStyle( wnd, id, stl_3D, stl_toggle, stl_alt )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data

      -- Select color scheme
      btn.style["BTN_STYLE_USE_ALTERNATE_COLORS"] = false
      btn.style["BTN_STYLE_TOGGLE_COLORS"] = false
      btn.state["BTN_STATE_ALWAYS_REDRAW"] = true
      if stl_toogle then
	 btn.style["BTN_STYLE_TOGGLE_COLORS"] = true
      elseif stl_alt then
	 btn.style["BTN_STYLE_USE_ALTERNATE_COLORS"] = true
      else
	 btn.state["BTN_STATE_ALWAYS_REDRAW"] = false
      end

      -- 3D or 2D
      btn.style["BTN_STYLE_3D"] = (style_3D or false)
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true

      return true      
   end
   return false
   
end

--- Returns the fore color of the button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @return Fore color of the button
local function UG_ButtonGetForeColor( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      return (btn.fc or C_BLACK)
   end

end

--- Returns the back color of the button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @return Back color of the button
local function UG_ButtonGetBackColor( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      return (btn.bc or C_BLACK)
   end

end
   
--- Returns the Alternate fore color of the button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @return Alternate fore color of the button
local function UG_ButtonGetAlternateForeColor( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      return (btn.afc or C_BLACK)
   end
   
end

--- Returns the Alternate back color of the button.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @return Alternate back color of the button
local function UG_ButtonGetAlternateBackColor( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      return (btn.abc or C_BLACK)
   end
   
end

--- Returns the button text.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @return Button text
local function UG_ButtonGetText( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      return btn.str
   end
   
end
   
--- Returns the button font.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @return Button font
local function UG_ButtonGetFont( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      return btn.font
   end
   
end

--- Returns the button style.
-- @param wnd Window object which contains the button
-- @param id Button ID
-- @return Button style
local function UG_ButtonGetStyle( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_BUTTON", id )
   if obj then
      local btn = obj.data
      return btn.style
   end
   
end

--- Textbox Functions
-- @section textboxfnc

--- Private Button update function
local function _UG_TextboxUpdate( wnd, obj )

   local txt ={}
   
   -- Get object-specific data
   local txb = obj.data

   -- Object touch section
   --
   -- Textbox doesn't support touch

   -- Object update section
   if obj.state["OBJ_STATE_UPDATE"] then
      if obj.state["OBJ_STATE_VISIBLE"] then
	 -- Full redraw necessary?
	 if obj.state["OBJ_STATE_REDRAW"] then
	    local a = UG_WindowGetArea(wnd)
	    obj.a_abs.xs = obj.a_rel.xs + a.xs
            obj.a_abs.ys = obj.a_rel.ys + a.ys
            obj.a_abs.xe = obj.a_rel.xe + a.xs
            obj.a_abs.ye = obj.a_rel.ye + a.ys
	    if (obj.a_abs.ye >= wnd.ye) then return end
	    if (obj.a_abs.xe >= wnd.xe) then return end

	    txt.bc = txb.bc
            txt.fc = txb.fc

	    UG_FillFrame(obj.a_abs.xs, obj.a_abs.ys,
			 obj.a_abs.xe, obj.a_abs.ye, txt.bc)

	    -- Draw Textbox text
	    txt.a = {}
	    txt.a.xs = obj.a_abs.xs
            txt.a.ys = obj.a_abs.ys
            txt.a.xe = obj.a_abs.xe
            txt.a.ye = obj.a_abs.ye
            txt.align = txb.align
            txt.font = txb.font
            txt.h_space = txb.h_space
            txt.v_space = txb.v_space
            txt.str = txb.str
	    _UG_PutText( txt )
	    obj.state["OBJ_STATE_REDRAW"] = false
	 end
      else
	 UG_FillFrame(obj.a_abs.xs, obj.a_abs.ys,
		      obj.a_abs.xe, obj.a_abs.ye, wnd.bc)
      end
      obj.state["OBJ_STATE_UPDATE"] = false
   end
   
end


--- Creates a textbox.
-- @param wnd Window object which contains the button
-- @param txb Textbox object
-- @param id Textbox id
-- @param xs X start position of the textbox
-- @param ys Y start position of the textbox
-- @param xe X end position of the textbox
-- @param ye Y end position of the textbox
-- @treturn bool Result of the function
local function UG_TextboxCreate( wnd, txb, id, xs, ys, xe, ye )

   -- Initialize object-specific parameters
   txb.str = nil
   txb.font = NULL
   txb.style = 0 -- reserved 
   txb.fc = wnd.fc
   txb.bc = wnd.bc
   txb.align = ALIGN_CENTER
   txb.h_space = 2
   txb.v_space = 2

   -- Initialize standard object parameters
   local obj = {
      update = _UG_TextboxUpdate, 
      touch_state = {OBJ_TOUCH_STATE_CHANGED=false,
		     OBJ_TOUCH_STATE_PRESSED_ON_OBJECT=false,
		     OBJ_TOUCH_STATE_PRESSED_OUTSIDE_OBJECT=false,
		     OBJ_TOUCH_STATE_RELEASED_ON_OBJECT=false,
		     OBJ_TOUCH_STATE_RELEASED_OUTSIDE_OBJECT=false,
		     OBJ_TOUCH_STATE_IS_PRESSED_ON_OBJECT=false,
		     OBJ_TOUCH_STATE_IS_PRESSED=false,
		     OBJ_TOUCH_STATE_CLICK_ON_OBJECT=false}, -- TOUCH_STATE_INIT
      type = "OBJ_TYPE_TEXTBOX",
      event = "OBJ_EVENT_NONE",
      a_rel = {xs=xs, ys=ys, xe=xe, ye=ye},
      a_abs = {xs=-1, ys=-1, xe=-1, ye=-1},
      id = id,
      state = {OBJ_STATE_VISIBLE=true,
	       OBJ_STATE_REDRAW=true,
	       OBJ_STATE_VALID=true},
      data = txb
   }

   -- Update function: Do your thing!
   obj.state["OBJ_STATE_FREE"] = false

   -- Insert object in window's object list
   wnd.objlst[id] = obj

   return true
   
end

--- Deletes a textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @treturn bool Result of the function
local function UG_TextboxDelete( wnd, id )
   return _UG_DeleteObject( wnd, "OBJ_TYPE_TEXTBOX", id )
end

--- Shows a textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @treturn bool Result of the function
local function UG_TextboxShow( wnd, id )
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )

   if obj then
      obj.state["OBJ_STATE_VISIBLE"] = true
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Hides a textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @treturn bool Result of the function
local function UG_TextboxHide( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )

   if obj then
      obj.state["OBJ_STATE_VISIBLE"] = false
      obj.state["OBJ_STATE_UPDATE"] = true
      return true
   end
   return false   
   
end

--- Changes the fore color of the textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @param fc Fore color
-- @treturn bool Result of the function
local function UG_TextboxSetForeColor( wnd, id, fc )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      txb.fc = fc
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Changes the back color of the textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @param bc Back color
-- @treturn bool Result of the function
local function UG_TextboxSetBackColor( wnd, id, bc )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      txb.bc = bc
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Changes the textbox text.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @param str Textbox text
-- @treturn bool Result of the function
local function UG_TextboxSetText( wnd, id, str )
  
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      txb.str = str
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Changes the textbox font.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @param font Textbox font
-- @treturn bool Result of the function
local function UG_TextboxSetFont( wnd, id, font )
  
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      txb.font = font
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Changes the horizontal space between the characters in the textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @param hs Horizontal space
-- @treturn bool Result of the function
local function UG_TextboxSetHSpace( wnd, id, hs )
  
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      txb.h_space = hs
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false

end

--- Changes the vertical space between the characters in the textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @param vs Vertical space
-- @treturn bool Result of the function
local function UG_TextboxSetVSpace( wnd, id, vs )
  
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      txb.v_space = vs
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false

end

--- Changes the alignment of the text in the textbox.
-- The following alignments are available:
-- <ul>
-- <li>ALIGN_NONE</li>
-- <li>ALIGN_BOTTOM_RIGHT</li>
-- <li>ALIGN_BOTTOM_CENTER</li>
-- <li>ALIGN_BOTTOM_LEFT</li>
-- <li>ALIGN_CENTER_RIGHT</li>
-- <li>ALIGN_CENTER</li>
-- <li>ALIGN_CENTER_LEFT</li>
-- <li>ALIGN_TOP_RIGHT</li>
-- <li>ALIGN_TOP_CENTER</li>
-- <li>ALIGN_TOP_LEFT</li>
-- </ul>
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @param align Alignment
-- @treturn bool Result of the function
local function UG_TextboxSetAlignment( wnd, id, align )
    
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      txb.align = txb.align
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Returns the fore color of the textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @return Fore color of the textbox
local function UG_TextboxGetForeColor( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      return (txb.fc or C_BLACK)
   end

end

--- Returns the back color of the textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @return Back color of the textbox
local function UG_TextboxGetBackColor( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      return (txb.bc or C_BLACK)
   end

end

--- Returns the textbox text.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @return Textbox text
local function UG_TextboxGetText( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      return txb.str
   end
   
end
   
--- Returns the textbox font.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @return Textbox font
local function UG_TextboxGetFont( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      return txb.font
   end
   
end

--- Returns the horizontal space between the characters in the textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @return Horizontal space
local function UG_TextboxGetHSpace( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      return (txb.h_space or 0)
   end

end

--- Returns the vertical space between the characters in the textbox.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @return Vertical space
local function UG_TextboxGetVSpace( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      return (txb.v_space or 0)
   end

end

--- Returns the alignment of the textbox text.
-- @param wnd Window object which contains the textbox
-- @param id Textbox ID
-- @return Alignment
local function UG_TextboxGetAlignment( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_TEXTBOX", id )
   if obj then
      local txb = obj.data
      return (txb.align or 0)
   end

end

--- Image Functions
-- @section imgfnc

--- Private Image update function
local function _UG_ImageUpdate( wnd, obj )

   local img = obj.data

   -- Object touch section
   --
   -- Textbox doesn't support touch

   -- Object update section
   if obj.state["OBJ_STATE_UPDATE"] then
      if obj.state["OBJ_STATE_VISIBLE"] then
	 -- Full redraw necessary?
	 if obj.state["OBJ_STATE_REDRAW"] then
	    local a = UG_WindowGetArea(wnd)
	    -- @todo more/better image features
	    obj.a_abs.xs = obj.a_rel.xs + a.xs
            obj.a_abs.ys = obj.a_rel.ys + a.ys
            obj.a_abs.xe = obj.a_rel.xs + img.img.width + a.xs
            obj.a_abs.ye = obj.a_rel.ys + img.img.height + a.ys
	    if (obj.a_abs.ye >= wnd.ye) then return end
	    if (obj.a_abs.xe >= wnd.xe) then return end

	    -- Draw Image

	    if img.img and (img.type == "IMG_TYPE_BMP") then
	       UG_DrawBMP(obj.a_abs.xs, obj.a_abs.ys, img.img)
	    end
	    obj.state["OBJ_STATE_REDRAW"] = false
	 end
      else
	 UG_FillFrame(obj.a_abs.xs, obj.a_abs.ys,
		      obj.a_abs.xe, obj.a_abs.ye, wnd.bc)
      end
      obj.state["OBJ_STATE_UPDATE"] = false
   end
	    
end

--- Creates an image.
-- @param wnd Window object which contains the image
-- @param  img The image
-- @param  id Image ID
-- @param  xs X start position of the image
-- @param  ys Y start position of the image
-- @param  xe X end position of the image
-- @param  ye Y end position of the image
-- @treturn bool Result of the function
local function UG_ImageCreate( wnd, img, id, xs, ys, xe, ye )

   -- Initialize object-specific parameters
   img.img = nil
   img.type = "IMG_TYPE_BMP"

   -- Initialize standard object parameters
   local obj = {
      update = _UG_ImageUpdate, 
      touch_state = {OBJ_TOUCH_STATE_CHANGED=false,
		     OBJ_TOUCH_STATE_PRESSED_ON_OBJECT=false,
		     OBJ_TOUCH_STATE_PRESSED_OUTSIDE_OBJECT=false,
		     OBJ_TOUCH_STATE_RELEASED_ON_OBJECT=false,
		     OBJ_TOUCH_STATE_RELEASED_OUTSIDE_OBJECT=false,
		     OBJ_TOUCH_STATE_IS_PRESSED_ON_OBJECT=false,
		     OBJ_TOUCH_STATE_IS_PRESSED=false,
		     OBJ_TOUCH_STATE_CLICK_ON_OBJECT=false}, -- TOUCH_STATE_INIT
      type = "OBJ_TYPE_IMAGE",
      event = "OBJ_EVENT_NONE",
      a_rel = {xs=xs, ys=ys, xe=xe, ye=ye},
      a_abs = {xs=-1, ys=-1, xe=-1, ye=-1},
      id = id,
      state = {OBJ_STATE_VISIBLE=true,
	       OBJ_STATE_REDRAW=true,
	       OBJ_STATE_VALID=true,
	       OBJ_STATE_TOUCH_ENABLE=true},
      data = img
   }
   
   -- Update function: Do your thing!
   obj.state["OBJ_STATE_FREE"] = false

   -- Insert object in window's object list
   wnd.objlst[id] = obj

   return true
   
end

--- Deletes an image.
-- @param wnd Window object which contains the image
-- @param id Image ID
-- @treturn bool Result of the function
local function UG_ImageDelete( wnd, id )
   return _UG_DeleteObject( wnd, "OBJ_TYPE_IMAGE", id )
end

--- Shows an image.
-- @param wnd Window object which contains the image
-- @param id Image ID
-- @treturn bool Result of the function
local function UG_ImageShow( wnd, id )
   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_IMAGE", id )

   if obj then
      obj.state["OBJ_STATE_VISIBLE"] = true
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- Hides an image.
-- @param wnd Window object which contains the image
-- @param id Image ID
-- @treturn bool Result of the function
local function UG_ImageHide( wnd, id )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_IMAGE", id )

   if obj then
      obj.state["OBJ_STATE_VISIBLE"] = false
      obj.state["OBJ_STATE_UPDATE"] = true
      return true
   end
   return false   
   
end

--- Links an μGUI bitmap (UG_BMP) to the image object.
-- @param wnd Window object which contains the image
-- @param id Image ID
-- @param bmp the μ GUI bitmap
-- @treturn bool Result of the function
local function UG_ImageSetBMP(  wnd, id, bmp )

   local obj = _UG_SearchObject( wnd, "OBJ_TYPE_IMAGE", id )

   if obj then
      local img = obj.data
      img.img = bmp
      img.type = "IMG_TYPE_BMP"
      obj.state["OBJ_STATE_UPDATE"] = true
      obj.state["OBJ_STATE_REDRAW"] = true
      return true
   end
   return false
   
end

--- @export
return {
   C = C,
   rgb32split = rgb32split,
   rgb16split = rgb16split,
   rgb32to16 = rgb32to16,
   rgb16to32 = rgb16to32,
   get_gui = get_gui,
   UG_Update = UG_Update, -- Miscellaneous Functions
   UG_TouchUpdate = UG_TouchUpdate,
   UG_Init = UG_Init, -- Classic Functions
   UG_SelectGUI = UG_SelectGUI,
   UG_FontSelect = UG_FontSelect,
   UG_FillScreen = UG_FillScreen,
   UG_FillFrame = UG_FillFrame,
   UG_FillRoundFrame = UG_FillRoundFrame,
   UG_DrawMesh = UG_DrawMesh,
   UG_DrawFrame = UG_DrawFrame,
   UG_DrawRoundFrame = UG_DrawRoundFrame,
   UG_DrawPixel = UG_DrawPixel,
   UG_DrawCircle = UG_DrawCircle,
   UG_FillCircle = UG_FillCircle,
   UG_DrawArc = UG_DrawArc,
   UG_DrawLine = UG_DrawLine,
   UG_DrawBMP = UG_DrawBMP,
   UG_PutChar = UG_PutChar,
   UG_PutString = UG_PutString,
   UG_ConsolePutString = UG_ConsolePutString,
   UG_ConsoleSetArea = UG_ConsoleSetArea,
   UG_ConsoleSetForecolor = UG_ConsoleSetForecolor,
   UG_ConsoleSetBackcolor = UG_ConsoleSetBackcolor,
   UG_SetForecolor = UG_SetForecolor,
   UG_SetBackcolor = UG_SetBackcolor,
   UG_GetXDim = UG_GetXDim,
   UG_GetYDim = UG_GetYDim,
   UG_FontSetHSpace = UG_FontSetHSpace,
   UG_FontSetVSpace = UG_FontSetVSpace,
   UG_DriverRegister = UG_DriverRegister, -- Driver Functions
   UG_DriverEnable = UG_DriverEnable,
   UG_DriverDisable = UG_DriverDisable,
   UG_WindowCreate = UG_WindowCreate, -- Window Functions
   UG_WindowDelete = UG_WindowDelete,
   UG_WindowShow = UG_WindowShow,
   UG_WindowHide = UG_WindowHide,
   UG_WindowResize = UG_WindowResize,
   UG_WindowAlert = UG_WindowAlert,
   UG_WindowSetForeColor = UG_WindowSetForeColor,
   UG_WindowSetBackColor = UG_WindowSetBackColor,
   UG_WindowSetTitleTextColor = UG_WindowSetTitleTextColor,
   UG_WindowSetTitleColor = UG_WindowSetTitleColor,
   UG_WindowSetTitleInactiveTextColor = UG_WindowSetTitleInactiveTextColor,
   UG_WindowSetTitleInactiveColor = UG_WindowSetTitleInactiveColor,
   UG_WindowSetTitleText = UG_WindowSetTitleText,
   UG_WindowSetTitleTextFont = UG_WindowSetTitleTextFont,
   UG_WindowSetTitleTextHSpace = UG_WindowSetTitleTextHSpace,
   UG_WindowSetTitleTextVSpace = UG_WindowSetTitleTextVSpace,
   UG_WindowSetTitleTextAlignment = UG_WindowSetTitleTextAlignment,
   UG_WindowSetTitleHeight = UG_WindowSetTitleHeight,
   UG_WindowSetXStart = UG_WindowSetXStart,
   UG_WindowSetYStart = UG_WindowSetYStart,
   UG_WindowSetXEnd = UG_WindowSetXEnd,
   UG_WindowSetYEnd = UG_WindowSetYEnd,
   UG_WindowSetStyle = UG_WindowSetStyle,
   UG_WindowGetForeColor = UG_WindowGetForeColor,
   UG_WindowGetBackColor = UG_WindowGetBackColor,
   UG_WindowGetTitleTextColor = UG_WindowGetTitleTextColor,
   UG_WindowGetTitleColor = UG_WindowGetTitleColor,
   UG_WindowGetTitleInactiveTextColor = UG_WindowGetTitleInactiveTextColor,
   UG_WindowGetTitleInactiveColor = UG_WindowGetTitleInactiveColor,
   UG_WindowGetTitleText = UG_WindowGetTitleText,
   UG_WindowGetTitleTextFont = UG_WindowGetTitleTextFont,
   UG_WindowGetTitleTextHSpace = UG_WindowGetTitleTextHSpace,
   UG_WindowGetTitleTextVSpace = UG_WindowGetTitleTextVSpace,
   UG_WindowGetTitleTextAlignment = UG_WindowGetTitleTextAlignment,
   UG_WindowGetTitleHeight = UG_WindowGetTitleHeight,
   UG_WindowGetXStart = UG_WindowGetXStart,
   UG_WindowGetYStart = UG_WindowGetYStart,
   UG_WindowGetXEnd = UG_WindowGetXEnd,
   UG_WindowGetYEnd = UG_WindowGetYEnd,
   UG_WindowGetStyle = UG_WindowGetStyle,
   UG_WindowGetArea = UG_WindowGetArea,
   UG_WindowGetInnerWidth = UG_WindowGetInnerWidth,
   UG_WindowGetOuterWidth = UG_WindowGetOuterWidth,
   UG_WindowGetInnerHeight = UG_WindowGetInnerHeight,
   UG_WindowGetOuterHeight = UG_WindowGetOuterHeight,
   UG_ButtonCreate = UG_ButtonCreate, -- Button functions
   UG_ButtonDelete = UG_ButtonDelete,
   UG_ButtonShow = UG_ButtonShow,
   UG_ButtonHide = UG_ButtonHide,
   UG_ButtonSetForeColor = UG_ButtonSetForeColor,
   UG_ButtonSetBackColor = UG_ButtonSetBackColor,
   UG_ButtonSetAlternateForeColor = UG_ButtonSetAlternateForeColor,
   UG_ButtonSetAlternateBackColor = UG_ButtonSetAlternateBackColor,
   UG_ButtonSetText = UG_ButtonSetText,
   UG_ButtonSetFont = UG_ButtonSetFont,
   UG_ButtonSetStyle = UG_ButtonSetStyle,
   UG_ButtonGetForeColor = UG_ButtonGetForeColor,
   UG_ButtonGetBackColor = UG_ButtonGetBackColor,
   UG_ButtonGetAlternateForeColor = UG_ButtonGetAlternateForeColor,
   UG_ButtonGetAlternateBackColor = UG_ButtonGetAlternateBackColor,
   UG_ButtonGetText = UG_ButtonGetText,
   UG_ButtonGetFont = UG_ButtonGetFont,
   UG_ButtonGetStyle = UG_ButtonGetStyle,
   UG_TextboxCreate = UG_TextboxCreate, -- Textbox functions
   UG_TextboxDelete = UG_TextboxDelete,
   UG_TextboxShow = UG_TextboxShow,
   UG_TextboxHide = UG_TextboxHide,
   UG_TextboxSetForeColor = UG_TextboxSetForeColor,
   UG_TextboxSetBackColor = UG_TextboxSetBackColor,
   UG_TextboxSetText = UG_TextboxSetText,
   UG_TextboxSetFont = UG_TextboxSetFont,
   UG_TextboxSetHSpace = UG_TextboxSetHSpace,
   UG_TextboxSetVSpace = UG_TextboxSetVSpace,
   UG_TextboxSetAlignment = UG_TextboxSetAlignment,
   UG_TextboxGetForeColor = UG_TextboxGetForeColor,
   UG_TextboxGetBackColor = UG_TextboxGetBackColor,
   UG_TextboxGetText = UG_TextboxGetText,
   UG_TextboxGetFont = UG_TextboxGetFont,
   UG_TextboxGetHSpace = UG_TextboxGetHSpace,
   UG_TextboxGetVSpace = UG_TextboxGetVSpace,
   UG_TextboxGetAlignment = UG_TextboxGetAlignment,
   UG_ImageCreate = UG_ImageCreate, -- Image functions
   UG_ImageDelete = UG_ImageDelete,
   UG_ImageShow = UG_ImageShow,
   UG_ImageHide = UG_ImageHide,
   UG_ImageSetBMP = UG_ImageSetBMP
}
