-- unload and reload uguic to use newer version
package.loaded.uguic = nil
uguic = require("uguic")

-- close previuous tekui visual if open
if v then v:close() end

ui = require "tek.ui"
visual = require "tek.lib.visual"

pngImage = require ("png")

-- useful table print function
function tprint (t, s)
   for k, v in pairs(t) do
      local kfmt = '["' .. tostring(k) ..'"]'
      if type(k) ~= 'string' then
	 kfmt = '[' .. k .. ']'
      end
      local vfmt = '"'.. tostring(v) ..'"'
      if type(v) == 'table' then
	 tprint(v, (s or '')..kfmt)
      else
	 if type(v) ~= 'string' then
	    vfmt = tostring(v)
	 end
	 print(type(t)..(s or '')..kfmt..' = '..vfmt)
      end
   end
end

-- load fonts
font_4x6 = require "ugui-font4x6"
font_6x10 = require "ugui-font6x10"
font_8x8 = require "ugui-font8x8"
font_8x12 = require "ugui-font8x12"
font_12x16 = require "ugui-font12x16"

-- create tekui visual object
v_width = 240
v_height = 240
v = visual.open { Title="uguic.lua demo", Width = v_width, Height=v_height,
		  MinWidth=v_width, MinHeight=v_height,
		  MaxWidth=v_width, MaxHeight=v_height }

function upsf(x,y,c)
   local rgb = uguic.splitIntegerBits(c,{8,8,8})
   local pen = v:allocPen(0,rgb[1],rgb[2],rgb[3])
   v:drawPoint(x,y,pen)
end

function tek_fillscreen(params)
   local c = params.c
   local rgb = uguic.splitIntegerBits(c,{8,8,8})
   local pen = v:allocPen(0,rgb[1],rgb[2],rgb[3])
   v:fillRect(0,0,239,239,pen)
   return true
end

local function tek_fillframe(params)
   local x1, y1 = params.x1, params.y1
   local x2, y2 = params.x2, params.y2
   local c = params.c
   local rgb = uguic.splitIntegerBits(c,{8,8,8})
   local pen = v:allocPen(0,rgb[1],rgb[2],rgb[3])
   v:fillRect(x1, y1, x2, y2, pen)
   return true
end

local function tek_drawline(params)
   local x1, y1 = params.x1, params.y1
   local x2, y2 = params.x2, params.y2
   local c = params.c
   local rgb = uguic.splitIntegerBits(c,{8,8,8})
   local pen = v:allocPen(0,rgb[1],rgb[2],rgb[3])
   v:drawLine(x1, y1, x2, y2, pen)
   return true
end   

local function tek_drawframe(params)
   local x1, y1 = params.x1, params.y1
   local x2, y2 = params.x2, params.y2
   local c = params.c
   local rgb = uguic.splitIntegerBits(c,{8,8,8})
   local pen = v:allocPen(0,rgb[1],rgb[2],rgb[3])
   v:drawRect(x1, y1, x2, y2, pen)
   return true
end

mainGUI = {}
print(uguic.UG_GUI("init", {g=mainGUI, p=upsf, x=240, y=240}))

mainGUI.debug = {classic = false,
		 window = false,
		 internal = false,
		 misc = false}

print(uguic.UG_Driver("register",
		      {driver_type="FILL_SCREEN", driver=tek_fillscreen}))

print(uguic.UG_Driver("register",
		      {driver_type="FILL_FRAME", driver=tek_fillframe}))

print(uguic.UG_Driver("register",
		      {driver_type="DRAW_LINE", driver=tek_drawline}))

print(uguic.UG_Driver("register",
		      {driver_type="DRAW_FRAME", driver=tek_drawframe}))

-- clear screen to white
uguic.UG_Fill("Screen", {c=uguic.C.WHITE})

-- Show all colors
function showAllColors()
   uguic.UG_Fill("Screen", {c=uguic.C.DARK_GRAY})
   local row = 0
   local col = 0
   for k,v in pairs(uguic.C) do
      uguic.UG_Fill("Frame", {x1=col,    y1 =row,
			      x2=col+20, y2=row+20,
			      c=v})
      col = col + 20
      if col >= 240 then
	 col = 0
	 row = row + 20
	 if row >= 240 then return end
      end
   end
end

-- Show classic functions
function drawStuff(bmp)
   -- Clear Screen
   uguic.UG_Fill("Screen", {c=uguic.C.WHITE})
   
   -- Draw functions
   uguic.UG_Draw("Mesh", {x1=10, y1=10, x2=80,y2=60,
			  c=uguic.C.FOREST_GREEN})
   uguic.UG_Draw("Frame", {x1=85, y1=10, x2=155, y2=60,
			   c=uguic.C.FOREST_GREEN})
   uguic.UG_Draw("RoundFrame", {x1=160, y1=10, x2=230, y2=60,r=10,
				c=uguic.C.FOREST_GREEN})
   uguic.UG_Draw("Circle", {x0=35, y0=90, r=25,
			    c=uguic.C.FOREST_GREEN})
   uguic.UG_Draw("Arc", {x0=75, y0=65, r=20, s=0x80,
			 c=uguic.C.FOREST_GREEN})
   uguic.UG_Draw("Arc", {x0=75, y0=65, r=20, s=0x40,
			 c=uguic.C.RED})
   uguic.UG_Draw("Arc", {x0=125, y0=65, r=20, s=0x20,
			 c=uguic.C.FOREST_GREEN})
   uguic.UG_Draw("Arc", {x0=125, y0=65, r=20, s=0x10,
			 c=uguic.C.RED})
   uguic.UG_Draw("Arc", {x0=75, y0=115, r=20, s=0x02,
			 c=uguic.C.FOREST_GREEN})
   uguic.UG_Draw("Arc", {x0=75, y0=115, r=20, s=0x01,
			 c=uguic.C.RED})
   uguic.UG_Draw("Arc", {x0=125, y0=115, r=20, s=0x08,
			 c=uguic.C.FOREST_GREEN})
   uguic.UG_Draw("Arc", {x0=125, y0=115, r=20, s=0x04,
			 c=uguic.C.RED})
   uguic.UG_Draw("Line", {x1=130, y1=65, x2=185, y2=100,
			  c=uguic.C.FOREST_GREEN})
   uguic.UG_Draw("Line", {x1=130, y1=115, x2=170, y2=65,
			  c=uguic.C.RED})
   uguic.UG_Draw("Triangle", {x0=180, y0=65,
			      x1=230, y1=75,
			      x2=195, y2=115,
			      c=uguic.C.FOREST_GREEN})
   if bmp then
      uguic.UG_Draw("BMP", {x0=10, y0=175, bmp=bmp})
      uguic.UG_Draw("Frame", {x1=10, y1=175,
			      x2=10+bmp.width, y2=175+bmp.height,
			      c=uguic.C.BLACK})
   end
   -- Fill functions
   uguic.UG_Fill("Circle", {x0=45, y0=145, r=25,
			    c=uguic.C.MEDIUM_PURPLE})
   uguic.UG_Fill("Frame", {x1=85, y1=120, x2=155, y2=170,
			   c=uguic.C.SEA_GREEN})
   uguic.UG_Fill("RoundFrame", {x1=160, y1=120, x2=230, y2=170, r=10,
				c=uguic.C.DARK_ORANGE})
   
   -- Text functions
   uguic.UG_Text("selectFont", {font=font_8x8})
   uguic.UG_Text("setForecolor", {c=uguic.C.BLACK})
   -- uguic.UG_Text("SetBackcolor", {c=uguic.C.WHITE})
   -- transparent text background ", {new feature})
   uguic.UG_Text("setBackcolor", {nil})
   uguic.UG_Text("putString", {x=20, y=20, str="Mesh"})
   uguic.UG_Text("putString", {x=90, y=20, str="Frame"})
   uguic.UG_Text("putString", {x=170, y=20, str="Round\nFrame"})
   uguic.UG_Text("putString", {x=20, y=75, str="Cir\ncle"})
   uguic.UG_Text("putString", {x=85, y=85, str="Arcs"})
   uguic.UG_Text("putString", {x=140, y=100, str="Lines"})
   uguic.UG_Text("putString", {x=185, y=70, str="Tri\nangle"})
   if bmp then
      uguic.UG_Text("putString", {x=15, y=2+175+bmp.height, str="BMP"}) end
   uguic.UG_Text("setForecolor", {c=uguic.C.YELLOW})
   uguic.UG_Text("putString", {x=30, y=130, str="Fill\ned\ncir\ncle"})
   uguic.UG_Text("putString", {x=90, y=125, str="Filled\nframe"})
   uguic.UG_Text("putString", {x=170, y=130, str="Filled\nround\nframe"})
   uguic.UG_Text("setBackcolor", {c=uguic.C.WHITE})
   
   -- Console
   local console_xs
   if bmp then
      uguic.UG_Text("selectFont", {font=font_8x12})
      console_xs = 90
   else
      uguic.UG_Text("selectFont", {font=font_8x12})
      console_xs = 10
   end
   uguic.UG_Console("setBackcolor", {c=uguic.C.GAINSBORO})
   uguic.UG_Console("setForecolor ", {c=0xDB4B4B})
   uguic.UG_Console("setArea", {xs=console_xs, ys=175, xe=230, ye=230})
   uguic.UG_Console("putString", {str="Console Area\n------------\n"})
   local guiDim = uguic.UG_GUI("getDim")
   uguic.UG_Console("putString", {str="Screen: "..guiDim.x})
   uguic.UG_Console("putString", {str="x"..guiDim.y})
end



-- tekui input processing
-- react to mouse
v:setInput(0x0400)
function getInput()
   visual.wait()
   local msg = visual.getMsg()
   while msg do
      print("msg available")
      local msg_type, msg_code, mxw, myw, mxs, mys = msg[2], msg[3], msg[4], msg[5], msg[11], msg[12]
      print("msg:", msg_type, msg_code, mxw, myw, mxs, mys)
      if msg_type == 1024 then
	 -- MouseButton
	 print("msg is mousebutton")
	 if msg_code == 1 or msg_code == 4 or msg_code == 16 then
	    uguic.UG_TouchUpdate(mxw, myw, "PRESSED")
	    uguic.UG_Update()
	 elseif msg_code == 2 or msg_code == 8 or msg_code == 32 then
	    uguic.UG_TouchUpdate(-1, -1, "RELEASED")
	    uguic.UG_Update()
	 end
      else
	 print("msg is something else")
      end
      msg = visual.getMsg()
   end
end

-- load png
test_image_32 = "images/Lua-Logo/Lua-Logo_32x32_whitebg_ft00.png"
test_image_64 = "images/Lua-Logo/Lua-Logo_64x64_whitebg_ft00.png"
test_image_val = "images/valdivia/valdivia_45x70_ft00.png"
function load_png_as_bmp (pngfile)
   local image_array = {}
   local color, height, width
   local function populateImage(rowNum, rowTotal, rowPixels)
      height = rowTotal
      for p,color_data in pairs(rowPixels) do
	 color = color_data["R"]*2^16+color_data["G"]*2^8+color_data["B"]
	 table.insert(image_array, uguic.rgb32to16(color))
	 width = p
      end
   end
   local image_raw = pngImage(pngfile, populateImage, true, true)
   return {p=image_array, width=width, height=height, bpp="BMP_BPP_16", colors="BMP_RGB565"}
end
   
-- Show windows
function showWindow(bmp)
   print("FillScreen:", uguic.UG_Fill("Screen", {c=uguic.C.WHITE}))
   mainWindow = {}
   local function mainWindow_cb (msg)
      print("Msg type: ", msg.msg_type)
      print("Msg src type: ", msg.src_type)
      print("Msg src id", msg.src_id)
   end
   -- create window and configure its title
   print("Create:",
	 uguic.UG_Window(mainWindow, "create", {cb=mainWindow_cb}))
   print("TitleHeight:",
	 uguic.UG_WindowSet(mainWindow, "TitleHeight", 16))
   print("TitleFont:",
	 uguic.UG_WindowSet(mainWindow, "TitleTextFont", font_8x12))
   print("TitleText:",
	 uguic.UG_WindowSet(mainWindow, "TitleText", "Window title"))
   -- Button widget
   button_id = "btn01"
   print("ButtonCreate:",
	 uguic.UG_Button(mainWindow, "create", button_id,
			 {xs=10, ys=50, xe=100, ye=100}))
   print("ButtonFont:",
	 uguic.UG_ButtonSet(mainWindow, button_id, "Font", font_8x12))
   print("ButtonText:",
	 uguic.UG_ButtonSet(mainWindow, button_id, "Text", "Ok"))
   -- Textbox widget
   textbox_id = "txt01"
   print("TextboxCreate:",
	 uguic.UG_Textbox(mainWindow, "create", textbox_id,
			 {xs=10, ys=110, xe=230, ye=210}))
   print("TextboxFont:",
	 uguic.UG_TextboxSet(mainWindow, textbox_id, "Font", font_8x12))
   print("TextboxText:",
	 uguic.UG_TextboxSet(mainWindow, textbox_id, "Text", "Textbox text"))
   print("TextboxForeColor:",
	 uguic.UG_TextboxSet(mainWindow, textbox_id,
			     "ForeColor", uguic.C.BLUE))
   print("TextboxAlign:",
	 uguic.UG_TextboxSet(mainWindow, textbox_id,
			     "Alignment", {v="BOTTOM", h="RIGHT"}))
   -- Image widget
   mainGUI.debug = {classic = true,
		    window = true,
		    internal = true,
		    misc = true}
   if bmp then
      image_id = "img01"
      bmp_x = 230-bmp.width
      bmp_y = 10
      print("ImageCreate:",
	    uguic.UG_Image(mainWindow, "create", image_id,
			   {xs=bmp_x, ys=bmp_y,
			    xe=bmp_x+bmp.width, ye=bmp_y+bmp.height}))
      print("ImageBMP:",
	    uguic.UG_ImageSet(mainWindow, image_id, "BMP", bmp))
   end
   -- Show window and update screen
   print("Show:", uguic.UG_Window(mainWindow, "show"))
   print("(update)", uguic.UG_Update())
   mainGUI.debug = {classic = false,
		    window = false,
		    internal = false,
		    misc = false}
   -- getInput()
   
end
   
-- test image
function testPNG(pngfile)
   local image_raw = pngImage(pngfile, nil, true, false)
   local image_array = {}
   for row, row_data in pairs(image_raw.pixels) do
      local color
      for c, color_data in pairs(row_data) do
	 color = color_data["R"]*2^16+color_data["G"]*2^8+color_data["B"]
	 table.insert(image_array, uguic.rgb32to16(color))
      end
   end
   local bitmap = {p=image_array, width=image_raw.width, height=image_raw.height, bpp="BMP_BPP_16", colors="BMP_RGB565"}
   uguic.UG_Draw("BMP",{x0=10,y0=10,bmp=bitmap})
end

function testSynthImg()
   local imagen = {}
   local w = 64
   local h = 64
   for row = 1,h,1 do
      for col = 1,w,1 do
	 if row>0.33*h and row<0.67*h and col>0.33*w and col<0.67*w then
	    table.insert(imagen, uguic.rgb32to16(uguic.C.YELLOW))
	 elseif row>0.33*h and row<0.67*h then
	    table.insert(imagen, uguic.rgb32to16(uguic.C.BLUE))
	 elseif col>0.33*w and col<0.67*w then
	    table.insert(imagen, uguic.rgb32to16(uguic.C.RED))
	 else
	    table.insert(imagen, uguic.rgb32to16(uguic.C.WHITE))
	 end
      end
   end
   bitmap = {p=imagen, width=w, height=h, bpp="BMP_BPP_16", colors="BMP_RGB565"}
   uguic.UG_Draw("BMP",{x0=10,y0=10,bmp=bitmap})
end
