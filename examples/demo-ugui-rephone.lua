#!/usr/bin/env lua

function tprint (t, s)
    for k, v in pairs(t) do
        local kfmt = '["' .. tostring(k) ..'"]'
        if type(k) ~= 'string' then
            kfmt = '[' .. k .. ']'
        end
        local vfmt = '"'.. tostring(v) ..'"'
        if type(v) == 'table' then
            tprint(v, (s or '')..kfmt)
        else
            if type(v) ~= 'string' then
                vfmt = tostring(v)
            end
            print(type(t)..(s or '')..kfmt..' = '..vfmt)
        end
    end
end

package.loaded.ugui = nil

ugui = require "ugui"
-- font_4x6 = require "ugui-font4x6"
-- package.loaded["ugui-font12x16"] = nil
font_12x16 = require "ugui-font12x16"
font_8x8 = require "ugui-font8x8"

res = lcd.init(8)
lcd.clear()

function upsf(x,y,c)
   local rgb16 = ugui.rgb32to16(c)
   lcd.putpixel(x,y,rgb16)
end

function rephone_fillscreen (c)
   local rgb16 = ugui.rgb32to16(c)
   lcd.clear(rgb16)
   return true
end

function rephone_fillframe (x1, y1, x2, y2, c)
   local rgb16 = ugui.rgb32to16(c)
   lcd.rect(x1,y1,x2-x1,y2-y1,rgb16,rgb16)
   return true
end

function rephone_drawline(x1, y1, x2, y2, c)
   local rgb16 = ugui.rgb32to16(c)
   lcd.line(x1, y1, x2, y2, rgb16)
   return true
end

function rephone_drawframe(x1, y1, x2, y2, c)
   local rgb16 = ugui.rgb32to16(c)
   lcd.rect(x1,y1,x2-x1,y2-y1,rgb16)
   return true
end

function rephone_drawcircle(x, y, r, c)
   local rgb16 = ugui.rgb32to16(c)
   lcd.circle(x,y,r,rgb16)
   return true
end

function rephone_fillcircle(x, y, r, c)
   local rgb16 = ugui.rgb32to16(c)
   lcd.circle(x,y,r,rgb16,rgb16)
   return true
end


mainGUI = {}
ugui.UG_Init(mainGUI, upsf,240,240)

ugui.UG_DriverRegister("DRIVER_FILL_SCREEN", rephone_fillscreen)
ugui.UG_DriverRegister("DRIVER_FILL_FRAME", rephone_fillframe)
ugui.UG_DriverRegister("DRIVER_DRAW_LINE", rephone_drawline)
ugui.UG_DriverRegister("DRIVER_DRAW_FRAME", rephone_drawframe)
ugui.UG_DriverRegister("DRIVER_DRAW_CIRCLE", rephone_drawcircle)
ugui.UG_DriverRegister("DRIVER_FILL_CIRCLE", rephone_fillcircle)

ugui.UG_FillScreen(ugui.C.WHITE)

-- Show all colors
function showAllColors()
   ugui.UG_FillScreen(ugui.C.DARK_GRAY)
   local row = 0
   local col = 0
   for k,v in pairs(ugui.C) do
      ugui.UG_FillFrame(col,row,col+20,row+20,v)
      col = col + 20
      if col >= 240 then
	 col = 0
	 row = row + 20
	 if row >= 240 then return end
      end
   end
end

-- Show classic functions
function drawStuff()
   -- Clear Screen
   ugui.UG_FillScreen(ugui.C.WHITE)
   -- Draw functions
   ugui.UG_DrawMesh(10,10,80,60,ugui.C.FOREST_GREEN)
   ugui.UG_DrawFrame(85,10,155,60,ugui.C.FOREST_GREEN)
   ugui.UG_DrawRoundFrame(160,10,230,60,10,ugui.C.FOREST_GREEN)
   ugui.UG_DrawCircle(45,90,25,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(85,65,20,0x80,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(85,65,20,0x40,ugui.C.RED)
   ugui.UG_DrawArc(155,65,20,0x20,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(155,65,20,0x10,ugui.C.RED)
   ugui.UG_DrawArc(85,115,20,0x02,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(85,115,20,0x01,ugui.C.RED)
   ugui.UG_DrawArc(155,115,20,0x08,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(155,115,20,0x04,ugui.C.RED)
   ugui.UG_DrawLine(160,65,230,100,ugui.C.FOREST_GREEN)
   ugui.UG_DrawLine(160,115,200,65,ugui.C.RED)
   -- Fill functions
   ugui.UG_FillCircle(45,145,25,ugui.C.MEDIUM_PURPLE)
   ugui.UG_FillFrame(85,120,155,170,ugui.C.SEA_GREEN)
   ugui.UG_FillRoundFrame(160,120,230,170,10,ugui.C.DARK_ORANGE)
   -- Text functions
   ugui.UG_FontSelect(font_8x8)
   ugui.UG_SetForecolor(ugui.C.BLACK)
   -- ugui.UG_SetBackcolor(ugui.C.WHITE)
   -- transparent text background (new feature)
   ugui.UG_SetBackcolor(nil)
   ugui.UG_PutString(20,20,"Mesh")
   ugui.UG_PutString(90,20,"Frame")
   ugui.UG_PutString(170,20,"Round\nFrame")
   ugui.UG_PutString(30,75,"Cir\ncle")
   ugui.UG_PutString(105,85,"Arcs")
   ugui.UG_PutString(170,100,"Lines")
   ugui.UG_SetForecolor(ugui.C.YELLOW)
   ugui.UG_PutString(30,130,"Fill\ned\ncir\ncle")
   ugui.UG_PutString(90,125,"Filled\nframe")
   ugui.UG_PutString(170,130,"Filled\nround\nframe")
   ugui.UG_SetBackcolor(ugui.C.WHITE)
   -- Console
   ugui.UG_FontSelect(font_12x16)
   ugui.UG_ConsoleSetBackcolor(ugui.C.GAINSBORO)
   ugui.UG_ConsoleSetForecolor ( 0xDB4B4B )
   ugui.UG_ConsoleSetArea (10,175,230,230)
   ugui.UG_ConsolePutString("Console Area\n------------\n")
   ugui.UG_ConsolePutString("Screen: "..ugui.UG_GetXDim())
   ugui.UG_ConsolePutString("x"..ugui.UG_GetYDim())
end
