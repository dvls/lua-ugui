#!/usr/bin/env lua

function tprint (t, s)
    for k, v in pairs(t) do
        local kfmt = '["' .. tostring(k) ..'"]'
        if type(k) ~= 'string' then
            kfmt = '[' .. k .. ']'
        end
        local vfmt = '"'.. tostring(v) ..'"'
        if type(v) == 'table' then
            tprint(v, (s or '')..kfmt)
        else
            if type(v) ~= 'string' then
                vfmt = tostring(v)
            end
            print(type(t)..(s or '')..kfmt..' = '..vfmt)
        end
    end
end

pngImage = require ("png")

package.loaded.ugui = nil

ugui = require "ugui"
ui = require "tek.ui"
font_4x6 = require "ugui-font4x6"
font_6x10 = require "ugui-font6x10"
font_8x8 = require "ugui-font8x8"
font_8x12 = require "ugui-font8x12"
font_12x16 = require "ugui-font12x16"

if v then v:close() end

visual = require "tek.lib.visual"
v = visual.open { Title="ugui.lua", Width = 240, Height=240,
		  MinWidth=240, MinHeight=240,
		  MaxWidth=240, MaxHeight=240 }
pen = v:allocPen(0,255,255,255)
v:drawRect(0,0,239,239,pen)
v:fillRect(0,0,239,239,pen)

function upsf(x,y,c)
   local red,green,blue = ugui.rgb32split(c)
   local pen = v:allocPen(0,red,green,blue)
   v:drawPoint(x,y,pen)
end

local function tek_fillscreen( c )
   local red, green, blue = ugui.rgb32split(c)
   local pen = v:allocPen(0,red,green,blue)
   v:fillRect(0,0,239,239,pen)
   return true
end

local function tek_fillframe(x1, y1, x2, y2, c)
   local red, green, blue = ugui.rgb32split(c)
   local pen = v:allocPen(0,red,green,blue)
   v:fillRect(x1, y1, x2, y2, pen)
   return true
end

local function tek_drawline(x1, y1, x2, y2, c)
   local red, green, blue = ugui.rgb32split(c)
   local pen = v:allocPen(0,red,green,blue)
   v:drawLine(x1, y1, x2, y2, pen)
   return true
end   

local function tek_drawframe(x1, y1, x2, y2, c)
   local red, green, blue = ugui.rgb32split(c)
   local pen = v:allocPen(0,red,green,blue)
   v:drawRect(x1, y1, x2, y2, pen)
   return true
end

mainGUI = {}
ugui.UG_Init(mainGUI, upsf,240,240)

ugui.UG_DriverRegister("DRIVER_FILL_SCREEN", tek_fillscreen)
ugui.UG_DriverRegister("DRIVER_FILL_FRAME", tek_fillframe)
ugui.UG_DriverRegister("DRIVER_DRAW_LINE", tek_drawline)
ugui.UG_DriverRegister("DRIVER_DRAW_FRAME", tek_drawframe)
ugui.UG_FillScreen(ugui.C.WHITE)

-- Show all colors
function showAllColors()
   ugui.UG_FillScreen(ugui.C.DARK_GRAY)
   local row = 0
   local col = 0
   for k,v in pairs(ugui.C) do
      ugui.UG_FillFrame(col,row,col+20,row+20,v)
      col = col + 20
      if col >= 240 then
	 col = 0
	 row = row + 20
	 if row >= 240 then return end
      end
   end
end

-- Show classic functions
function drawStuff(bmp)
   -- Clear Screen
   ugui.UG_FillScreen(ugui.C.WHITE)
   -- Draw functions
   ugui.UG_DrawMesh(10,10,80,60,ugui.C.FOREST_GREEN)
   ugui.UG_DrawFrame(85,10,155,60,ugui.C.FOREST_GREEN)
   ugui.UG_DrawRoundFrame(160,10,230,60,10,ugui.C.FOREST_GREEN)
   ugui.UG_DrawCircle(45,90,25,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(85,65,20,0x80,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(85,65,20,0x40,ugui.C.RED)
   ugui.UG_DrawArc(155,65,20,0x20,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(155,65,20,0x10,ugui.C.RED)
   ugui.UG_DrawArc(85,115,20,0x02,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(85,115,20,0x01,ugui.C.RED)
   ugui.UG_DrawArc(155,115,20,0x08,ugui.C.FOREST_GREEN)
   ugui.UG_DrawArc(155,115,20,0x04,ugui.C.RED)
   ugui.UG_DrawLine(160,65,230,100,ugui.C.FOREST_GREEN)
   ugui.UG_DrawLine(160,115,200,65,ugui.C.RED)
   if bmp then
      ugui.UG_DrawBMP(10, 175, bmp)
      ugui.UG_DrawFrame(10, 175,
			10+bmp.width, 175+bmp.height,
			ugui.C.BLACK)
   end
   -- Fill functions
   ugui.UG_FillCircle(45,145,25,ugui.C.MEDIUM_PURPLE)
   ugui.UG_FillFrame(85,120,155,170,ugui.C.SEA_GREEN)
   ugui.UG_FillRoundFrame(160,120,230,170,10,ugui.C.DARK_ORANGE)
   -- Text functions
   ugui.UG_FontSelect(font_8x8)
   ugui.UG_SetForecolor(ugui.C.BLACK)
   -- ugui.UG_SetBackcolor(ugui.C.WHITE)
   -- transparent text background (new feature)
   ugui.UG_SetBackcolor(nil)
   ugui.UG_PutString(20,20,"Mesh")
   ugui.UG_PutString(90,20,"Frame")
   ugui.UG_PutString(170,20,"Round\nFrame")
   ugui.UG_PutString(30,75,"Cir\ncle")
   ugui.UG_PutString(105,85,"Arcs")
   ugui.UG_PutString(170,100,"Lines")
   if bmp then
      ugui.UG_PutString(15, 2+175+bmp.height, "BMP") end
   ugui.UG_SetForecolor(ugui.C.YELLOW)
   ugui.UG_PutString(30,130,"Fill\ned\ncir\ncle")
   ugui.UG_PutString(90,125,"Filled\nframe")
   ugui.UG_PutString(170,130,"Filled\nround\nframe")
   -- Console
   local console_xs
   if bmp then
      ugui.UG_FontSelect(font_8x12)
      console_xs = 90
   else
      ugui.UG_FontSelect(font_8x12)
      console_xs = 10
   end
   ugui.UG_FontSelect(font_8x12)
   ugui.UG_ConsoleSetBackcolor(ugui.C.GAINSBORO)
   ugui.UG_ConsoleSetForecolor ( 0xDB4B4B )
   ugui.UG_ConsoleSetArea (console_xs,175,230,230)
   ugui.UG_ConsolePutString("Console Area\n------------\n")
   ugui.UG_ConsolePutString("Screen: "..ugui.UG_GetXDim())
   ugui.UG_ConsolePutString("x"..ugui.UG_GetYDim())
end



-- tekui input processing
-- react to mouse
v:setInput(0x0400)
function getInput()
   visual.wait()
   local msg = visual.getMsg()
   if msg then
      local msg_code, mxw, myw, mxs, mys = msg[3], msg[4], msg[5], msg[11], msg[12]
      ugui.UG_TouchUpdate(mxw, myw, 1)
      ugui.UG_Update()
      ugui.UG_TouchUpdate(-1, -1, 0)
      ugui.UG_Update()
   end
end

-- load png
test_image_32 = "images/Lua-Logo/Lua-Logo_32x32_whitebg_ft00.png"
test_image_64 = "images/Lua-Logo/Lua-Logo_64x64_whitebg_ft00.png"
test_image_val = "images/valdivia/valdivia_45x70_ft00.png"
function load_png_as_bmp (pngfile)
   local image_array = {}
   local color, height, width
   local function populateImage(rowNum, rowTotal, rowPixels)
      height = rowTotal
      for p,color_data in pairs(rowPixels) do
	 color = color_data["R"]*2^16+color_data["G"]*2^8+color_data["B"]
	 table.insert(image_array, ugui.rgb32to16(color))
	 width = p
      end
   end
   local image_raw = pngImage(pngfile, populateImage, true, true)
   return {p=image_array, width=width, height=height, bpp="BMP_BPP_16", colors="BMP_RGB565"}
end

-- Show windows
function showWindow(bmp)
   mainWindow = {}
   local function mainWindow_cb (msg)
      print("Msg type: ", msg.type)
      print("Msg id: ", msg.id)
      print("Msg sub_id", msg.sub_id)
   end

   print("Create:",
	 ugui.UG_WindowCreate(mainWindow, mainWindow_cb))
   print("mainWindow:",mainWindow)
   -- Window title setup
   print("TitleHeight:",
	 ugui.UG_WindowSetTitleHeight(mainWindow, 20))
   print("TitleFont:",
	 ugui.UG_WindowSetTitleTextFont(mainWindow, font_8x12))
   print("TitleText:",
	 ugui.UG_WindowSetTitleText(mainWindow, "Window title"))
   print("TitleTextAlign:",
	 ugui.UG_WindowSetTitleTextAlignment(mainWindow, "ALIGN_CENTER"))
   -- Button widget
   button = {}
   print("ButtonCreate:",
	 ugui.UG_ButtonCreate(mainWindow, button, "btn01", 10,50,100,100))
   print("ButtonFont:",
	 ugui.UG_ButtonSetFont(mainWindow, "btn01", font_8x12))
   print("ButtonText:",
	 ugui.UG_ButtonSetText(mainWindow, "btn01", "Ok"))
   print("ButtonShow:",
	 ugui.UG_ButtonShow(mainWindow, "btn01"))
   -- Textbox widget
   tb = {}
   print("TextboxCreate:",
	 ugui.UG_TextboxCreate(mainWindow, tb, "txt01",	10, 110, 230,210))
   print("TextboxFont:",
	 ugui.UG_TextboxSetFont(mainWindow, "txt01", font_8x12))
   print("TextboxText:",
	 ugui.UG_TextboxSetText(mainWindow, "txt01", "Textbox text"))
   print("TextboxForeColor:",
	 ugui.UG_TextboxSetForeColor(mainWindow, "txt01", ugui.C.BLUE))
   print("TextboxAlign:",
	 ugui.UG_TextboxSetAlignment(mainWindow, "txt01",
				     "ALIGN_BOTTOM_RIGHT"))
   -- Image widget
   mainGUI.debug = {classic = true,
		    window = true,
		    internal = true,
		    misc = true}
   if bmp then
      img = {}
      image_id = "img01"
      bmp_x = 230-bmp.width
      bmp_y = 10
      print("ImageCreate:",
	    ugui.UG_ImageCreate(mainWindow, img, image_id,
				bmp_x, bmp_y,
				bmp_x+bmp.width, bmp_y+bmp.height))
      print("ImageBMP:",
	    ugui.UG_ImageSetBMP(mainWindow, image_id, bmp))
   end
   print("Show:", ugui.UG_WindowShow(mainWindow))
   print("(update)", ugui.UG_Update())
   getInput()
end
   
-- test image
function testPNG(pngfile)
   local image_raw = pngImage(pngfile, nil, true, false)
   local image_array = {}
   for row, row_data in pairs(image_raw.pixels) do
      local color
      for c, color_data in pairs(row_data) do
	 color = color_data["R"]*2^16+color_data["G"]*2^8+color_data["B"]
	 table.insert(image_array, ugui.rgb32to16(color))
      end
   end
   local bitmap = {p=image_array, width=image_raw.width, height=image_raw.height, bpp="BMP_BPP_16", colors="BMP_RGB565"}
   ugui.UG_DrawBMP(10,10,bitmap)
end

function testSynthImg()
   local imagen = {}
   local w = 64
   local h = 64
   for row = 1,h,1 do
      for col = 1,w,1 do
	 if row>0.33*h and row<0.67*h and col>0.33*w and col<0.67*w then
	    table.insert(imagen, ugui.rgb32to16(ugui.C.YELLOW))
	 elseif row>0.33*h and row<0.67*h then
	    table.insert(imagen, ugui.rgb32to16(ugui.C.BLUE))
	 elseif col>0.33*w and col<0.67*w then
	    table.insert(imagen, ugui.rgb32to16(ugui.C.RED))
	 else
	    table.insert(imagen, ugui.rgb32to16(ugui.C.WHITE))
	 end
      end
   end
   bitmap = {p=imagen, width=w, height=h, bpp="BMP_BPP_16", colors="BMP_RGB565"}
   ugui.UG_DrawBMP(10,10,bitmap)
end


function manipulateWindows()
   -- @todo Test other window functions
   -- print("gui.windows: ", mainGUI.active_window, mainGUI.last_window, mainGUI.next_window)
   -- print("Show:",ugui.UG_WindowShow(mainWindow))
   -- print("Resize: ",ugui.UG_WindowResize(mainWindow, 20,20,200,200))
   -- print("(update)", ugui.UG_Update())
   -- print("TextboxShow:", ugui.UG_TextboxShow(mainWindow, "texto"))
end


