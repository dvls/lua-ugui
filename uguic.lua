#!/usr/bin/env lua

-- lua-uguic
-- Copyright (C) 2020  Daniel Vicente Lühr Sierra <dvls@trianguloaustral.cl>
-- 
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

--- Compact reimplementation of lua-ugui.
-- Source code available at:
-- <a href="https://gitlab.com/dvls/lua-ugui">
-- https://gitlab.com/dvls/lua-ugui
-- </a>
-- @license GNU General Public License version 3 or later
-- <a href="https://www.gnu.org/licenses/gpl.html">
-- https://www.gnu.org/licenses/gpl.html
-- </a>
-- @copyright 2020
-- @release 1.2
-- @author Daniel Vicente Lühr Sierra 
-- <a href="mailto:dvls@trianguloaustral.cl">
-- &lt;dvls@trianguloaustral.cl&gt;
-- </a>
-- @module uguic

-- ---------------------------------------------------------- --
-- µGUI COLORS                                                --
-- Source: http://www.rapidtables.com/web/color/RGB_Color.htm --
-- ---------------------------------------------------------- --

--- Table of colors
-- @table colors
local C={
MAROON                  =  0x800000,-- RGB888: 0x800000 <div style="display: inline-flex; background-color: #800000 ; min-width: 4em">&nbsp;</div>
DARK_RED                =  0x8B0000,-- RGB888: 0x8B0000
BROWN                   =  0xA52A2A,-- RGB888: 0xA52A2A
FIREBRICK               =  0xB22222,-- RGB888: 0xB22222
CRIMSON                 =  0xDC143C,-- RGB888: 0xDC143C
RED                     =  0xFF0000,-- RGB888: 0xFF0000
TOMATO                  =  0xFF6347,-- RGB888: 0xFF6347
CORAL                   =  0xFF7F50,-- RGB888: 0xFF7F50
INDIAN_RED              =  0xCD5C5C,-- RGB888: 0xCD5C5C
LIGHT_CORAL             =  0xF08080,-- RGB888: 0xF08080
DARK_SALMON             =  0xE9967A,-- RGB888: 0xE9967A
SALMON                  =  0xFA8072,-- RGB888: 0xFA8072
LIGHT_SALMON            =  0xFFA07A,-- RGB888: 0xFFA07A
ORANGE_RED              =  0xFF4500,-- RGB888: 0xFF4500
DARK_ORANGE             =  0xFF8C00,-- RGB888: 0xFF8C00
ORANGE                  =  0xFFA500,-- RGB888: 0xFFA500
GOLD                    =  0xFFD700,-- RGB888: 0xFFD700
DARK_GOLDEN_ROD         =  0xB8860B,-- RGB888: 0xB8860B
GOLDEN_ROD              =  0xDAA520,-- RGB888: 0xDAA520
PALE_GOLDEN_ROD         =  0xEEE8AA,-- RGB888: 0xEEE8AA
DARK_KHAKI              =  0xBDB76B,-- RGB888: 0xBDB76B
KHAKI                   =  0xF0E68C,-- RGB888: 0xF0E68C
OLIVE                   =  0x808000,-- RGB888: 0x808000
YELLOW                  =  0xFFFF00,-- RGB888: 0xFFFF00
YELLOW_GREEN            =  0x9ACD32,-- RGB888: 0x9ACD32
DARK_OLIVE_GREEN        =  0x556B2F,-- RGB888: 0x556B2F
OLIVE_DRAB              =  0x6B8E23,-- RGB888: 0x6B8E23
LAWN_GREEN              =  0x7CFC00,-- RGB888: 0x7CFC00
CHART_REUSE             =  0x7FFF00,-- RGB888: 0x7FFF00
GREEN_YELLOW            =  0xADFF2F,-- RGB888: 0xADFF2F
DARK_GREEN              =  0x006400,-- RGB888: 0x006400
GREEN                   =  0x00FF00,-- RGB888: 0x00FF00
FOREST_GREEN            =  0x228B22,-- RGB888: 0x228B22
LIME                    =  0x00FF00,-- RGB888: 0x00FF00
LIME_GREEN              =  0x32CD32,-- RGB888: 0x32CD32
LIGHT_GREEN             =  0x90EE90,-- RGB888: 0x90EE90
PALE_GREEN              =  0x98FB98,-- RGB888: 0x98FB98
DARK_SEA_GREEN          =  0x8FBC8F,-- RGB888: 0x8FBC8F
MEDIUM_SPRING_GREEN     =  0x00FA9A,-- RGB888: 0x00FA9A
SPRING_GREEN            =  0x00FF7F,-- RGB888: 0x00FF7F
SEA_GREEN               =  0x2E8B57,-- RGB888: 0x2E8B57
MEDIUM_AQUA_MARINE      =  0x66CDAA,-- RGB888: 0x66CDAA
MEDIUM_SEA_GREEN        =  0x3CB371,-- RGB888: 0x3CB371
LIGHT_SEA_GREEN         =  0x20B2AA,-- RGB888: 0x20B2AA
DARK_SLATE_GRAY         =  0x2F4F4F,-- RGB888: 0x2F4F4F
TEAL                    =  0x008080,-- RGB888: 0x008080
DARK_CYAN               =  0x008B8B,-- RGB888: 0x008B8B
AQUA                    =  0x00FFFF,-- RGB888: 0x00FFFF
CYAN                    =  0x00FFFF,-- RGB888: 0x00FFFF
LIGHT_CYAN              =  0xE0FFFF,-- RGB888: 0xE0FFFF
DARK_TURQUOISE          =  0x00CED1,-- RGB888: 0x00CED1
TURQUOISE               =  0x40E0D0,-- RGB888: 0x40E0D0
MEDIUM_TURQUOISE        =  0x48D1CC,-- RGB888: 0x48D1CC
PALE_TURQUOISE          =  0xAFEEEE,-- RGB888: 0xAFEEEE
AQUA_MARINE             =  0x7FFFD4,-- RGB888: 0x7FFFD4
POWDER_BLUE             =  0xB0E0E6,-- RGB888: 0xB0E0E6
CADET_BLUE              =  0x5F9EA0,-- RGB888: 0x5F9EA0
STEEL_BLUE              =  0x4682B4,-- RGB888: 0x4682B4
CORN_FLOWER_BLUE        =  0x6495ED,-- RGB888: 0x6495ED
DEEP_SKY_BLUE           =  0x00BFFF,-- RGB888: 0x00BFFF
DODGER_BLUE             =  0x1E90FF,-- RGB888: 0x1E90FF
LIGHT_BLUE              =  0xADD8E6,-- RGB888: 0xADD8E6
SKY_BLUE                =  0x87CEEB,-- RGB888: 0x87CEEB
LIGHT_SKY_BLUE          =  0x87CEFA,-- RGB888: 0x87CEFA
MIDNIGHT_BLUE           =  0x191970,-- RGB888: 0x191970
NAVY                    =  0x000080,-- RGB888: 0x000080
DARK_BLUE               =  0x00008B,-- RGB888: 0x00008B
MEDIUM_BLUE             =  0x0000CD,-- RGB888: 0x0000CD
BLUE                    =  0x0000FF,-- RGB888: 0x0000FF
ROYAL_BLUE              =  0x4169E1,-- RGB888: 0x4169E1
BLUE_VIOLET             =  0x8A2BE2,-- RGB888: 0x8A2BE2
INDIGO                  =  0x4B0082,-- RGB888: 0x4B0082
DARK_SLATE_BLUE         =  0x483D8B,-- RGB888: 0x483D8B
SLATE_BLUE              =  0x6A5ACD,-- RGB888: 0x6A5ACD
MEDIUM_SLATE_BLUE       =  0x7B68EE,-- RGB888: 0x7B68EE
MEDIUM_PURPLE           =  0x9370DB,-- RGB888: 0x9370DB
DARK_MAGENTA            =  0x8B008B,-- RGB888: 0x8B008B
DARK_VIOLET             =  0x9400D3,-- RGB888: 0x9400D3
DARK_ORCHID             =  0x9932CC,-- RGB888: 0x9932CC
MEDIUM_ORCHID           =  0xBA55D3,-- RGB888: 0xBA55D3
PURPLE                  =  0x800080,-- RGB888: 0x800080
THISTLE                 =  0xD8BFD8,-- RGB888: 0xD8BFD8
PLUM                    =  0xDDA0DD,-- RGB888: 0xDDA0DD
VIOLET                  =  0xEE82EE,-- RGB888: 0xEE82EE
MAGENTA                 =  0xFF00FF,-- RGB888: 0xFF00FF
ORCHID                  =  0xDA70D6,-- RGB888: 0xDA70D6
MEDIUM_VIOLET_RED       =  0xC71585,-- RGB888: 0xC71585
PALE_VIOLET_RED         =  0xDB7093,-- RGB888: 0xDB7093
DEEP_PINK               =  0xFF1493,-- RGB888: 0xFF1493
HOT_PINK                =  0xFF69B4,-- RGB888: 0xFF69B4
LIGHT_PINK              =  0xFFB6C1,-- RGB888: 0xFFB6C1
PINK                    =  0xFFC0CB,-- RGB888: 0xFFC0CB
ANTIQUE_WHITE           =  0xFAEBD7,-- RGB888: 0xFAEBD7
BEIGE                   =  0xF5F5DC,-- RGB888: 0xF5F5DC
BISQUE                  =  0xFFE4C4,-- RGB888: 0xFFE4C4
BLANCHED_ALMOND         =  0xFFEBCD,-- RGB888: 0xFFEBCD
WHEAT                   =  0xF5DEB3,-- RGB888: 0xF5DEB3
CORN_SILK               =  0xFFF8DC,-- RGB888: 0xFFF8DC
LEMON_CHIFFON           =  0xFFFACD,-- RGB888: 0xFFFACD
LIGHT_GOLDEN_ROD_YELLOW =  0xFAFAD2,-- RGB888: 0xFAFAD2
LIGHT_YELLOW            =  0xFFFFE0,-- RGB888: 0xFFFFE0 
SADDLE_BROWN            =  0x8B4513,-- RGB888: 0x8B4513
SIENNA                  =  0xA0522D,-- RGB888: 0xA0522D
CHOCOLATE               =  0xD2691E,-- RGB888: 0xD2691E 
PERU                    =  0xCD853F,-- RGB888: 0xCD853F 
SANDY_BROWN             =  0xF4A460,-- RGB888: 0xF4A460 
BURLY_WOOD              =  0xDEB887,-- RGB888: 0xDEB887 
TAN                     =  0xD2B48C,-- RGB888: 0xD2B48C 
ROSY_BROWN              =  0xBC8F8F,-- RGB888: 0xBC8F8F 
MOCCASIN                =  0xFFE4B5,-- RGB888: 0xFFE4B5 
NAVAJO_WHITE            =  0xFFDEAD,-- RGB888: 0xFFDEAD 
PEACH_PUFF              =  0xFFDAB9,-- RGB888: 0xFFDAB9
MISTY_ROSE              =  0xFFE4E1,-- RGB888: 0xFFE4E1
LAVENDER_BLUSH          =  0xFFF0F5,-- RGB888: 0xFFF0F5 
LINEN                   =  0xFAF0E6,-- RGB888: 0xFAF0E6 
OLD_LACE                =  0xFDF5E6,-- RGB888: 0xFDF5E6
PAPAYA_WHIP             =  0xFFEFD5,-- RGB888: 0xFFEFD5
SEA_SHELL               =  0xFFF5EE,-- RGB888: 0xFFF5EE
MINT_CREAM              =  0xF5FFFA,-- RGB888: 0xF5FFFA
SLATE_GRAY              =  0x708090,-- RGB888: 0x708090
LIGHT_SLATE_GRAY        =  0x778899,-- RGB888: 0x778899
LIGHT_STEEL_BLUE        =  0xB0C4DE,-- RGB888: 0xB0C4DE
LAVENDER                =  0xE6E6FA,-- RGB888: 0xE6E6FA
FLORAL_WHITE            =  0xFFFAF0,-- RGB888: 0xFFFAF0
ALICE_BLUE              =  0xF0F8FF,-- RGB888: 0xF0F8FF
GHOST_WHITE             =  0xF8F8FF,-- RGB888: 0xF8F8FF
HONEYDEW                =  0xF0FFF0,-- RGB888: 0xF0FFF0
IVORY                   =  0xFFFFF0,-- RGB888: 0xFFFFF0
AZURE                   =  0xF0FFFF,-- RGB888: 0xF0FFFF 
SNOW                    =  0xFFFAFA,-- RGB888: 0xFFFAFA
BLACK                   =  0x000000,-- RGB888: 0x000000
DIM_GRAY                =  0x696969,-- RGB888: 0x696969
GRAY                    =  0x808080,-- RGB888: 0x808080
DARK_GRAY               =  0xA9A9A9,-- RGB888: 0xA9A9A9 
SILVER                  =  0xC0C0C0,-- RGB888: 0xC0C0C0
LIGHT_GRAY              =  0xD3D3D3,-- RGB888: 0xD3D3D3
GAINSBORO               =  0xDCDCDC,-- RGB888: 0xDCDCDC
WHITE_SMOKE             =  0xF5F5F5,-- RGB888: 0xF5F5F5 
WHITE                   =  0xFFFFFF,-- RGB888: 0xFFFFFF
}


local gui = {}

-- Table of drivers
local DRIVERS = {
   "FILL_SCREEN",
   "DRAW_LINE",
   "DRAW_FRAME",
   "FILL_FRAME",
   "DRAW_TRIANGLE",
   "DRAW_CIRCLE",
   "FILL_CIRCLE"
}

--- Get value of specified bit
-- @param number Number
-- @param bit Bit position (starting from 0 as LSB)
local function getbit (number, bit)
   local highbits = math.floor(number/2^(bit+1))
   local lowbits = number - highbits*2^(bit+1)
   return math.floor(lowbits/2^bit)
end

--- Logarithm in base 2
-- @param number Number
-- @treturn number Logarithm in base 2 of Number
local function log2 (number)
   return math.log(number)/math.log(2)
end

--- Split bitfields packed in a number
-- @param number number containing the bit fields
-- @tparam {int,...} fieldbits a table of integers, each one indicating the bit-width
-- of each field from MSB to LSB.
-- @treturn {int,...}  a table of integers, each one with the value of the
-- corresponding field
-- @usage
-- result = splitIntegerBits(0xAABBCC, {8,8,8})
-- -- result = {0xAA, 0xBB, 0xCC}
local function splitIntegerBits(number, fieldbits)
   local fb = {0}
   local total_bits = 0
   for i, v in ipairs(fieldbits) do
      total_bits = total_bits + v
      table.insert(fb, v)
   end
   local components = {}
   local temp_value = number
   local remaining_bits = total_bits
   for i, bits_in_field in ipairs(fb) do
      remaining_bits = remaining_bits - bits_in_field
      local temp_component = math.floor(temp_value/2^remaining_bits)
      temp_value = temp_value - temp_component*(2^remaining_bits)
      table.insert(components, temp_component)
   end
   table.remove(components,1)
   return components
end

--- Transform a 32 bit RGB (888) color to 16 bit RGB (565) color.
-- @param rgb32 32 bit RGB (888) color (0xRRGGBB)
-- @treturn int 16 bit RGB (565) color (bits: rrrr rggg gggb bbbb)
local function rgb32to16(rgb32)
   local rgb = splitIntegerBits(rgb32, {8,8,8})
   local red8, green8, blue8 = rgb[1], rgb[2], rgb[3]
   local red = red8/(2^8-1)
   local green = green8/(2^8-1)
   local blue = blue8/(2^8-1)
   local red5 = math.floor( (2^5-1)*red )
   local green6 = math.floor( (2^6-1)*green )
   local blue5 = math.floor( (2^5-1)*blue )
   return red5 * 2^(5+6) + green6 * 2^5 + blue5
end

--- Transform a 16 bit RGB (565) color to 32 bit RGB (888) color.
-- @param rgb16 16 bit RGB (565) color (bits: rrrr rggg gggb bbbb)
-- @treturn int 32 bit RGB (888) color (0xRRGGBB)
local function rgb16to32(rgb16)
   local rgb = splitIntegerBits(rgb16, {5,6,5})
   local red5, green6, blue5 = rgb[1], rgb[2], rgb[3]
   local red = red5/(2^5-1)
   local green = green6/(2^6-1)
   local blue = blue5/(2^5-1)
   local red8 = math.floor( (2^8-1)*red )
   local green8 = math.floor( (2^8-1)*green )
   local blue8 = math.floor( (2^8-1)*blue )
   return red8 * 2^16 + green8 * 2^8 + blue8
end

--- µGUI Core structure.
-- @field pset User pset-function
-- @field dim Dimensions (resolution) of the display {x, y}
-- @field touch Touch object holding latest touch data to be processed
-- @field window Window handler structure {active, last, next}
-- @field console Console structure
-- @field font Font structure
-- @field color Color structure {fore, back, desktop}
-- @field state GUI state
-- @field driver List of hardware drivers
-- @table UG_GUIcore

--- BITMAP structure
-- @field p BMP data
-- @field width BMP width
-- @field height BMP height
-- @field bpp Bits per pixel
-- <ul>
-- <li>BMP_BPP_1</li>
-- <li>BMP_BPP_2</li>
-- <li>BMP_BPP_4</li>
-- <li>BMP_BPP_8</li>
-- <li>BMP_BPP_16</li>
-- <li>BMP_BPP_32</li>
-- </ul>
-- @field colors
-- <ul>
-- <li>BMP_RGB888</li>
-- <li>BMP_RGB565</li>
-- <li>BMP_RGB555</li>
-- </ul>
-- @table UG_BMP

--- Classic functions.
-- @section classicfnc


--- GUI management function.
-- This function initializes a new GUI object, selects another GUI
-- or returns the current active GUI object or its dimensions.
-- @tparam str command  command string: init, select, get or getDim
-- <dl>Possible commands are:
-- <dt><span class="parameter">init</span></dt>
-- <dd>Initialize GUI object</dd>
-- <dt><span class="parameter">select</span></dt>
-- <dd>Make the supplied GUI object the active one</dd>
-- <dt><span class="parameter">get</span></dt>
-- <dd>Get the active GUI</dd>
-- <dt><span class="parameter">getDim</span></dt>
-- <dd>Get a table with GUI's {x,y} dimensions</dd>
-- </dl>
-- @param parameters Parameter for the commands.
-- <ul>
-- <li><dl>The parameters for the "init" command are:
-- <dt><span class="parameter">g</span></dt>
-- <dd> GUI object (table)</dd>
-- <dt><span class="parameter">p</span></dt>
-- <dd> User pset-function</dd>
-- <dt><span class="parameter">x</span></dt>
-- <dd> X-Dimension (= X-Resolution) of the display</dd>
-- <dt><span class="parameter">y</span></dt>
-- <dd> Y-Dimension (= Y-Resolution) of the display</dd>
-- </dl></li>
-- <li><dl>The parameters for the "select" command is:
-- <dt><span class="parameter">g</span></dt>
-- <dd> A previously initilialized GUI object (table)</dd>
-- </dl></li>
-- <li>"get" command does not require additional parameters.</li>
-- <li>"getDim" command does not require additional parameters.</li>
-- </ul>
-- @treturn bool Result of function (commands "init" and "select")
-- @treturn tbl The current active GUI object when command is "get"
-- @treturn tbl The GUI's x and y dimensions if command is "getDim"
-- @usage
-- gui_oled = {} -- Global GUI object (OLED)
-- gui_tft = {} -- Global GUI object (TFT)
--
-- UG_GUI("init", gui_oled, {p=OLEDPixelSetFunction, x=128, y=64})
-- UG_GUI("init", gui_tft, {p=OLEDPixelSetFunction, x=480, y=272})
-- UG_GUI("select", gui_oled)
-- -- ...
-- UG_GUI("select", gui_tft)
-- -- ...
-- active_gui = UG_GUI("get")
-- -- ...
-- GUIdimensions = UG_GUI("getDim")
-- print(GUIdimensions.x, GUIdimensions.y)
-- 
local function UG_GUI(command, parameters)
   -- Process all possible paramaters, those which are not defined
   -- should be ignored (and calling them will return nil)
   local g, p, x, y 
   
   if command == "init" then
      -- unpack payload
      g, p, x, y = parameters.g, parameters.p, parameters.x, parameters.y
      if g and p and x and y then
	 -- all parameters set, let's go
	 g.pset = p
	 g.dim = {x=x, y=y}
	 g.touch = {xp=-1, yp=-1, state="RELEASED"}
	 g.console = {}
	 g.console.start_c = {x=4, y=4}
	 g.console.end_c = {x = g.dim.x - g.console.start_c.x-1,
			    y = g.dim.y - g.console.start_c.y-1}
	 g.console.pos_c = {x = g.console.end_c.x,
			    y = g.console.end_c.y}
	 g.console.color = {fore=C.WHITE, back=C.BLACK}
	 g.font = {char_h_space = 1,
		   char_v_space = 1,
		   char_width = nil,
		   char_height = nil,
		   p = nil}
	 g.color = {desktop = 0x5E8BEf,
		    fore = C.WHITE,
		    back = C.BLACK}
	 g.state = {}
	 g.window = {}
	 -- Clear drivers
	 g.driver = {}
	 for i = 1, #DRIVERS, 1 do
	    g.driver[DRIVERS[i]]={}
	    g.driver[DRIVERS[i]].driver = nil
	    g.driver[DRIVERS[i]].state = {REGISTERED=false,
					  ENABLED=false}
	 end
      else
	 -- missing parameters
	 return false, "[UG_GUI|"..command.."] Error: Some parameters are missing."
      end
      
   elseif command == "select" then
      -- unpack payload
      g = parameters.g

   elseif command == "get" then
      -- give back the current gui
      return gui

   elseif command == "getDim" then
      -- return table with the GUI's x and y dimensions
      return gui.dim
      
   else
      -- wrong command
      return false, "[UG_GUI] Error: "..command.."is not a valid command."
   end

   if g then
      gui = g
      return true
   else
      return false, "[UG_GUI] Error GUI parameter missing."
   end

end

--- Draw objects on the GUI.
-- @tparam str shape Object (shape) to draw
-- <dl>Valid shapes and its properties:
-- <dt><span class="parameter">Pixel</span></dt>
-- <dd>Draws a pixel with a selected color..</br>
-- (Properties: x0, y0, c)</dd>
-- <dt><span class="parameter">Line</span></dt>
-- <dd>Draws a line between two points.</br>
-- (Properties: x1, y1, x2, y2, c).</dd>
-- <dt><span class="parameter">Frame</span></dt>
-- <dd>Draws a frame with a selected color.</br>
-- (Properties: x1, y1, x2, y2, c).</dd>
-- <dt><span class="parameter">RoundFrame</span></dt>
-- <dd>Draws a rounded-corners frame with a selected color.</br>
-- (Properties: x1, y1, x2, y2, r, c).</dd>
-- <dt><span class="parameter">Triangle</span></dt>
-- <dd>Draws a triangle with a selected color.</br>
-- (Properties: x0, y0, x1, y1, x2, y2, c).</dd>
-- <dt><span class="parameter">Mesh</span></dt>
-- <dd>Draw s a rectangular mesh with a selected color.</br>
-- (Properties: x1, y1, x2, y2, c[, mesh_step=2])</dd>
-- <dt><span class="parameter">Arc</span></dt>
-- <dd>Draws an arc with a selected color.</br>
-- (Properties: x0, y0, r, s, c)</dd>
-- <dt><span class="parameter">Circle</span></dt>
-- <dd>Draws a circle with a selected color and radius.</br>
-- (Properties: x0, y0, r, c)</dd>
-- <dt><span class="parameter">BMP</span></dt>
-- <dd>Draws a bitmap.</br>
-- (Properties: x0, y0, bmp)</dd>
-- <dl>
-- @tparam tbl properties Object's properties (coordinates, color, ...)
-- <dl>Properties description:
-- <dt><span class="parameter">x0, y0, x1, y1, x2, y2</span></dt>
-- <dd>x, y coordinates for reference points:
-- line endpoints, frame corners, triangle vertices, arc and circle center,
-- image top/left corner.</dd>
-- <dt><span class="parameter">c</span></dt>
-- <dd>Color.</dd>
-- <dt><span class="parameter">r</span></dt>
-- <dd>Radius for roundframe, arc and circle objects.</dd>
-- <dt><span class="parameter">s</span></dt>
-- <dd>Arc's sector (8 bit encoded).</dd>
-- <dt><span class="parameter">mesh_step</span></dt>
-- <dd>Grid point step for mesh object.</dd>
-- <dt><span class="parameter">bmp</span></dt>
-- <dd>Bitmap color data.</dd>
-- </dl>
-- @treturn[1] bool True
-- @treturn[2] bool False
-- @treturn[2] str Shape is not valid
-- @treturn[3] bool false
-- @treturn[3] str Parameters missing.
-- @treturn[4] bool false
-- @treturn[4] str Parameters out of range or invalid.
local function UG_Draw(shape, properties)
   local driver_id = "DRAW_"..string.upper(shape)
   local dbglbl = "[DEBUG|UG_Draw]"

   if gui.debug and gui.debug.classic then
      print(dbglbl, "Function BEGINS for shape:", shape)
      print(dbglbl, "- properties parameter:")
      for k,v in pairs(properties) do
	 print(dbglbl, "  -", k, ":", v)
      end
   end
   
   -- Process all possible properties, those which are not defined
   -- should be ignored (and calling them will return nil)
   local x0, y0 = properties.x0, properties.y0
   local x1, y1 = properties.x1, properties.y1
   local x2, y2 = properties.x2, properties.y2
--   local x3, y3 = properties.x3, properties.y3
   local r, s = properties.r, properties.s
   local c = properties.c
   local mesh_step = properties.mesh_step
   local bmp = properties.bmp
   
   if gui.driver[driver_id] and gui.driver[driver_id].state.ENABLED then
      if gui.debug and gui.debug.classic then
	 print(dbglbl, "Driver "..driver_id.." enabled")
      end
      if gui.driver[driver_id].driver(properties) then return true end

   end
   
   if shape == "Line" then
      -- Draw a line
      
      -- Check if the required parameters are available
      if x1 and y1 and x2 and y2 and c then
	 local dx, dy, sgndx, sgndy, dxabs, dyabs, x, y, drawx, drawy

	 dx = x2 - x1
	 dy = y2 - y1
	 if (dx>0) then dxabs = dx else dxabs = -dx end
	 if (dy>0) then dyabs = dy else dyabs = -dy end
	 if (dx>0) then sgndx = 1 else sgndx = -1 end
	 if (dy>0) then sgndy = 1 else sgndy = -1 end
	 x = math.floor(dyabs / 2^1)
	 y = math.floor(dxabs / 2^1)
	 drawx = x1
	 drawy = y1

	 gui.pset(drawx, drawy,c)

	 if( dxabs >= dyabs ) then
	    for n=0, dxabs-1, 1 do
	       y = y + dyabs
	       if( y >= dxabs ) then
		  y = y - dxabs;
		  drawy = drawy + sgndy;
	       end
	       drawx = drawx + sgndx;
	       gui.pset(drawx, drawy,c)
	    end
	 else
	    for n=0, dyabs-1, 1 do
	       x = x + dxabs
	       if( x >= dyabs ) then
		  x = x - dyabs
		  drawx = drawx + sgndx
	       end
	       drawy = drawy + sgndy
	       gui.pset(drawx, drawy,c)
	    end
	 end
      else
	 -- Some parameters are missing
	 return false, "[UG_Draw|"..shape.."] Error: Some parameters are missing."
      end

      return true
      
   elseif shape == "Arc" then
      -- Draw an Arc

      -- Check if the required parameters are available
      if x0 and y0 and r and s and c then
	 local x,y,xd,yd,e

	 if ( x0<0 ) then
	    return false, "[UG_Draw|"..shape.."] Error: x0 out of range (x0 < 0)."
	 end
	 if ( y0<0 ) then
	    return false, "[UG_Draw|"..shape.."] Error: y0 out of range (x0 < 0)."
	 end
	 if ( r<=0 ) then
	    return false, "[UG_Draw|"..shape.."] Error: r out of range (r <= 0)."
	 end

	 xd = 1 - (r * 2^1)
	 yd = 0
	 e = 0
	 x = r
	 y=0
	 
	 while ( x >= y ) do
	    -- Q1
	    if getbit(s,0) > 0 then gui.pset(x0 + x, y0 - y, c) end
	    if getbit(s,1) > 0 then gui.pset(x0 + y, y0 - x, c) end
	    -- Q2
	    if getbit(s,2) > 0 then gui.pset(x0 - y, y0 - x, c) end
	    if getbit(s,3) > 0 then gui.pset(x0 - x, y0 - y, c) end
	    -- Q3
	    if getbit(s,4) > 0 then gui.pset(x0 - x, y0 + y, c) end
	    if getbit(s,5) > 0 then gui.pset(x0 - y, y0 + x, c) end
	    -- Q4
	    if getbit(s,6) > 0 then gui.pset(x0 + y, y0 + x, c) end
	    if getbit(s,7) > 0 then gui.pset(x0 + x, y0 + y, c) end

	    y = y + 1
	    e = e + yd
	    yd = yd + 2
	    if ( ((e * 2^1) + xd) > 0 ) then
	       x = x - 1
	       e = e + xd
	       xd = xd + 2
	    end
	 end
      else
	 -- Some parameters are missing
	 return false, "[UG_Draw|"..shape.."] Error: Some parameters are missing."
      end
      
      return true
      
   elseif shape == "Mesh" then
      -- Draw a mesh
      
      -- Check if the required parameters are available
      if x1 and y1 and x2 and y2 and c then
	 local mesh_step = mesh_step or 2
	 local n,m
	 if ( x2 < x1 ) then
	    n = x2
	    x2 = x1
	    x1 = n
	 end
	 if ( y2 < y1 ) then
	    n = y2
	    y2 = y1
	    y1 = n
	 end

	 for m = y1, y2, mesh_step do
	    for n = x1, x2, mesh_step do
	       gui.pset(n,m,c)
	    end
	 end
      else
	 -- Some parameters are missing
	 return false, "[UG_Draw|"..shape.."] Error: Some parameters are missing."
      end

      return true
      
   elseif shape == "Frame" then
      -- Draw a frame

      -- Check if the required parameters are available
      if x1 and y1 and x2 and y2 and c then
	 UG_Draw("Line", {x1=x1, y1=y1, x2=x2, y2=y1, c=c})
	 UG_Draw("Line", {x1=x1, y1=y2, x2=x2, y2=y2, c=c})
	 UG_Draw("Line", {x1=x1, y1=y1, x2=x1, y2=y2, c=c})
	 UG_Draw("Line", {x1=x2, y1=y1, x2=x2, y2=y2, c=c})
      else
	 -- Some parameters are missing
	 return false, "[UG_Draw|"..shape.."] Error: Some parameters are missing."
      end
      
      return true	 
      
   elseif shape == "RoundFrame" then
      -- Draw a frame with rounded corners

      -- Check if the required parameters are available
      if x1 and y1 and x2 and y2 and c and r then
	 local n
	 
	 if x2 < x1 then
	    n = x2
	    x2 = x1
	    x1 = n
	 end
	 if y2 < y1 then
	    n = y2
	    y2 = y1
	    y1 = n
	 end

	 -- @todo check if it should be (r > 0.5*(x2-x1) ) and (r > 0.5*(y2-y1) )
	 if ( r > x2 ) then
	    return false, "[UG_Draw|"..shape.."] Error: r out of range (r > x2)."
	 end
	 if ( r > y2 ) then
	    return false, "[UG_Draw|"..shape.."] Error: r out of range (r > y2)."
	 end

	 UG_Draw("Line", {x1=x1+r, y1=y1, x2=x2-r, y2=y1, c=c})
	 UG_Draw("Line", {x1=x1+r, y1=y2, x2=x2-r, y2=y2, c=c})
	 UG_Draw("Line", {x1=x1, y1=y1+r, x2=x1, y2=y2-r, c=c})
	 UG_Draw("Line", {x1=x2, y1=y1+r, x2=x2, y2=y2-r, c=c})
	 UG_Draw("Arc", {x0=x1+r, y0=y1+r, r=r, s=0x0C, c=c})
	 UG_Draw("Arc", {x0=x2-r, y0=y1+r, r=r, s=0x03, c=c})
	 UG_Draw("Arc", {x0=x1+r, y0=y2-r, r=r, s=0x30, c=c})
	 UG_Draw("Arc", {x0=x2-r, y0=y2-r, r=r, s=0xC0, c=c})
      else
	 -- Some parameters are missing
	 return false, "[UG_Draw|"..shape.."] Error: Some parameters are missing."
      end
      
      return true	 
      
   elseif shape == "Pixel" then
      -- Draw a pixel

      -- Check if the required parameters are available
      if x0 and y0 and c then
	 gui.pset(x0,y0,c)
      else
	 -- Some parameters are missing
	 return false, "[UG_Draw|"..shape.."] Error: Some parameters are missing."
      end
      
      return true	 
      
   elseif shape == "Circle" then
      -- Draw a circle

      -- Check if the required parameters are available
      if x0 and y0 and c and r then
	 local x,y,xd,yd,e

	 if ( x0<0 ) then
	    return false, "[UG_Draw|"..shape.."] Error: x0 out of range (x0 < 0)."
	 end
	 if ( y0<0 ) then
	    return false, "[UG_Draw|"..shape.."] Error: y0 out of range (y0 < 0)."
	 end
	 if ( r<=0 ) then
	    return false, "[UG_Draw|"..shape.."] Error: r out of range (r <= 0)."
	 end
	 
	 xd = 1 - (r * 2^1)
	 yd = 0
	 e = 0
	 x = r
	 y = 0

	 while ( x >= y ) do
	    gui.pset(x0 - x, y0 + y, c)
	    gui.pset(x0 - x, y0 - y, c)
	    gui.pset(x0 + x, y0 + y, c)
	    gui.pset(x0 + x, y0 - y, c)
	    gui.pset(x0 - y, y0 + x, c)
	    gui.pset(x0 - y, y0 - x, c)
	    gui.pset(x0 + y, y0 + x, c)
	    gui.pset(x0 + y, y0 - x, c)
	    
	    y = y + 1
	    e = e + yd
	    yd = yd + 2
	    if ( ((e * 2^1) + xd) > 0 ) then
	       x = x - 1
	       e = e + xd
	       xd = xd +2
	    end
	 end
      else
	 -- Some parameters are missing
	 return false, "[UG_Draw|"..shape.."] Error: Some parameters are missing."
      end
      
      return true
	 
   elseif shape == "Triangle" then
      -- Draw a triangle
      
      -- Check if the required parameters are available
      if x1 and y1 and x2 and y2 and x0 and y0 and c then
	 UG_Draw("Line", {x1=x1, y1=y1, x2=x2, y2=y2, c=c})
	 UG_Draw("Line", {x1=x2, y1=y2, x2=x0, y2=y0, c=c})
	 UG_Draw("Line", {x1=x1, y1=y1, x2=x0, y2=y0, c=c})
      else
	 -- Some parameters are missing
	 return false, "[UG_Draw|"..shape.."] Error: Some parameters are missing."
      end
      
      return true
   elseif shape == "BMP" then
      -- Draw a bitmap

      -- Check if the required parameters are available
      if x0 and y0 and bmp then
	 local x, y, xs, rgb, p, i
	 if not bmp.p then
	    return false, "[UG_Draw|"..shape.."] Error: BMP has no data."
	 end

	 -- Only 16 BPP supported so far
	 if bmp.bpp == "BMP_BPP_16" then
	    p = bmp.p
	 else
	    return false, "[UG_Draw|"..shape.."] Error: "..bmp.bpp.."bits per pixel is not supported."
	 end

	 i = 1
	 for y = 0, bmp.height-1, 1 do
	    for x = 0, bmp.width-1, 1 do
	       rgb16 = splitIntegerBits(p[i],{5,6,5})
	       rgb = rgb16to32(p[i])
	       rgb32 = splitIntegerBits(rgb,{8,8,8})
	       if gui.debug and gui.debug.classic then
		  print(dbglbl, "BMP: i="..i.." x,y="..x..","..y,
			"rgb16=("..rgb16[1]..", "..rgb16[2]..", "..rgb16[3]..")",
			", rgb32=("..rgb32[1]..", "..rgb32[2]..", "..rgb32[3]..")")
	       end
	       -- UG_Draw("Pixel", {x0=x0 + x, y0=y0 + y, c=rgb})
	       gui.pset(x0+x,y0+y,rgb)
	       i=i+1
	    end
	 end
	 
      else
	 -- Some parameters are missing
	 return false, "[UG_Draw|"..shape.."] Error: Some parameters are missing."
      end
      
   else
      -- wrong shape
      return false, "[UG_Draw] Error: "..shape.." is not a valid shape."
   end

   -- Should not get past here, but just in case.
   return false, "[UG_Draw] Error."
   
end

--- "Paint" filled objects on the GUI.
-- @tparam str shape Object (shape) to draw
-- <dl>Valid shapes and its properties:
-- <dt><span class="parameter">Screen</span></dt><dd>Fills the whole screen with the selected color.
-- </br>(Properties: c)</dd>
-- <dt><span class="parameter">Frame</span></dt><dd>Fills a rectangular area with a selected color.
-- </br>(Properties: x1, y1, x2, y2, c)</dd>
-- <dt><span class="parameter">RoundFrame</span></dt><dd>Fills a rounded-corners rectangular area with a selected color.
-- </br>(Properties: x1, y1, x2, y2, r, c)</dd>
-- <dt><span class="parameter">Circle</span></dt><dd>Fills a circle with a selected color.
-- </br>(Properties: x0, y0, r, c)</dd>
-- </dl>
-- @tparam tbl properties Object's properties (coordinates, color, ...)
-- <dl>Properties description:
-- <dt><span class="parameter">x0, y0, x1, y1, x2, y2</span></dt>
-- <dd>x, y coordinates for reference points:
-- frame corners, triangle vertices and circle center.</dd>
-- <dt><span class="parameter">c</span></dt><dd>Color.</dd>
-- <dt><span class="parameter">r</span></dt><dd>Radius for roundframe and circle objects.</dd>
-- </dl>
-- @treturn[1] bool True
-- @treturn[2] bool False
-- @treturn[2] str Shape is not valid
-- @treturn[3] bool false
-- @treturn[3] str Parameters missing.
-- @treturn[4] bool false
-- @treturn[4] str Parameters out of range or invalid.
-- @treturn[5] bool false
-- @treturn[5] str Error in subroutine.
local function UG_Fill(shape, properties)
   local driver_id = "FILL_"..string.upper(shape)

   local dbglbl = "[DEBUG|UG_Fill]"

   if gui.debug and gui.debug.classic then
      print(dbglbl, "Function BEGINS for shape:", shape)
      print(dbglbl, "- properties parameter:")
      for k,v in pairs(properties) do
	 print(dbglbl, "  -", k, ":", v)
      end
   end
   
   -- Process all possible properties, those which are not defined
   -- should be ignored (and calling them will return nil)
   local x0, y0 = properties.x0, properties.y0
   local x1, y1 = properties.x1, properties.y1
   local x2, y2 = properties.x2, properties.y2
--   local x3, y3 = properties.x3, properties.y3
   local r, s = properties.r, properties.s
   local c = properties.c
   
   if gui.driver[driver_id] and gui.driver[driver_id].state.ENABLED then
      if gui.debug and gui.debug.classic then
	 print(dbglbl, "Driver "..driver_id.." enabled")
      end
      if gui.driver[driver_id].driver(properties) then return true end

   end

   if gui.debug and gui.debug.classic then
      print (dbglbl, "Driver "..driver_id.." not used. Try module implementation.")
   end
   
   if shape == "Frame" then
      -- Fill a frame

      -- Check if the required parameters are available
      if x1 and y1 and x2 and y2 and c then
	 local n,m

	 -- swap coordinates to make x2,y2 greater than x1,y1 (respectively)
	 if ( x2 < x1 ) then
	    n = x2
	    x2 = x1
	    x1 = n
	 end
	 if ( y2 < y1 ) then
	    n = y2
	    y2 = y1
	    y1 = n
	 end

	 -- fill the region with selected color
	 for m=y1, y2, 1 do
	    for n=x1, x2, 1 do
	       gui.pset(n,m,c)
	    end
	 end

	 return true
      
      else
	 -- Some parameters are missing
	 return false, "[UG_Fill|"..shape.."] Error: Some parameters are missing."
      end

   elseif shape == "Screen" then
      -- not really a shape...

      -- Check if the required parameters are available
      if c then
	 return UG_Fill("Frame", {x1=0, y1=0, x2=gui.dim.x-1, y2=gui.dim.y-1, c=c})
      else
	 -- Some parameters are missing
	 return false, "[UG_Fill|"..shape.."] Error: Some parameters are missing."
      end
	 
   elseif shape == "RoundFrame" then
      -- Fill a rounded corners frame

      -- Check if the required parameters are available
      if x1 and y1 and x2 and y2 and c and r then
	 local x,y,xd

	 -- swap coordinates to make x2,y2 greater than x1,y1 (respectively)
	 if x2 < x1 then
	    x = x2
	    x2 = x1
	    x1 = x
	 end
	 if y2 < y1 then
	    y = y2
	    y2 = y1
	    y1 = y
	 end

	 if ( r<=0 ) then
	    return false, "[UG_Fill|"..shape.."] Error: r out of range (r <= 0)."
	 end

	 xd = 3 - r*2^1
	 x = 0
	 y = r

	 local FF_OK = UG_Fill("Frame", {x1=x1+r, y1=y1, x2=x2-r, y2=y2, c=c})
	 local DL_OK = true

	 while x <= y do
	    if( y > 0 ) then
	       DL_OK = (UG_Draw("Line", {x1=x2+x-r, y1=y1-y+r, x2=x2+x-r, y2=y+y2-r, c=c})
			   and DL_OK)
	       DL_OK = (UG_Draw("Line",	{x1=x1-x+r, y1=y1-y+r, x2=x1-x+r, y2=y+y2-r, c=c})
			   and DL_OK)
	    end
	    if( x > 0 ) then
	       DL_OK = (UG_Draw("Line",	{x1=x1-y+r, y1=y1-x+r, x2=x1-y+r, y2=x+y2-r, c=c})
			   and DL_OK)
	       DL_OK = (UG_Draw("Line",	{x1=x2+y-r, y1=y1-x+r, x2=x2+y-r, y2=x+y2-r, c=c})
			   and DL_OK)
	    end
	    if ( xd < 0 ) then xd = xd + (x * 2^2) + 6 else
	       xd = xd + ( (x-y) * 2^2 ) + 10
	       y = y - 1;
	    end
	    x = x + 1	 
	 end

	 if not (DL_OK and FF_OK) then
	    return false, "[UG_Fill|"..shape.."] Error: internal function."
	 end

	 return true
	 
      else
	 -- Some parameters are missing
	 return false, "[UG_Fill|"..shape.."] Error: Some parameters are missing."
      end	 

   elseif shape == "Circle" then
      -- Fill a circle

      -- Check if the required parameters are available
      if x0 and y0 and c and r then
	 local x,y,xd
	 
	 xd = 3 - r*2^1
	 x = 0
	 y = r

	 local DL_OK = true
	 while ( x <= y ) do
	    if( y > 0 ) then
	       DL_OK = (UG_Draw("Line", {x1=x0-x, y1=y0-y, x2=x0-x, y2=y0+y, c=c})
			   and DL_OK)
	       DL_OK = (UG_Draw("Line", {x1=x0+x, y1=y0-y, x2=x0+x, y2=y0+y, c=c})
			   and DL_OK)
	    end
	    if( x > 0 ) then
	       DL_OK = (UG_Draw("Line", {x1=x0-y, y1=y0-x, x2=x0-y, y2=y0+x, c=c})
			   and DL_OK)
	       DL_OK = (UG_Draw("Line", {x1=x0+y, y1=y0-x, x2=x0+y, y2=y0+x, c=c})
			   and DL_OK)
	    end
	    if ( xd < 0 ) then xd = xd + (x * 2^2) + 6 else
	       xd = xd + ((x - y) * 2^2) + 10
	       y = y - 1
	    end
	    x = x + 1
	 end
	 local DC_OK = UG_Draw("Circle",{x0=x0, y0=y0, r=r, c=c})

	 if not (DL_OK and DC_OK) then
	    return false, "[UG_Fill|"..shape.."] Error: internal function."
	 end

	 return true
	 
      else
	 -- Some parameters are missing
	 return false, "[UG_Fill|"..shape.."] Error: Some parameters are missing."
      end	 
	 
      
   else
      -- wrong shape
      return false, "[UG_Fill] Error: "..shape.." is not a valid shape."
   end
end

--- Render text and manage the text rendering settings.
-- @tparam str command Method to call.
-- <dl>Valid commands and its parameters:
-- <dt><span class="parameter">selectFont</span></dt><dd>Select font.
-- </br>(Parameters: font)</dd>
-- <dt><span class="parameter">setForecolor</span></dt>
-- <dd>Defines the fore color of the string.
-- </br>(Parameters: c)</dd>
-- <dt><span class="parameter">setBackcolor</span></dt>
-- <dd>Defines the back color of the string.
-- </br>(Parameters: c)</dd>
-- <dt><span class="parameter">setFontHSpace</span></dt>
-- <dd>Defines the horizontal space between each char.
-- </br>(Parameters: s)</dd>
-- <dt><span class="parameter">setFontVSpace</span></dt>
-- <dd>Defines the vertical space between each char.
-- </br>(Parameters: s)</dd>
-- <dt><span class="parameter">putChar</span></dt><dd>Draws a single char.
-- </br>(Parameters: chr, x, y, fc[, bc])</dd>
-- <dt><span class="parameter">putString</span></dt><dd>Draws a string.
-- </br>(Parameters: str, x, y)</dd>
-- </dl>
-- @tparam tbl parameters Parameters for the method.
-- <dl>Parameters description:
-- <dt><span class="parameter">font</span></dt><dd>Font object.</dd>
-- <dt><span class="parameter">c</span></dt>
-- <dd>Color for setForecolor and setBackcolor methods.</dd>
-- <dt><span class="parameter">s</span></dt>
-- <dd>Space between chars (horizontal or vertical) for setFontHSpace and
-- setFontVSpace methods.</dd>
-- <dt><span class="parameter">x, y</span></dt>
-- <dd>Coordinates for the putChar and putString methods.</dd>
-- <dt><span class="parameter">chr</span></dt>
-- <dd>Char for the putChar method.</dd>
-- <dt><span class="parameter">fc, bc</span></dt>
-- <dd>Fore and back color (respectively) for the putChar
-- method.</dd>
-- <dt><span class="parameter">str</span></dt>
-- <dd>String for the putString method.</dd>
-- </dl>
-- @treturn[1] bool True
-- @treturn[2] bool False
-- @treturn[2] str command is not valid
-- @treturn[3] bool false
-- @treturn[3] str Parameters missing.
-- @treturn[5] bool false
-- @treturn[5] str Error in subroutine.
local function UG_Text(command, parameters)
   local driver_id = "TEXT_"..string.upper(command)

   local dbglbl = "[DEBUG|UG_Text]"

   if gui.debug and gui.debug.classic then
      print(dbglbl, "Function BEGINS for command:", command)
      print(dbglbl, "- parameter:")
      for k,v in pairs(parameters) do
	 print(dbglbl, "  -", k, ":", v)
      end
   end
   -- Process all possible parameters, those which are not defined
   -- should be ignored (and calling them will return nil)
   local font = parameters.font
   local c = parameters.c
   local space = parameters.s
   local chr, x, y = parameters.chr, parameters.x, parameters.y
   local fc, bc = parameters.fc, parameters.bc
   local str = parameters.str
   
   if gui.driver[driver_id] and gui.driver[driver_id].state.ENABLED then
      if gui.debug and gui.debug.classic then
	 print(dbglbl, "Driver "..driver_id.." enabled")
      end
      if gui.driver[driver_id].driver(properties) then return true end

   end
   
   if command == "selectFont" then
      -- Select the active font for text methods.
      
      -- Check if the required parameters are available
      if font then
	 gui.font.p = font.p
	 gui.font.char_width = font.char_width
	 gui.font.char_height = font.char_height

	 return true
	 
      else
	 -- Some parameters are missing
	 return false, "[UG_Text|"..command.."] Error: Some parameters are missing."
      end
      
   elseif command == "setForecolor" then
      -- Defines the fore color for the putString method

      -- Check if the required parameters are available
      if c then
	 gui.color.fore = c

	 return true
	 
      else
	 -- Some parameters are missing
	 return false, "[UG_Text|"..command.."] Error: Some parameters are missing."
      end
      
   elseif command == "setBackcolor" then
      -- Defines the back color for the putString method

      -- if c is nil or false, the background will be "transparent" (not painted)
      gui.color.back = c

      return true
      
   elseif command == "setFontHSpace" then
      -- Defines the horizontal space between each char.

      -- Check if the required parameters are available
      if space then
	 gui.font.char_h_space = space

	 return true
      else
	 -- Some parameters are missing
	 return false, "[UG_Text|"..command.."] Error: Some parameters are missing."
      end
      
   elseif command == "setFontVSpace" then
      -- Defines the vertical space between each char.

      -- Check if the required parameters are available
      if space then
	 gui.font.char_v_space = space

	 return true
      else
	 -- Some parameters are missing
	 return false, "[UG_Text|"..command.."] Error: Some parameters are missing."
      end
      
   elseif command == "putChar" then
      -- Draw a single char.

      -- Check if the required parameters are available
      if chr and x and y and fc then
	 local xo, yo, bt, p

	 bt = string.byte(chr) + 1

	 p = gui.font.p
	 local char_width = gui.font.char_width
	 local char_height = gui.font.char_height
	 local number_of_bytes = math.ceil(char_width/8)
	 local symbol = p[bt]

	 yo = y

	 for row = 1, char_height, 1 do
	    xo = x
	    -- io.write("["..row.."]: ")
	    for bit = 0, char_width-1, 1 do
	       -- select the corresponding byte in the row according to the bit number
	       local row_byte = math.floor((char_width-1)/8) - math.floor(bit/8)
	       -- get the index of the select byte in the char object
	       local index = number_of_bytes * row - row_byte 
	       -- io.write(index.."/"..row_byte..","..(bit%8).." ("..xo..","..yo..") | ")
	       -- get the value for the specific row, column of the char data
	       local value = getbit(symbol[index], bit%8 )
	       if value > 0 then gui.pset(xo,yo,fc) else
		  -- transparent background if not bc
		  if bc then gui.pset(xo,yo,bc) end 
	       end
	       xo = xo + 1
	    end
	    yo = yo + 1
	    -- io.write("\n")
	 end

	 return true
      else
	 -- Some parameters are missing
	 return false, "[UG_Text|"..command.."] Error: Some parameters are missing."
      end
	 
   elseif command == "putString" then
      -- Draw a string

      -- Check if the required parameters are available
      if x and y and str then
	 local xp, yp, chr

	 xp, yp = x, y
	 for chr_pos = 1, str:len(), 1 do
	    chr = str:sub(chr_pos, chr_pos)
	    if chr == "\n" then
	       xp = gui.dim.x
	    else
	       if ( xp + gui.font.char_width > gui.dim.x - 1 ) then
		  xp = x
		  yp = yp + gui.font.char_height + gui.font.char_v_space
	       end

	       local PC_OK = UG_Text("putChar",
				     {chr=chr, x=xp, y=yp,
				      fc=gui.color.fore, bc=gui.color.back})

	       xp = xp + gui.font.char_width + gui.font.char_h_space
	    end
	 end

	 if not PC_OK then
	    return false, "[UG_Text|"..command.."] Error: internal function."
	 end

	 return true

      else
	 -- Some parameters are missing
	 return false, "[UG_Text|"..command.."] Error: Some parameters are missing."
      end   
      
   else
      -- wrong command
      return false, "[UG_Text] Error: "..command.."is not a valid command."
   end
   
end

--- Manage a "console" area for printing message strings.
-- @tparam str command Method to call.
-- <dl>Valid commands and its parameters:
-- <dt><span class="parameter">setArea</span></dt>
-- <dd>Defines the active console area.
-- </br>(Parameters: xs, ys, xe, ye)</dd>
-- <dt><span class="parameter">setForecolor</span></dt>
-- <dd>Defines the fore color of the console.
-- </br>(Parameters: c)</dd>
-- <dt><span class="parameter">setBorecolor</span></dt>
-- <dd>Defines the back color of the console.
-- </br>(Parameters: c)</dd>
-- <dt><span class="parameter">putString</span></dt>
-- <dd>Adds a string to the console.
-- </br>(Parameters: str)</dd>
-- </dl>
-- @tparam tbl parameters Parameters for the method.
-- <dl>Parameters description:
-- <dt><span class="parameter">xs, ys</span></dt>
-- <dd>x,y start position coordinates of the console.</dd>
-- <dt><span class="parameter">xe, ye</span></dt>
-- <dd>x,y end position coordinates of the console.</dd>
-- <dt><span class="parameter">c</span></dt>
-- <dd>Color.</dd>
-- <dt><span class="parameter">str</span></dt>
-- <dd>String to add to the console.</dd>
-- </dl>
-- @treturn[1] bool True
-- @treturn[2] bool False
-- @treturn[2] str command is not valid
-- @treturn[3] bool false
-- @treturn[3] str Parameters missing.
local function UG_Console(command, parameters)

   -- Process all possible parameters, those which are not defined
   -- should be ignored (and calling them will return nil)
   local xs, ys = parameters.xs, parameters.ys
   local xe, ye = parameters.xe, parameters.ye
   local c = parameters.c
   local str = parameters.str
   
   if command == "setArea" then
      -- Define the active console area

      -- Check if the required parameters are available
      if xs and ys and xe and ye then
	 gui.console.start_c.x = xs
	 gui.console.start_c.y = ys
	 gui.console.end_c.x = xe
	 gui.console.end_c.y = ye

	 return true
      else
	 -- Some parameters are missing
	 return false, "[UG_Console|"..command.."] Error: Some parameters are missing."
      end   
      
   elseif command == "setForecolor" then
      -- Define the fore color of the console

      -- Check if the required parameters are available
      if c then
	 gui.console.color.fore = c

	 return true
      else
	 -- Some parameters are missing
	 return false, "[UG_Console|"..command.."] Error: Some parameters are missing."
      end   
	 
   elseif command == "setBackcolor" then
      -- Define the back color of the console

      -- Check if the required parameters are available
      if c then
	 gui.console.color.back = c

	 return true
      else
	 -- Some parameters are missing
	 return false, "[UG_Console|"..command.."] Error: Some parameters are missing."
      end   
	 
   elseif command == "putString" then
      -- Add a string to the console

      -- Check if the required parameters are available
      if str then
	 local chr
	 local cw = gui.font.char_width
	 local ch = gui.font.char_height
	 local chs = gui.font.char_h_space
	 local cvs = gui.font.char_v_space

	 for chr_pos = 1, str:len(), 1 do
	    chr = str:sub(chr_pos, chr_pos)
	    if chr == "\n" then
	       gui.console.pos_c.x = gui.dim.x
	    else
	       gui.console.pos_c.x = gui.console.pos_c.x + cw + chs
	       if gui.console.pos_c.x + cw > gui.console.end_c.x then
		  gui.console.pos_c.x = gui.console.start_c.x
		  gui.console.pos_c.y = gui.console.pos_c.y + ch + cvs
	       end
	       if gui.console.pos_c.y + ch > gui.console.end_c.y then
		  gui.console.pos_c.x = gui.console.start_c.x
		  gui.console.pos_c.y = gui.console.start_c.y
		  UG_Fill("Frame",{x1=gui.console.start_c.x,
				   y1=gui.console.start_c.y,
				   x2=gui.console.end_c.x,
				   y2=gui.console.end_c.y,
				   c=gui.console.color.back})
	       end
	       UG_Text("putChar", {chr=chr,
				   x=gui.console.pos_c.x,
				   y=gui.console.pos_c.y,
				   fc=gui.console.color.fore,
				   bc=gui.console.color.back})
	    end
	    chr_pos = chr_pos + 1
	 end
	 return true
      else
	 -- Some parameters are missing
	 return false, "[UG_Console|"..command.."] Error: Some parameters are missing."
      end   
	 
   else
      -- wrong command
      return false, "[UG_Console] Error: "..command.."is not a valid command."
   end
end

local pal={
   window = {
      0x646464, 0x646464, 0x646464, 0x646464, -- Frame 0
      0xFFFFFF, 0xFFFFFF, 0x696969, 0x696969, -- Frame 1
      0xE3E3E3, 0xE3E3E3, 0xA0A0A0, 0xA0A0A0  -- Frame 2
   },
   button={
      pressed = {
	 0x646464, 0x646464, 0x646464, 0x646464, -- Frame 0
	 0xA0A0A0, 0xA0A0A0, 0xA0A0A0, 0xA0A0A0, -- Frame 1
	 0xF0F0F0, 0xF0F0F0, 0xF0F0F0, 0xF0F0F0  -- Frame 2
      },
      released = {
	 0x646464, 0x646464, 0x646464, 0x646464, -- Frame 0
	 0xFFFFFF, 0xFFFFFF, 0x696969, 0x696969, -- Frame 1
	 0xE3E3E3, 0xE3E3E3, 0xA0A0A0, 0xA0A0A0  -- Frame 2
      }
   }
}


--- Driver Functions.
-- @section driverfnc

--- Manage accelerated hardware drivers.
-- @tparam str command Method to call.
-- <dl>Valid commands and its parameters:
-- <dt><span class="parameter">register</span></dt>
-- <dd>
-- </br>(Parameters: driver_type, driver)</dd>
-- <dt><span class="parameter">enable</span></dt>
-- <dd>
-- </br>(Parameters: driver_type)</dd>
-- <dt><span class="parameter">disable</span></dt>
-- <dd>
-- </br>(Parameters: driver_type)</dd>
-- </dl>
-- @tparam tbl parameters Parameters for the method.
-- <dl>Parameters description:
-- <dt><span class="parameter">driver_type</span></dt>
-- <dd>Driver type.</dd>
-- <dt><span class="parameter">driver</span></dt>
-- <dd>Driver function.</dd>
-- </dl>
-- @treturn[1] bool True
-- @treturn[2] bool False
-- @treturn[2] str command is not valid
-- @treturn[3] bool false
-- @treturn[3] str Parameters missing.
-- @treturn[4] bool false
-- @treturn[4] str Driver not registered
local function UG_Driver(command, parameters)

   -- Process all possible parameters, those which are not defined
   -- should be ignored (and calling them will return nil)
   local driver_type = parameters.driver_type
   local driver = parameters.driver

   -- driver_type is required for all methods.
   if driver_type then
      
      if command == "register" then
	 if driver then
	    gui.driver[driver_type] = {
	       driver = driver,
	       state = {
		  REGISTERED = true,
		  ENABLED = true
	       }
	    }
	 else
	    return false, "[UG_Driver|"..command.."] Error: driver parameter missing."
	 end

	 return true
	 
      elseif command == "enable" then
	 if (gui.driver[driver_type]  and
	     gui.driver[driver_type].state.REGISTERED) then
	    gui.driver[driver_type].state.ENABLED = true
	 else
	    return false, "[UG_Driver|"..command.."] Warning: "..driver_type.." is not registered."
	 end

	 return true

      elseif command == "disable" then
	 if (gui.driver[driver_type]  and
	     gui.driver[driver_type].state.REGISTERED) then
	    gui.driver[driver_type].state.ENABLED = false
	 else
	    return false, "[UG_Driver|"..command.."] Warning: "..driver_type.." is not registered."
	 end

	 return true

	 
      else
	 -- wrong command
	 return false, "[UG_Driver] Error: "..command.."is not a valid command."

      end
   else
      return false, "[UG_Driver] Error: driver_type parameter missing"
   end
end


--- Internal functions

local function _UG_ProcessTouchData (wnd)
   local xp, yp = gui.touch.xp, gui.touch.yp
   local tchstate = gui.touch.state

   local dbglbl = "[DEBUG|_UG_ProcessTouchData]"
   
   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function BEGINS\n",
	    dbglbl, "- gui.touch.(xp, yp) = (", xp, ",", yp,")\n",
	    dbglbl, "- gui.touch.state:", tchstate)
      print(dbglbl, "- Processing objects:")
   end
      
   for obj_type, objs in pairs(wnd.objlst) do
      for obj_id, obj in pairs(objs) do
	 if gui.debug and gui.debug.internal then
	    print(dbglbl, "   * object type:", obj_type, "| object id:", obj_id)
	    print(dbglbl, "   -> object state:")
	    for k,v in pairs(obj.state) do
	       print(dbglbl, "      -",k,":",v)
	    end
	    print (dbglbl, "   -> object touch state:")
	    for k,v in pairs(obj.touch_state) do
	       print(dbglbl, "      -",k,":",v)
	    end
	 end
	 if obj.state.VALID and obj.state.VISIBLE and not obj.state.REDRAW then
	    -- Process touch data
	    if (tchstate ~= "RELEASED") and (xp ~= -1) and (yp ~= -1) then
	       if gui.debug and gui.debug.internal then
		  print(dbglbl, "   -> Process touch data (touch data is valid)")
	       end
	       if not obj.touch_state.IS_PRESSED then
		  obj.touch_state.PRESSED_OUTSIDE_OBJECT = true
		  obj.touch_state.CHANGED = true
		  obj.touch_state.RELEASED_ON_OBJECT= false
		  obj.touch_state.RELEASED_OUTSIDE_OBJECT = false
		  obj.touch_state.CLICK_ON_OBJECT = false
	       end
	       obj.touch_state.IS_PRESSED_ON_OBJECT = false
	       local area = obj.area.abs
	       if (xp >= area.xs and xp <= area.xe
		   and yp >= area.ys and yp <= area.ye) then
		  if gui.debug and gui.debug.internal then
		     print(dbglbl, "   -> xp,yp inside object abs area ")
		  end
		  obj.touch_state.IS_PRESSED_ON_OBJECT = true
		  if not obj.touch_state.IS_PRESSED then
		     obj.touch_state.PRESSED_OUTSIDE_OBJECT = false
		     obj.touch_state.PRESSED_ON_OBJECT = true
		  end
	       end
	       obj.touch_state.IS_PRESSED = true
	    elseif obj.touch_state.IS_PRESSED then
	       if obj.touch_state.IS_PRESSED_ON_OBJECT then
		  if obj.touch_state.PRESSED_ON_OBJECT then
		     obj.touch_state.CLICK_ON_OBJECT = true
		  end
		  obj.touch_state.RELEASED_ON_OBJECT = true
	       else
		  obj.touch_state.RELEASED_OUTSIDE_OBJECT = true
	       end
	       if obj.touch_state.IS_PRESSED then
		  obj.touch_state.CHANGED = true
	       end
	       obj.touch_state.PRESSED_OUTSIDE_OBJECT = false
	       obj.touch_state.PRESSED_ON_OBJECT = false
	       obj.touch_state.IS_PRESSED = false
	       
	    end
	 end
	 if gui.debug and gui.debug.internal then
	    print(dbglbl, "   -> new touch state:")
	    for k,v in pairs(obj.touch_state) do
	       print(dbglbl, "      -",k,":",v)
	    end
	    print(dbglbl, "   * end of object processing")
	 end
      end
   end
   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function ENDS")
   end
end

local function _UG_UpdateObjects( wnd )

   local dbglbl = "[DEBUG|_UG_UpdateObjects]"
   
   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function BEGINS")
   end
   
   -- Check each object, if it needs to be updated?
   for obj_type, objs in pairs(wnd.objlst) do
      for obj_id, obj in pairs(objs) do
	 if obj.state.VALID then
	    if obj.state.UPDATE then
	       if gui.debug and gui.debug.internal then
		  print(dbglbl, " - Updating (independent of touch)",
			obj_type, "id: ",obj_id)
	       end
	       obj.update(wnd, obj_type, obj_id)
	    end
	    if obj.state.VISIBLE and obj.state.TOUCH_ENABLE then
	       if obj.touch_state.CHANGED or obj.touch_state.IS_PRESSED then
		  if gui.debug and gui.debug.internal then
		     print(dbglbl, " - Updating (triggered by touch)",
			   obj_type, "id: ",obj_id)
		  end
		  obj.update(wnd, obj_type, obj_id)
	       end
	    end
	 end
      end
   end
   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function ENDS")
   end
end

local function _UG_HandleEvents( wnd )
   local msg = {}

   local dbglbl = "[DEBUG|_UG_HandleEvents]"
   
   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function BEGINS")
   end
   
   -- @todo Handle window-related events

   -- Handle object related events
   msg.msg_type="OBJECT"
   for obj_type, objs in pairs(wnd.objlst) do
      for obj_id, obj in pairs(objs) do
	 if obj.state.VALID then
	    if obj.event ~= "NONE" then
	       msg.src = obj
	       msg.src_type = obj_type
	       msg.src_id = obj_id
	       if gui.debug and gui.debug.internal then
		  print(dbglbl, " - Call wnd.cb: msg.src_type =", msg.src_type,
			"| msg.src_id =",msg.src_id)
	       end
	       wnd.cb(msg)
	       obj.event = "NONE"
	    end
	 end
      end
   end
   
   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function ENDS")
   end
   
end

local function _UG_PutText( txt )
   if not txt.font.p then return end
   local str = txt.str
   local xs, ys, xe, ye = txt.a.xs, txt.a.ys, txt.a.xe, txt.a.ye
   local align = txt.align
   local char_width = txt.font.char_width
   local char_height = txt.font.char_height
   local char_h_space = txt.h_space
   local char_v_space = txt.v_space
   local xp, yp

   local dbglbl = "[DEBUG|_UG_PutText]"

   
   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function BEGINS")
   end
   

   -- save current font
   local current_font = gui.font
   -- select font in txt object
   UG_Text("selectFont", {font=txt.font})

    -- Return if basic conditions not met
   if not str then return end
   if (ye -ys) < txt.font.char_height then return end

   -- count rows
   local _, rc = str:gsub('\n', '\n')
   rc = rc + 1

   yp = 0

   -- calculate yp
   if align.v == "CENTER" or align.v == "BOTTOM" then
      yp = ye - ys + 1
      yp = yp - char_height * rc
      yp = yp - char_v_space * (rc - 1)
      if yp < 0 then return end
   end
   if align.v == "CENTER" then yp = math.floor(yp/2) end
   yp = yp + ys

   -- process each line in string
   for line in string.gmatch(str, "[^\n]+") do
      sl = line:len()
      -- calculate xp
      xp = xe - xs + 1
      xp = xp - char_width * sl
      xp = xp - char_h_space * (sl-1)
      if xp < 0  then return end

      if align.h == "LEFT" then xp=0
      elseif align.h == "CENTER" then xp = math.floor(xp/2) end
      xp = xp + xs

      if gui.debug and gui.debug.internal then
	 print(dbglbl, " - Processing line starting at (x,y) = ("..xp..","..yp..")")
      end
      
      -- process each char in the line
      for chr_pos = 1, sl, 1 do
	 local chr = str:sub(chr_pos, chr_pos)
	 UG_Text("putChar", {chr=chr, x=xp, y=yp, fc=txt.fc, bc=txt.bc})
	 xp = xp + char_width + char_h_space
      end
      yp = yp + char_height + char_v_space
   end

   -- reset font back to the saved one
   UG_Text("selectFont", {font=current_font})

   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function ENDS")
   end
   
end

local function _UG_WindowDrawTitle( wnd )
   local txt = {}
   local xs, ys, xe, ye
   if wnd and wnd.state.VALID then
      xs, ys, xe, ye = wnd.xs, wnd.ys, wnd.xe, wnd.ye

      -- 3D style?
      if wnd.style.ThreeD then
	 xs = xs + 3
	 ys = ys + 3
	 xe = xe - 3
	 ye = ye - 3
      end

      -- Is the window active or inactive?
      if ( wnd == gui.window.active ) then
	 txt.bc = wnd.title.bc
	 txt.fc = wnd.title.fc
      else
	 txt.bc = wnd.title.ibc
	 txt.fc = wnd.title.ifc
      end

      -- Draw title
      UG_Fill("Frame", {x1=xs, y1=ys,
			x2=xe, y2=ys + wnd.title.height - 1,
			c=txt.bc})

      -- Draw title text
      txt.str = wnd.title.str
      txt.font = wnd.title.font
      txt.a = {}
      txt.a.xs = xs + 3
      txt.a.ys = ys
      txt.a.xe = xe
      txt.a.ye = ys + wnd.title.height - 1
      txt.align = wnd.title.align
      txt.h_space = wnd.title.h_space
      txt.v_space = wnd.title.v_space
      _UG_PutText( txt )

      -- Draw line
      UG_Draw("Line", {x1=xs, y1=ys+wnd.title.height,
		       x2=xe, y2=ys+wnd.title.height,
		       c=pal.window[11+1]})
      return true
   end
   return false
end

local function _UG_DrawObjectFrame(xs, ys ,xe, ye, p)
   -- Frame 0
   UG_Draw("Line", {x1=xs,   y1=ys  , x2=xe-1, y2=ys  , c=p[1]})
   UG_Draw("Line", {x1=xs,   y1=ys+1, x2=xs  , y2=ye-1, c=p[2]})
   UG_Draw("Line", {x1=xs,   y1=ye  , x2=xe  , y2=ye  , c=p[3]})
   UG_Draw("Line", {x1=xe,   y1=ys  , x2=xe  , y2=ye-1, c=p[4]})
   -- Frame 1
   UG_Draw("Line", {x1=xs+1, y1=ys+1, x2=xe-2, y2=ys+1, c=p[5]})
   UG_Draw("Line", {x1=xs+1, y1=ys+2, x2=xs+1, y2=ye-2, c=p[6]})
   UG_Draw("Line", {x1=xs+1, y1=ye-1, x2=xe-1, y2=ye-1, c=p[7]})
   UG_Draw("Line", {x1=xe-1, y1=ys+1, x2=xe-1, y2=ye-2, c=p[8]})
   -- Frame 2
   UG_Draw("Line", {x1=xs+2, y1=ys+2, x2=xe-3, y2=ys+2, c=p[9]})
   UG_Draw("Line", {x1=xs+2, y1=ys+3, x2=xs+2, y2=ye-3, c=p[10]})
   UG_Draw("Line", {x1=xs+2, y1=ye-2, x2=xe-2, y2=ye-2, c=p[11]})
   UG_Draw("Line", {x1=xe-2, y1=ys+2, x2=xe-2, y2=ye-3, c=p[12]})
end


local function _UG_WindowUpdate( wnd )
   local xs, ys, xe, ye = wnd.xs, wnd.ys, wnd.xe, wnd.ye

   wnd.state.UPDATE = false
   -- Is the window visible?
   if wnd.state.VISIBLE then
      -- 3D style?
      if wnd.style.ThreeD and not wnd.state.REDRAW_TITLE then
	 _UG_DrawObjectFrame(xs, ys, xe, ye, pal.window)
	 xs = xs + 3
	 ys = ys + 3
	 xe = xe - 3
	 ye = ye - 3
      end
      -- Show title bar?
      if wnd.style.SHOW_TITLE then
	 _UG_WindowDrawTitle( wnd )
	 ys = ys + wnd.title.height + 1
	 if wnd.state.REDRAW_TITLE then
	    wnd.state.REDRAW_TITLE = false
	    return
	 end
      end
      -- Draw window area?
      UG_Fill("Frame", {x1=xs, y1=ys, x2=xe, y2=ye, c=wnd.bc})

      -- Force each object to be updated!
      for obj_type, objs in pairs(wnd.objlst) do
	 for obj_id, obj in pairs(objs) do
	    if obj.state.VALID and obj.state.VISIBLE then
	       obj.state.UPDATE = true
	       obj.state.REDRAW = true
	    end
	 end
      end
   else
      UG_Fill("Frame", {x1=wnd.xs, y1=wnd.xs, x2=wnd.xe, y2=wnd.ye,
			c=gui.desktop_color})
   end	    
   
end


-- _UG_SearchObject( wnd, obj_type, id ) -- replaced by direct access to object in objlst
-- _UG_DeleteObject( wnd, obj_type, id ) -- only used by UG_*Delete functions

--- Miscellaneous Structures and Functions
-- @section miscfnc

-- UG_Update(): _UG_WindowDrawTitle, _UG_WindowUpdate, _UG_ProcessTouchData, _UG_UpdateObjects, _UG_HandleEvents
-- UG_WaitForUpdate()
-- UG_DrawBMP(xp, yp, bmp) moved into UG_Draw methods
-- UG_TouchUpdate(xp, yp, state)


--- Update Function.
-- μGUI controls the refreshing process of each window
-- including all daughter objects. This is all
-- done by the function <code>UG_Update</code>. Therefore, this
-- function has to be called periodically either in
-- an ISR or in background.
--
-- Note: If the user forgets to call this function, nothing will happen.
local function UG_Update()
   local wnd

   local dbglbl = "[DEBUG|UG_Update]"
   
   if gui.debug and gui.debug.misc then
      print(dbglbl, "Function BEGINS")
      print(dbglbl, "- active:", gui.window.active,
	    "| next:", gui.window.next,
	    "| last:", gui.window.last)
   end
   
   -- Is somebody waiting for this update? Reset state
   
   if gui.state.WAIT_FOR_UPDATE then gui.state.WAIT_FOR_UPDATE = false end

   -- Keep track of the windows
   if gui.window.next ~= gui.window.active then
      if gui.debug  and gui.debug.misc then
	 print(dbglbl, " - Next and Active window differ")
      end
      if gui.window.next then
	 if gui.debug  and gui.debug.misc then
	    print(dbglbl, " - Next window available, shift the windows.")
	 end
	 gui.window.last = gui.window.active
	 gui.window.active = gui.window.next

	 -- Do we need to draw an inactive title?
	 if (gui.window.last and gui.window.last.style.SHOW_TITLE
	     and gui.window.last.state.VISIBLE) then
	    if gui.debug  and gui.debug.misc then
	       print(dbglbl, " - An inactive title needs to be drawn.")
	    end
	    -- Do both windows differ in size/position?
	    if ( (gui.window.last.xs ~= gui.window.active.xs) or
		  (gui.window.last.xe ~= gui.window.active.xe) or
		  (gui.window.last.ys ~= gui.window.active.ys) or
		  (gui.window.last.ye ~= gui.window.active.ye) )
	    then
	       -- Redraw title of the last window
	       if gui.debug  and gui.debug.misc then
		  print(dbglbl, " - Redrawing last window's title.")
	       end
	       _UG_WindowDrawTitle ( gui.last_window )
	    end
	 end
	 gui.window.active.state.REDRAW_TITLE = false
	 gui.window.active.state.UPDATE = true
	 gui.window.active.state.VISIBLE = true
      end
   end

   -- Is there an active window?
   if gui.window.active then
      if gui.debug  and gui.debug.misc then
	 print(dbglbl, " - Active window exists.")
      end
      wnd = gui.window.active

      -- Does the window need to be updated?
      if wnd.state.UPDATE then
	 if gui.debug  and gui.debug.misc then
	    print(dbglbl, " - Active window needs update, updating!")
	 end
	 -- Do it!
	 _UG_WindowUpdate( wnd )
      end

      -- Is the window visible?
      if wnd.state.VISIBLE then
	 if gui.debug  and gui.debug.misc then
	    print(dbglbl, " - Active window is visible.",
		  "Processing touch data.",
		  "Updating objects.",
		  "Handling events.")
	 end
	 _UG_ProcessTouchData( wnd )
	 _UG_UpdateObjects( wnd )
	 _UG_HandleEvents( wnd )
      end
   end
   
   if gui.debug and gui.debug.misc then
      print(dbglbl, "Function ENDS")
   end
   
end

--- Wait for update.
-- "Blocking" function waiting until some other method trigers and update.
local function UG_WaitForUpdate()
   gui.state.WAIT_FOR_UPDATE = true
   while gui.state.WAIT_FOR_UPDATE do end
end

--- Touch Update
-- μ GUI supports any touch technology (like analog resistive or
-- projected capacitive) as long as it provides input data in X/Y format.
-- It is very simple to connect a touch device to μ GUI. The only
-- thing the user needs to do is to call UG_TouchUpdate().
-- This function transfers the raw touch data to the μ GUI touch
-- processor which then will detect, validate and track all touch events.
-- @param xp X coordinate
-- @param yp Y coordinate
-- @param state State. Valid states are
-- <code>"PRESSED"</code> or <code>"RELEASED"</code>.
local function UG_TouchUpdate(xp, yp, state)
   gui.touch = {xp = xp, yp = yp, state = state}
end

--- Window Structures and Functions.
-- @section windowfnc

--- Manage window objects.
-- @tparam tbl wnd Window object.
-- @tparam str command Method to call.
-- <dl>Valid commands and its parameters:
-- <dt><span class="parameter">create</span></dt>
-- <dd>Creates a window.
-- </br>(Parameters: <code>cb</code>)</dd>
-- <dt><span class="parameter">delete</span></dt>
-- <dd>Deletes a window.
-- </br>(Parameters: None)</dd>
-- <dt><span class="parameter">show</span></dt>
-- <dd>Shows a window.
-- </br>(Parameters: None)</dd>
-- <dt><span class="parameter">hide</span></dt>
-- <dd>Hides a window.
-- </br>(Parameters: None)</dd>
-- <dt><span class="parameter">resize</span></dt>
-- <dd>Changes the size of a window.
-- </br>(Parameters: <code>xs, ys, xe, ye</code>)</dd>
-- <dt><span class="parameter">alert</span></dt>
-- <dd>Swaps fore- and back color of the window title.
-- </br>(Parameters: None)</dd>
-- </dl>
-- @tparam tbl parameters Parameters for the method.
-- <dl>Parameters description:
-- <dt><span class="parameter">cb</span></dt>
-- <dd>The callback function of the window</dd>
-- <dt><span class="parameter">xs, ys</span></dt>
-- <dd>Window's start position (x,y) coordinates</dd>
-- <dt><span class="parameter">xe, ye</span></dt>
-- <dd>Window's start position (x,y) coordinates</dd>
-- </dl>
-- @treturn[1] bool True
-- @treturn[2] bool False
-- @treturn[2] str command is not valid.
-- @treturn[3] bool false
-- @treturn[3] str Parameters missing.
-- @treturn[4] bool false
-- @treturn[4] str Invalid window or active window can't be deleted.
-- @treturn[5] bool false
-- @treturn[5] str Parameters out of range.
local function UG_Window(wnd, command, parameters)
   -- Process all possible paramaters, those which are not defined
   -- should be ignored (and calling them will return nil)
   local cb, xs, ys, xe, ye
   if parameters then
      cb = parameters.cb
      xs, ys = parameters.xs, parameters.ys
      xe, ye = parameters.xe, parameters.ye
   end
   
   
   if command == "create" then
      local gui_dimensions = UG_GUI("getDim")
      
      if wnd and cb then
	 wnd.objlst = {}
	 wnd.state = {VALID=true}
	 wnd.fc = 0x000000
	 wnd.bc = 0xF0F0F0
	 wnd.xs = 0
	 wnd.ys = 0
	 wnd.xe = gui_dimensions.x - 1
	 wnd.ye = gui_dimensions.y - 1
	 wnd.cb = cb
	 wnd.style = {ThreeD = true,
		      SHOW_TITLE = true}
	 wnd.title = {str = "",
		      font= nil,
		      h_space = 2,
		      v_space = 2,
		      align = {v="CENTER", h="LEFT"},
		      fc = C.WHITE,
		      bc = C.BLUE,
		      ifc = C.WHITE,
		      ibc = C.GRAY,
		      height = 15}
      else
	 -- missing parameters
	 return false, "[UG_Window|"..command.."] Error: Some parameters are missing."
      end
      
   elseif command == "delete" then
      -- Do not delete the active window
      if wnd == gui.window.active then
	 return false, "[UG_Window|"..command.."] Error: Can't delete active window."
      end
      
      if wnd and wnd.state.VALID then
	 for k, v in pairs(wnd.state) do wnd.state[k] = false end
	 wnd.cb=nil
	 wnd.objlst = nil
	 wnd.xs = 0
	 wnd.ys = 0
	 wnd.xe = 0
	 wnd.ye = 0
	 for k, v in pairs(wnd.style) do wnd.style[k] = false end
      else
	 return false, "[UG_Window|"..command.."] Error: Invalid window or missing window parameter."
      end
      
   elseif command == "show" then
      if wnd then
	 -- Force an update, even if this is the active window!
	 wnd.state.VISIBLE = true
	 wnd.state.UPDATE = true
	 wnd.state.REDRAW_TITLE = false
	 gui.window.next = wnd
      else
	 return false, "[UG_Window|"..command.."] Error: Missing window parameter."
      end
      
   elseif command == "hide" then
      if wnd then
	 if wnd == gui.window.active then
	    -- Is there an old window which just lost the focus?
	    if gui.window.last and gui.window.last.state.VISIBLE then
	       if ( gui.window.last.xs > wnd.xs or
		       gui.window.last.ys > wnd.ys or
		       gui.window.last.xe < wnd.xe or
		       gui.window.last.ye < wnd.ye)
	       then  _UG_Window("clear", wnd)  end
	       gui.window.next = gui.window.last
	    else
	       gui.window.active.state.VISIBLE = false
	       gui.window.active.state.UPDATE = true
	    end
	 else
	    -- If the old window is visible, clear it!
	    _UG_Window("clear", wnd )
	 end
      else
	 return false, "[UG_Window|"..command.."] Error: Missing window parameter."
      end
      
   elseif command == "resize" then
      if wnd and xs and ys and xe and ye then
      	 local pos
      	 local gui_dimensions = UG_GUI("getDim")
      	 local xmax, ymax = gui_dimensions.x, gui_dimensions.y
	 
      	 if wnd.state.VALID then
      	    if (xs < 0) or (ys < 0) or (xe > xmax) or (ye > ymax) then
      	       return false, "[UG_Window|"..command.."] Error: New size is out of range."
      	    end
      	    pos = xe - xs
      	    if ( pos < 10 ) then
      	       return false, "[UG_Window|"..command.."] Error: New width is too small (<10)."
      	    end
      	    pos = ye-ys
      	    if ( pos < 10 ) then
      	       return false, "[UG_Window|"..command.."] Error: New height is too small (<10)."
      	    end
	    
	    -- ... and if everything is OK move the window!
      	    wnd.xs, wnd.ys, wnd.xe, wnd.ye = xs, ys, xe, ye
	    
      	    -- Redraw uncovered desktop areas
      	    if wnd.state.VISIBLE and gui.window.active == wnd then
      	       if wnd.ys > 0 then
      		  UG_Fill("Frame", {x1=0, y1=0, x2=xmax, y2=wnd.ys-1,
      				    c=gui.desktop_color})
      		  pos = wnd.ye + 1
      	       end
      	       if s <= ymax then
      		  UG_Fill("Frame", {x1=0, y1=pos, x2=xmax, y2=ymax,
      				    c=gui.desktop_color})
      	       end
      	       if wnd.xs > 0 then
      		  UG_Fill("Frame", {x1=0, y1=wnd.ys, x2=wnd.xs-1, y2=wnd.ye,
      				    c=gui.desktop_color})
      	       end
      	       pos = wnd.xe + 1
      	       if pos <= xmax then
      		  UG_Fill("Frame", {x1=pos, y1=wnd.ys, x2=xmax, y2=wnd.ye,
      				    c=gui.desktop_color})
      	       end
	       
      	       wnd.state.REDRAW_TITLE = false
      	       wnd.state.UPDATE = true
      	    end
      	 else
      	    return false, "[UG_Window|"..command.."] Error: Invalid window."
      	 end
	 
      else
      	 -- missing parameters
      	 return false, "[UG_Window|"..command.."] Error: Some parameters are missing."
      end
      
   elseif command == "alert" then
      if wnd then
	 local title_text_color = wnd.title.fc
	 local title_color = wnd.title.bc
	 wnd.title.fc = title_color
	 wnd.title.bc = title_text_color
      else
	 return false, "[UG_Window|"..command.."] Error: Missing window parameter."
      end
      
   else
      -- wrong command
      return false, "[UG_Window] Error: "..command.."is not a valid command."
      
   end   
   
   return true
end

--- Set window's properties.
-- @tparam tbl wnd Window object.
-- @tparam str property Property to update
-- <dl>Valid properties:
-- <dt><span class="parameter">ForeColor</span></dt>
-- <dd>Changes the fore color of the window.
-- </dd>
-- <dt><span class="parameter">BackColor</span></dt>
-- <dd>Changes the back color of the window.
-- </dd>
-- <dt><span class="parameter">TitleTextColor</span></dt>
-- <dd>Changes the text color of the window title.
-- </dd>
-- <dt><span class="parameter">TitleColor</span></dt>
-- <dd>Changes the color of the window title.
-- </dd>
-- <dt><span class="parameter">TitleInactiveTextColor</span></dt>
-- <dd>Changes the inactive text color of the window title.
-- </dd>
-- <dt><span class="parameter">TitleInactiveColor</span></dt>
-- <dd>Changes the inactive color of the window title.
-- </dd>
-- <dt><span class="parameter">TitleText</span></dt>
-- <dd>Changes the text of the window title.
-- </dd>
-- <dt><span class="parameter">TitleTextFont</span></dt>
-- <dd>Changes the text font of the window title.
-- </dd>
-- <dt><span class="parameter">TitleTextHSpace</span></dt>
-- <dd>Changes the horizontal space between the characters in the window title.
-- </dd>
-- <dt><span class="parameter">TitleTextVSpace</span></dt>
-- <dd>Changes the vertical space between the characters in the window title.
-- </dd>
-- <dt><span class="parameter">TitleTextAlignment</span></dt>
-- <dd>Changes the alignment of the text in the window title.
-- </br><code>value</code> should be a table with entries
-- <code>v</code> or <code>h</code> representing vertical or horizontal
-- alignement, respectively. Both entries are optional. If not specified,
-- the current value for the unespecified property remains unchanged.
-- <ul>Valid vertical alignment values are:
-- <li>"TOP"</li><li>"CENTER"</li><li>"BOTTOM"</li></ul>
-- <ul>Valid horizontal alignment values are:
-- <li>"LEFT"</li><li>"CENTER"</li><li>"RIGHT"</li></ul>
-- </dd>
-- <dt><span class="parameter">TitleHeight</span></dt>
-- <dd>Changes the height of the window title.
-- </dd>
-- <dt><span class="parameter">XStart</span></dt>
-- <dd>Changes the x start position of the window.
-- </dd>
-- <dt><span class="parameter">YStart</span></dt>
-- <dd>Changes the y start position of the window.
-- </dd>
-- <dt><span class="parameter">XEnd</span></dt>
-- <dd>Changes the x end position of the window.
-- </dd>
-- <dt><span class="parameter">YEnd</span></dt>
-- <dd>Changes the y end position of the window.
-- </dd>
-- <dt><span class="parameter">Style</span></dt>
-- <dd>Changes the style of the window.
-- </br><code>value</code> should be a table with entries
-- <code>ThreeD</code> or <code>SHOW_TITLE</code>. Both are boolean properties. 
-- The first one chooses between 3D (true) or 2D (false) window style.
-- The second one determines if the window title is shown (true) or not (false).
-- Both entries are optional. If not specified,
-- the current value for the unespecified property remains unchanged.
-- </dd>
-- </dl>
-- @param value New value for property
-- @treturn[1] bool true
-- @treturn[2] bool false
-- @treturn[2] str Missing window parameter or invalid window.
-- @treturn[3] bool false
-- @treturn[3] str Invalid property.
-- @treturn[4] bool false
-- @treturn[4] str Missing property value.
local function UG_WindowSet(wnd, property, value)
   
   if wnd and wnd.state.VALID then
      if value then
	 -- most methods set UPDATE or REDRAW_TITLE true
	 -- let's set them to true for all of them and then
	 -- set them to their previous value when appropriate
	 -- (it makes the following code slightly less "crowded")
	 -- @todo check that the state is not processed before changing
	 -- the property value (by an async call to UG_Update, for instance)
	 local previous_UPDATE = wnd.state.UPDATE
	 local previous_REDRAW_TITLE = wnd.state.REDRAW_TITLE
	 wnd.state.UPDATE = true
	 wnd.state.REDRAW_TITLE = true
	 
	 if property == "ForeColor" then
	    wnd.fc = value
	    wnd.state.REDRAW_TITLE = previous_REDRAW_TITLE
	 elseif property == "BackColor" then
	    wnd.bc = value
	    wnd.state.REDRAW_TITLE = previous_REDRAW_TITLE
	 elseif property == "TitleTextColor" then
	    wnd.title.fc = value
	 elseif property == "TitleColor" then
	    wnd.title.bc = value
	 elseif property == "TitleInactiveTextColor" then
	    wnd.title.ifc = value
	 elseif property == "TitleInactiveColor" then
	    wnd.title.ibc = value
	 elseif property == "TitleText" then
	    wnd.title.str = value
	 elseif property == "TitleTextFont" then
	    wnd.title.font = value
	    if wnd.title.height <= (wnd.title.font.char_height + 1) then
	       wnd.title.height = wnd.title.font.char_height + 2
	       wnd.state.REDRAW_TITLE = false
	    end
	 elseif property == "TitleTextHSpace" then
	    wnd.title.h_space = value
	 elseif property == "TitleTextVSpace" then
	    wnd.title.v_space = value
	 elseif property == "TitleTextAlignment" then
	    wnd.title.align.v = value.v or wnd.title.align.v
	    wnd.title.align.h = value.h or wnd.title.align.h
	 elseif property == "TitleHeight" then
	    wnd.title.height = value
	 elseif property == "XStart" then
	    wnd.state.UPDATE = previous_UPDATE
	    wnd.state.REDRAW_TITLE = previous_REDRAW_TITLE
	    wnd.xs = value
	    return UG_Window("resize", wnd, {xs=wnd.xs, ys=wnd.ys,
					     xe=wnd.xw, ye=wnd.ye})
	 elseif property == "YStart" then
	    wnd.state.UPDATE = previous_UPDATE
	    wnd.state.REDRAW_TITLE = previous_REDRAW_TITLE
	    wnd.ys = value
	    return UG_Window("resize", wnd, {xs=wnd.xs, ys=wnd.ys,
					     xe=wnd.xw, ye=wnd.ye})	    
	 elseif property == "XEnd" then
	    wnd.state.UPDATE = previous_UPDATE
	    wnd.state.REDRAW_TITLE = previous_REDRAW_TITLE
	    wnd.xe = value
	    return UG_Window("resize", wnd, {xs=wnd.xs, ys=wnd.ys,
					     xe=wnd.xw, ye=wnd.ye})
	 elseif property == "YEnd" then
	    wnd.state.UPDATE = previous_UPDATE
	    wnd.state.REDRAW_TITLE = previous_REDRAW_TITLE
	    wnd.ye = value
	    return UG_Window("resize", wnd, {xs=wnd.xs, ys=wnd.ys,
					     xe=wnd.xw, ye=wnd.ye})
	 elseif property == "Style" then
	    wnd.state.REDRAW_TITLE = previous_REDRAW_TITLE
	    wnd.style.ThreeD = value.ThreeD or wnd.style.ThreeD
	    wnd.style.SHOW_TITLE = value.SHOW_TITLE or wnd.style.SHOW_TITLE
	 else
	    -- wrong property
	    return false, "[UG_WindowSet] Error: "..property.."is not a valid property."
	 end
      else
	 return false, "[UG_WindowSet] Error: Missing property value."
      end
   else
      return false, "[UG_WindowSet] Error: Missing window parameter or window is not VALID."
   end

   return true
end

--- Get window's properties.
-- @tparam tbl wnd Window object.
-- @tparam str property Property to get.
-- <dl>Valid properties:
-- <dt><span class="parameter">ForeColor</span></dt>
-- <dd>Returns the fore color of the window.
-- </dd>
-- <dt><span class="parameter">BackColor</span></dt>
-- <dd>Returns the back color of the window.
-- </dd>
-- <dt><span class="parameter">TitleTextColor</span></dt>
-- <dd>Returns the text color of the window title.
-- </dd>
-- <dt><span class="parameter">TitleColor</span></dt>
-- <dd>Returns the color of the window title.
-- </dd>
-- <dt><span class="parameter">TitleInactiveTextColor</span></dt>
-- <dd>Returns the inactive text color of the window title.
-- </dd>
-- <dt><span class="parameter">TitleInactiveColor</span></dt>
-- <dd>Returns the inactive color of the window title.
-- </dd>
-- <dt><span class="parameter">TitleText</span></dt>
-- <dd>Returns the text of the window title.
-- </dd>
-- <dt><span class="parameter">TitleTextFont</span></dt>
-- <dd>Returns the text font of the window title.
-- </dd>
-- <dt><span class="parameter">TitleTextHSpace</span></dt>
-- <dd>Returns the horizontal space between the characters in the window title.
-- </dd>
-- <dt><span class="parameter">TitleTextVSpace</span></dt>
-- <dd>Returns the vertical space between the characters in the window title.
-- </dd>
-- <dt><span class="parameter">TitleTextAlignment</span></dt>
-- <dd>Returns the alignment of the text in the window title.
-- </dd>
-- <dt><span class="parameter">TitleHeight</span></dt>
-- <dd>Returns the height of the window title.
-- </dd>
-- <dt><span class="parameter">XStart</span></dt>
-- <dd>Returns the x start position of the window.
-- </dd>
-- <dt><span class="parameter">YStart</span></dt>
-- <dd>Returns the y start position of the window.
-- </dd>
-- <dt><span class="parameter">XEnd</span></dt>
-- <dd>Returns the x end position of the window.
-- </dd>
-- <dt><span class="parameter">YEnd</span></dt>
-- <dd>Returns the y end position of the window.
-- </dd>
-- <dt><span class="parameter">Style</span></dt>
-- <dd>Returns the style of the window.
-- </dd>
-- </dl>
-- @treturn[1] bool true
-- @treturn[2] bool false
-- @treturn[2] str Missing window parameter or invalid window.
-- @treturn[3] bool false
-- @treturn[3] str Invalid property.
local function UG_WindowGet(wnd, property)
   if wnd and wnd.state.VALID then
      if property == "ForeColor" then
	 return (wnd.fc or C.BLACK)
      elseif property == "BackColor" then
	 return (wnd.bc or C.BLACK)
      elseif property == "TitleTextColor" then
	 return (wnd.title.fc or C.BLACK)
      elseif property == "TitleColor" then
	 return (wnd.title.bc or C.BLACK)
      elseif property == "TitleInactiveTextColor" then
	 return (wnd.title.ifc or C.BLACK)
      elseif property == "TitleInactiveColor" then
	 return (wnd.title.ibc or C.BLACK)
      elseif property == "TitleText" then
	 return (wnd.title.str)
      elseif property == "TitleTextFont" then
	 return (wnd.title.font)
      elseif property == "TitleTextHSpace" then
	 return (wnd.title.h_space or 0)
      elseif property == "TitleTextVSpace" then
	 return (wnd.title.v_space or 0)
      elseif property == "TitleTextAlignment" then
	 return (wnd.title.align or {v="CENTER", h="LEFT"})
      elseif property == "TitleHeight" then
	 return (wnd.title.height or 0)
      elseif property == "XStart" then
	 return (wnd.xs or -1)
      elseif property == "YStart" then
	 return (wnd.ys or -1)
      elseif property == "XEnd" then
	 return (wnd.xe or -1)
      elseif property == "YEnd" then
	 return (wnd.ye or -1)
      elseif property == "Style" then
	 return (wnd.style or {ThreeD = true, SHOW_TITLE = true})
      elseif property == "Area" then
	 local a = {xs=wnd.xs, ys=wnd.ys,
		    xe=wnd.xe, ye=wnd.ye}
	 if wnd.style.ThreeD then
	    for k,v in pairs(a) do a[k] = v + 3 end
	 end
	 if wnd.style.SHOW_TITLE then
	    a.ys = a.ys + wnd.title.height + 1
	 end
	 return a	    
      elseif property == "InnerWidth" then
	 local w = wnd.xe - wnd.xs
	 if wnd.style.ThreeD then w = w - 6 end
	 if w < 0 then w = 0 end
	 return (w or 0)
      elseif property == "OuterWidth" then
	 local w = wnd.xe - wnd.xs
	 if w < 0 then w = 0 end
	 return (w or 0)
      elseif property == "InnerHeight" then
	 local h = wnd.ye - wnd.ys
	 if wnd.style.ThreeD then h = h - 6 end
	 if h < 0 then h = 0 end
	 return (h or 0)
      elseif property == "OuterHeight" then
	 local h = wnd.ye - wnd.ys
	 if h < 0 then h = 0 end
	 return (h or 0)
      else
	 -- wrong property
	 return false, "[UG_WindowSet] Error: "..property.."is not a valid property."
      end
   else
      return false, "[UG_WindowSet] Error: Missing window parameter or window is not VALID."
   end

end

local function _UG_ObjectUpdate(wnd, obj_type, obj_id)
   -- debug msg setup
   local dbglbl = "[DEBUG|_UG_ObjectUpdate]"
   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function BEGINS for", obj_type, obj_id)
   end
   
   -- Get object
   local obj = wnd.objlst[obj_type][obj_id]
   if not obj then
      if gui.debug and gui.debug.internal then
	 print(dbglbl, "Object not found.")
	 print(dbglbl, "Function ENDS")
      end
      return
   end
   
   -- Get object-specific data
   local data = obj.data
   if not data then
      if gui.debug and gui.debug.internal then
	 print(dbglbl, "Object data not found.")
	 print(dbglbl, "Function ENDS")
      end
      return
   end

   -- Object touch section
   -- Currently only the BUTTON object respond to touch events.
   -- (more objects can be added)
   if obj_type == "BUTTON" then
      if obj.touch_state.CHANGED then
	 if gui.debug and gui.debug.internal then
	    print(dbglbl, "Touch state is CHANGED.")
	 end
	 -- Handle 'click' event
	 if obj.touch_state.CLICK_ON_OBJECT then
	    if gui.debug and gui.debug.internal then
	       print(dbglbl, "Handle click event.")
	    end
	    obj.event = "CLICKED"
	    obj.state.UPDATE = true
	 end
	 -- Is the button pressed down?
	 if obj.touch_state.PRESSED_ON_OBJECT then
	    data.state.PRESSED = true
	    obj.state.UPDATE = true
	 elseif data.state.PRESSED then
	    data.state.PRESSED = false
	    obj.state.UPDATE = true
	 end
	 obj.touch_state.CHANGED = false
      end
   end

   -- Object update section
   if obj.state.UPDATE then
      if obj.state.VISIBLE then
	 -- Full redraw necessary?
	 if ( obj.state.REDRAW
		 or (obj_type == "BUTTON" and data.state.ALWAYS_REDRAW) )
	 then
	    if gui.debug and gui.debug.internal then
	       print(dbglbl, "Full redraw is necessary.")
	    end
	    local a = UG_WindowGet(wnd, "Area")
	    -- Recalculate absolute object area
	    obj.area.abs.xs = obj.area.rel.xs + a.xs
	    obj.area.abs.ys = obj.area.rel.ys + a.ys
	    obj.area.abs.xe = obj.area.rel.xe + a.xs
	    obj.area.abs.ye = obj.area.rel.ye + a.ys
	    if obj_type == "IMAGE" then
	       
	       -- @todo why not calculate rel{xe, ye} when creating
	       -- the image
	       obj.area.abs.xe = obj.area.rel.xs + data.img.width + a.xs
	       obj.area.abs.ye = obj.area.rel.ys + data.img.height + a.ys
	    end
	    -- return if object is bigger than window
	    if (obj.area.abs.ye >= wnd.ye) then
	       if gui.debug and gui.debug.internal then
		  print(dbglbl, "Object is higher than its window.")
		  print(dbglbl, "Function ENDS")
	       end
	       return
	    end
	    if (obj.area.abs.xe >= wnd.xe) then
	       if gui.debug and gui.debug.internal then
		  print(dbglbl, "Object is wider than its window.")
		  print(dbglbl, "Function ENDS")
	       end
	       return
	    end
	    
	    -- Draw object
	    if obj_type == "IMAGE" then
	       if data.img and (data.img_type == "IMG_TYPE_BMP") then
		  UG_Draw("BMP", {x0=obj.area.abs.xs,
				  y0=obj.area.abs.ys,
				  bmp=data.img})
	       end
	       
	    elseif obj_type == "TEXTBOX" then
	       
	       local txt = {}
	       txt.bc = data.bc
	       txt.fc = data.fc

	       -- Draw textbox background
	       UG_Fill("Frame", {x1=obj.area.abs.xs,
				 y1=obj.area.abs.ys,
				 x2=obj.area.abs.xe,
				 y2=obj.area.abs.ye,
				 c=txt.bc})

	       -- Draw textbox text
	       txt.a = {}
	       for k,v in pairs(obj.area.abs) do txt.a[k] = v end
	       txt.align = data.align
	       txt.font = data.font
	       txt.h_space = data.h_space
	       txt.v_space = data.v_space
	       txt.str = data.str
	       _UG_PutText( txt )
	       
	    elseif obj_type == "BUTTON" then

	       local txt = {}
	       txt.bc = data.bc
	       txt.fc = data.fc

	       -- 3D or 2D style?
	       local d
	       if data.style.ThreeD then d = 3 else d = 1 end

	       -- Is button pressed?
	       if data.state.PRESSED then
		  -- "toogle" style?
		  if data.style.TOGGLE_COLORS then
		     -- Swap colors
		     txt.bc = data.fc
		     txt.fc = data.bc
		     -- Use alternate colors?
		  elseif data.style.USE_ALTERNATE_COLORS then
		     txt.bc = data.abc
		     txt.fc = data.afc
		  end
	       end
	       -- Draw the button "background"
	       UG_Fill("Frame", {x1=obj.area.abs.xs + d,
				 y1=obj.area.abs.ys + d,
				 x2=obj.area.abs.xe - d,
				 y2=obj.area.abs.ye - d,
				 c=txt.bc})

	       -- Draw button text
	       txt.a = {xs = obj.area.abs.xs+d,
			ys=obj.area.abs.ys + d,
			xe=obj.area.abs.xe - d,
			ye=obj.area.abs.ye - d}
	       txt.align = {v="CENTER", h="CENTER"}
	       txt.font = data.font
	       txt.h_space = 2
	       txt.v_space = 2
	       txt.str = data.str
	       _UG_PutText( txt )
	       
	    else return end -- object type not supported
	    
	    -- Clear REDRAW state
	    obj.state.REDRAW=false
	 end

	 -- Partial redraw? (currently, only for BUTTONs)
	 if obj_type == "BUTTON" then
	    local color
	    if data.state.PRESSED then
	       if data.style.ThreeD then
		  color = pal.button.pressed
	       else
		  color = data.abc
	       end
	    else
	       if data.style.ThreeD then
		  color = pal.button.released
	       else
		  color = data.afc
	       end
	    end
	    _UG_DrawObjectFrame(obj.area.abs.xs, obj.area.abs.ys,
				obj.area.abs.xe, obj.area.abs.ye,
				color)
	 end
	 
      else
	 -- Clear the object area
	 UG_Fill("Frame", {x1=obj.area.abs.xs, y1=obj.area.abs.ys,
			   x2=obj.area.abs.xe, y2=obj.area.abs.ye,
			   c=wnd.bc})
      end
      obj.state.UPDATE = false
   end

   if gui.debug and gui.debug.internal then
      print(dbglbl, "Function ENDS")
   end
   
end

--- Object structures
-- @section objsec

--- Object structure.
-- @field state Object state
-- <ul>
-- <li>VALID</li>
-- <li>BUSY</li>
-- <li>VISIBLE</li>
-- <li>ENABLE</li>
-- <li>UPDATE</li>
-- <li>REDRAW</li>
-- <li>TOUCH_ENABLE</li>
-- </ul>
-- @field touch_state Object touch state
-- <ul>
-- <li>CHANGED</li>
-- <li>PRESSED_ON_OBJECT</li>
-- <li>PRESSED_OUTSIDE_OBJECT</li>
-- <li>RELEASED_ON_OBJECT</li>
-- <li>RELEASED_OUTSIDE_OBJECT</li>
-- <li>IS_PRESSED_ON_OBJECT</li>
-- <li>IS_PRESSED</li>
-- <li>CLICK_ON_OBJECT</li>
-- </ul>
-- @field update Object update function
-- @field area Table containing relative and absolute object areas
-- Each area is a table with start and end coordinates:
-- <code>xs, ys, xe, ye</code>
-- <dl>
-- <dt><span class="parameter">abs</span></dt>
-- <dd>Absolute area of the object</dd>
-- <dt><span class="parameter">rel</span></dt>
-- <dd>Relative area of the object</dd>
-- </dl>
-- @field obj_type Object type
-- <ul>
-- <li>NONE</li>
-- <li>BUTTON</li>
-- <li>TEXTBOX</li>
-- <li>IMAGE</li>
-- </ul>
-- @field id Object ID
-- @field event Object-specific events
-- <ul>Standard object events:
-- <li>NONE</li>
-- <li>CLICKED</li>
-- </ul>
-- @field data Object-specific data
-- @table UG_OBJECT

--- Button object
-- @field state Button state
-- <ul>
-- <li>PRESSED</li>
-- <li>ALWAYS_REDRAW</li>
-- </ul>
-- @field bc Back color
-- @field fc Fore color
-- @field abc Alternate back color
-- @field afc Alternate fore color
-- @field style Button style
-- <ul>
-- <li>3D (3D vs 2D style)</li>
-- <li>TOGGLE_COLORS</li>
-- <li>USE_ALTERNATE_COLORS</li>
-- </ul>
-- @field font Font
-- @field str Button text
-- @table UG_BUTTON

--- Textbox object
-- @field str Text string
-- @field font Textbox font
-- @field style Textbox style (reserved)
-- @field fc Fore color
-- @field bc Back color
-- @field align Text alignment. Table with entries
-- <code>v</code> and <code>h</code> representing vertical and horizontal
-- alignement, respectively.
-- <ul>Valid vertical alignment values are:
-- <li>"TOP"</li><li>"CENTER"</li><li>"BOTTOM"</li></ul>
-- <ul>Valid horizontal alignment values are:
-- <li>"LEFT"</li><li>"CENTER"</li><li>"RIGHT"</li></ul>
-- @field h_space Horizontal space
-- @field v_space Vertical space
-- @table UG_TEXTBOX

--- Image object
-- @field img Image
-- @field type Image type
-- <ul>
-- <li>IMG_TYPE_BMP</li>
-- </ul>
-- @table UG_IMAGE

--- Button Functions
-- @section buttonfnc

--- Manage button objects.
--  @tparam tbl wnd Window object.
-- @tparam str command Method to call.
-- <dl>Valid commands and its parameters:
-- <dt><span class="parameter">create</span></dt>
-- <dd>Creates a button.
-- </br>(Parameters: <code>xs, ys, xe, ye</code>)</dd>
-- <dt><span class="parameter">delete</span></dt>
-- <dd>Deletes a button.
-- </br>(Parameters: None)</dd>
-- <dt><span class="parameter">show</span></dt>
-- <dd>Shows a button.
-- </br>(Parameters: None)</dd>
-- <dt><span class="parameter">hide</span></dt>
-- <dd>Hides a button.
-- </br>(Parameters: None)</dd>
-- </dl>
-- @param id Button id. It is used as the key entry for the button in the
-- window's object list. Therefore, it has the same naming restrictions
-- than table keys.
-- @tparam tbl parameters Parameters for the method.
-- <dl>Parameters description:
-- <dt><span class="parameter">xs, ys</span></dt>
-- <dd>Button's start position (x,y) coordinates</dd>
-- <dt><span class="parameter">xe, ye</span></dt>
-- <dd>Button's start position (x,y) coordinates</dd>
-- </dl>
-- @treturn[1] bool True
-- @treturn[2] bool false
-- @treturn[2] str Missing window or id parameter.
-- @treturn[3] bool false
-- @treturn[3] str command is not valid.
-- @treturn[4] bool false
-- @treturn[4] str Warning: Object not found.
-- @treturn[5] bool false
-- @treturn[5] str Parameters missing.
-- @treturn[6] bool false
-- @treturn[6] str Visible or busy button can't be deleted.
-- @treturn[7] bool false
-- @treturn[7] str Parameters out of range.
local function UG_Button(wnd, command, id, parameters)
   local xs, ys, xe, ye
   if parameters then
      xs, ys = parameters.xs, parameters.ys
      xe, ye = parameters.xe, parameters.ye
   end

   local obj_type = "BUTTON"
   
   if (not wnd) or (not id) then
      return false, "[UG_Button] Error: Missing window or id parameter."
   end
   
   if command == "create" then
      -- Check parameters
      if not xs or not ys or not xe or not ye then
	 return false, "[UG_Button] Error: Missing parameters."
      end
      local btn = {}
      -- Initialize object-specific parameters
      btn.state = {PRESSED = false,
		   REDRAW = false}
      btn.bc = wnd.bc
      btn.fc = wnd.fc
      btn.abc = wnd.bc
      btn.afc = wnd.fc
      btn.style = {ThreeD = true}
      btn.font = nil
      btn.str = "-"
      
      -- Initialize standard object parameters
      local obj = {
	 update = _UG_ObjectUpdate,
	 touch_state = {CHANGED=false,
			PRESSED_ON_OBJECT=false,
			PRESSED_OUTSIDE_OBJECT=false,
			RELEASED_ON_OBJECT=false,
			RELEASED_OUTSIDE_OBJECT=false,
			IS_PRESSED_ON_OBJECT=false,
			IS_PRESSED=false,
			CLICK_ON_OBJECT=false}, -- TOUCH_STATE_INIT
	 obj_type = obj_type,
	 event = "NONE",
	 area = {rel = {xs=xs, ys=ys, xe=xe, ye=ye},
		 abs = {xs=-1, ys=-1, xe=-1, ye=-1}},
	 id = id,
	 state = {VISIBLE=true,
		  REDRAW=true,
		  VALID=true,
		  TOUCH_ENABLE=true},
	 data = btn
      }
      if not wnd.objlst[obj_type] then
	 -- if there are not yet objects of this type
	 -- create the empty list
	 wnd.objlst[obj_type] = {}
      end
      wnd.objlst[obj_type][id] = obj
	 
   elseif command == "delete" then
      local obj_type = "BUTTON"
      local obj = wnd.objlst[obj_type][id]

      -- Object found?
      if obj then
	 -- We don't want to delete a visible or busy object!
	 if obj.state.VISIBLE or obj.state.UPDATE then
	    return false, "[UG_Button|"..command.."] Error: Cannot delete a visible or busy button."
	 end
	 table.remove(wnd.objlst[obj_type], id)
      else
	 return false, "[UG_Button|"..command.."] Warning: Object not found."
      end
      
   elseif command == "show" then
      local obj_type = "BUTTON"
      local obj = wnd.objlst[obj_type][id]

      if obj then
	 obj.state.VISIBLE = true
	 obj.state.UPDATE = true
	 obj.state.REDRAW = true
      else
	 return false, "[UG_Button|"..command.."] Warning: Object not found."
      end
      
   elseif command == "hide" then
      local obj_type = "BUTTON"
      local obj = wnd.objlst[obj_type][id]

      if obj then
	 local btn = obj.data
	 btn.state.PRESSED = false
	 -- OBJ_TOUCH_STATE_INIT
	 obj.touch_state = {CHANGED=false,
			    PRESSED_ON_OBJECT=false,
			    PRESSED_OUTSIDE_OBJECT=false,
			    RELEASED_ON_OBJECT=false,
			    RELEASED_OUTSIDE_OBJECT=false,
			    IS_PRESSED_ON_OBJECT=false,
			    IS_PRESSED=false,
			    CLICK_ON_OBJECT=false}
	 obj.event = "NONE"
	 obj.state.VISIBLE = false
	 obj.state.UPDATE = true
      else
	 return false, "[UG_Button|"..command.."] Warning: Object not found."
      end
      
   else
      -- wrong command
      return false, "[UG_Button] Error: "..command.."is not a valid command."
   end

   return true
end

--- Set button's properties.
-- @tparam tbl wnd Window object.
-- @param id Button id.
-- @tparam str property Property to update
-- <dl>Valid properties:
-- <dt><span class="parameter">ForeColor</span></dt>
-- <dd>Changes the fore color of the button.
-- </dd>
-- <dt><span class="parameter">BackColor</span></dt>
-- <dd>Changes the back color of the button.
-- </dd>
-- <dt><span class="parameter">AlternateForeColor</span></dt>
-- <dd>Changes the alternate fore color of the button.
-- </dd>
-- <dt><span class="parameter">AlternateBackColor</span></dt>
-- <dd>Changes the alternate back color of the button.
-- </dd>
-- <dt><span class="parameter">Text</span></dt>
-- <dd>Changes the button text.
-- </dd>
-- <dt><span class="parameter">Font</span></dt>
-- <dd>Changes the button font.
-- </dd>
-- <dt><span class="parameter">Style</span></dt>
-- <dd>Changes the button style.
-- </dd>
-- </dl>
-- @param value New value for property
-- @treturn[1] bool true
-- @treturn[2] bool false
-- @treturn[2] str Button object not found.
-- @treturn[3] bool false
-- @treturn[3] str Invalid property.
-- @treturn[4] bool false
-- @treturn[4] str Missing parameters.
local function UG_ButtonSet(wnd, id, property, value)
   local obj_type = "BUTTON"
   if wnd and id and property and value then
      -- get object
      local obj = wnd.objlst[obj_type][id]
      if obj then
	 local btn = obj.data
	 if property == "ForeColor" then
	    btn.fc = value
	 elseif property == "BackColor" then
	    btn.bc = value
	 elseif property == "AlternateForeColor" then
	    btn.afc = value
	 elseif property == "AlternateBackColor" then
	    btn.abc = value
	 elseif property == "Text" then
	    btn.str = value
	 elseif property == "Font" then
	    btn.font = value
	 elseif property == "Style" then
	    local ThreeD = value.ThreeD
	    local USE_ALTERNATE_COLORS = value.USE_ATERNATE_COLORS
	    local TOGGLE_COLORS = value.TOGGLE_COLORS

	    -- Select color scheme
	    btn.style.USE_ALTERNATE_COLORS = false
	    btn.style.TOGGLE_COLORS = false
	    btn.state.ALWAYS_REDRAW = true
	    if TOGGLE_COLORS then
	       btn.style.TOGGLE_COLORS = true
	    elseif USE_ALTERNATE_COLORS then
	       btn.style.USE_ALTERNATE_COLORS = true
	    else
	       btn.state.ALWAYS_REDRAW = false
	    end

	    -- 3D or 2D
	    btn.style.ThreeD = (ThreeD or false)
	 else
	    -- wrong property
	    return false, "[UG_ButtonSet] Error: "..property.."is not a valid property."
	 end
	 -- set state to trigger the object's redrawing
	 obj.state.UPDATE = true
	 obj.state.REDRAW = true
      else
	 return false, "[UG_ButtonSet] Error: Button object with id:",id, "not found."
      end
   else
      return false, "[UG_ButtonSet] Error: Missing parameters."
   end
      
   return true
end

--- Get button's properties.
-- @tparam tbl wnd Window object.
-- @param id Button id.
-- @tparam str property Property to get.
-- <dl>Valid properties:
-- <dt><span class="parameter">ForeColor</span></dt>
-- <dd>Returns the fore color of the button.
-- </dd>
-- <dt><span class="parameter">BackColor</span></dt>
-- <dd>Returns the back color of the button.
-- </dd>
-- <dt><span class="parameter">AlternateForeColor</span></dt>
-- <dd>Returns the alternate fore color of the button.
-- </dd>
-- <dt><span class="parameter">AlternateBackColor</span></dt>
-- <dd>Returns the alternate back color of the button.
-- </dd>
-- <dt><span class="parameter">Text</span></dt>
-- <dd>Changes the button text.
-- </dd>
-- <dt><span class="parameter">Font</span></dt>
-- <dd>Changes the button font.
-- </dd>
-- </dl>
-- @treturn[1] bool true
-- @treturn[2] bool false
-- @treturn[2] str Button object not found.
-- @treturn[3] bool false
-- @treturn[3] str Invalid property.
-- @treturn[4] bool false
-- @treturn[4] str Missing parameters.
local function UG_ButtonGet(wnd, id, property)
   local obj_type = "BUTTON"
   if wnd and id and property then
      -- get object
      local obj = wnd.objlst[obj_type][id]
      if obj then
	 local btn = obj.data
	 if property == "ForeColor" then
	    return (btn.fc or C_BLACK)
	 elseif property == "BackColor" then
	    return (btn.bc or C_BLACK)
	 elseif property == "AlternateForeColor" then
	    return (btn.afc or C_BLACK)
	 elseif property == "AlternateBackColor" then
	    return (btn.abc or C_BLACK)
	 elseif property == "Text" then
	    return btn.str
	 elseif property == "Font" then
	    return btn.font
	 elseif property == "Style" then
	    return btn.style
	 else
	    -- wrong property
	    return false, "[UG_ButtonGet] Error: "..property.."is not a valid property."
	 end

      else
	 return false, "[UG_ButtonGet] Error: Button object with id:",id, "not found."
      end
   else
      return false, "[UG_ButtonGet] Error: Missing parameters."
   end
end

--- Textbox Functions
-- @section textboxfnc

--- Manage textbox objects.
--  @tparam tbl wnd Window object.
-- @tparam str command Method to call.
-- <dl>Valid commands and its parameters:
-- <dt><span class="parameter">create</span></dt>
-- <dd>Creates a textbox.
-- </br>(Parameters: <code>xs, ys, xe, ye</code>)</dd>
-- <dt><span class="parameter">delete</span></dt>
-- <dd>Deletes a textbox.
-- </br>(Parameters: None)</dd>
-- <dt><span class="parameter">show</span></dt>
-- <dd>Shows a textbox.
-- </br>(Parameters: None)</dd>
-- <dt><span class="parameter">hide</span></dt>
-- <dd>Hides a textbox.
-- </br>(Parameters: None)</dd>
-- </dl>
-- @param id Textbox id. It is used as the key entry for the textbox in the
-- window's object list. Therefore, it has the same naming restrictions
-- than table keys.
-- @tparam tbl parameters Parameters for the method.
-- <dl>Parameters description:
-- <dt><span class="parameter">xs, ys</span></dt>
-- <dd>Textbox's start position (x,y) coordinates</dd>
-- <dt><span class="parameter">xe, ye</span></dt>
-- <dd>Textbox's start position (x,y) coordinates</dd>
-- </dl>
-- @treturn[1] bool True
-- @treturn[2] bool false
-- @treturn[2] str Missing window or id parameter.
-- @treturn[3] bool false
-- @treturn[3] str command is not valid.
-- @treturn[4] bool false
-- @treturn[4] str Warning: Object not found.
-- @treturn[5] bool false
-- @treturn[5] str Parameters missing.
-- @treturn[6] bool false
-- @treturn[6] str Visible or busy textbox can't be deleted.
-- @treturn[7] bool false
-- @treturn[7] str Parameters out of range.
local function UG_Textbox(wnd, command, id, parameters)
   local xs, ys, xe, ye
   if parameters then
      xs, ys = parameters.xs, parameters.ys
      xe, ye = parameters.xe, parameters.ye
   end
   local obj_type = "TEXTBOX"
   
   if (not wnd) or (not id) then
      return false, "[UG_Textbox] Error: Missing window or id parameter."
   end
   
   if command == "create" then
      local txb = {}
      -- Initialize object-specific parameters
      txb.str = nil
      txb.font = NULL
      txb.style = 0 -- reserved 
      txb.fc = wnd.fc
      txb.bc = wnd.bc
      txb.align = {h="CENTER", v="CENTER"}
      txb.h_space = 2
      txb.v_space = 2
      
      -- Initialize standard object parameters
      local obj = {
	 update = _UG_ObjectUpdate, 
	 touch_state = {CHANGED=false,
			PRESSED_ON_OBJECT=false,
			PRESSED_OUTSIDE_OBJECT=false,
			RELEASED_ON_OBJECT=false,
			RELEASED_OUTSIDE_OBJECT=false,
			IS_PRESSED_ON_OBJECT=false,
			IS_PRESSED=false,
			CLICK_ON_OBJECT=false}, -- TOUCH_STATE_INIT
	 obj_type = obj_type,
	 event = "NONE",
	 area = {rel = {xs=xs, ys=ys, xe=xe, ye=ye},
		 abs = {xs=-1, ys=-1, xe=-1, ye=-1}},
	 id = id,
	 state = {VISIBLE=true,
		  REDRAW=true,
		  VALID=true},
	 data = txb
      }
      
      if not wnd.objlst[obj_type] then
	 -- if there are not yet objects of this type
	 -- create the empty list
	 wnd.objlst[obj_type] = {}
      end
      wnd.objlst[obj_type][id] = obj
      

   elseif command == "delete" then

      local obj = wnd.objlst[obj_type][id]

      -- Object found?
      if obj then
	 -- We don't want to delete a visible or busy object!
	 if obj.state.VISIBLE or obj.state.UPDATE then
	    return false, "[UG_Textbox|"..command.."] Error: Cannot delete a visible or busy textbox."
	 end
	 table.remove(wnd.objlst[obj_type], id)
      else
	 return false, "[UG_Textbox|"..command.."] Warning: Object not found."
      end
      
      
   elseif command == "show" then

      local obj = wnd.objlst[obj_type][id]

      if obj then
	 obj.state.VISIBLE = true
	 obj.state.UPDATE = true
	 obj.state.REDRAW = true
      else
	 return false, "[UG_Textbox|"..command.."] Warning: Object not found."
      end
      
   elseif command == "hide" then

      local obj = wnd.objlst[obj_type][id]

      if obj then
	 obj.state.VISIBLE = false
	 obj.state.UPDATE = true
      else
	 return false, "[UG_Textbox|"..command.."] Warning: Object not found."
      end
      
      
   else
      -- wrong command
      return false, "[UG_Textbox] Error: "..command.."is not a valid command."
   end

   return true
end

--- Set textbox's properties.
-- @tparam tbl wnd Window object.
-- @param id Textbox id.
-- @tparam str property Property to update
-- <dl>Valid properties:
-- <dt><span class="parameter">ForeColor</span></dt>
-- <dd>Changes the fore color of the textbox.
-- </dd>
-- <dt><span class="parameter">BackColor</span></dt>
-- <dd>Changes the back color of the textbox.
-- </dd>
-- <dt><span class="parameter">Text</span></dt>
-- <dd>Changes the textbox text.
-- </dd>
-- <dt><span class="parameter">Font</span></dt>
-- <dd>Changes the textbox font.
-- </dd>
-- <dt><span class="parameter">HSpace</span></dt>
-- <dd>Changes the horizontal space between the characters in the textbox.
-- </dd>
-- <dt><span class="parameter">VSpace</span></dt>
-- <dd>Changes the vertical space between the characters in the textbox.
-- </dd>
-- <dt><span class="parameter">Alignment</span></dt>
-- <dd>Changes the alignment of the text in the textbox.
-- </dd>
-- </dl>
-- @param value New value for property
-- @treturn[1] bool true
-- @treturn[2] bool false
-- @treturn[2] str Textbox object not found.
-- @treturn[3] bool false
-- @treturn[3] str Invalid property.
-- @treturn[4] bool false
-- @treturn[4] str Missing parameters.
local function UG_TextboxSet(wnd, id, property, value)
   local obj_type = "TEXTBOX"
   if wnd and id and property and value then
      -- get object
      local obj = wnd.objlst[obj_type][id]   
      if obj then
	 local txb = obj.data
	 if property == "ForeColor" then
	    txb.fc = value
	 elseif property == "BackColor" then
	    txb.bc = value
	 elseif property == "Text" then
	    txb.str = value
	 elseif property == "Font" then
	    txb.font = value
	 elseif property == "HSpace" then
	    txb.h_space = value
	 elseif property == "VSpace" then
	    txb.v_space = value
	 elseif property == "Alignment" then
	    txb.align ={v = (value.v or "CENTER"),
			h = (value.h or "CENTER")}
	 else
	    -- wrong property
	    return false, "[UG_TextboxSet] Error: "..property.."is not a valid property."
	 end
	 -- set state to trigger the object's redrawing
	 obj.state.UPDATE = true
	 obj.state.REDRAW = true
      else
	 return false, "[UG_TextboxSet] Error: Textbox object with id:",id, "not found."
      end
   else
      return false, "[UG_TextboxSet] Error: Missing parameters."
   end
      
   return true
end

--- Get textbox's properties.
-- @tparam tbl wnd Window object.
-- @param id Textbox id.
-- @tparam str property Property to get.
-- <dl>Valid properties:
-- <dt><span class="parameter">ForeColor</span></dt>
-- <dd>Returns the fore color of the textbox.
-- </dd>
-- <dt><span class="parameter">BackColor</span></dt>
-- <dd>Returns the back color of the textbox.
-- </dd>
-- <dt><span class="parameter">Text</span></dt>
-- <dd>Returns the textbox text.
-- </dd>
-- <dt><span class="parameter">Font</span></dt>
-- <dd>Returns the textbox font.
-- </dd>
-- <dt><span class="parameter">HSpace</span></dt>
-- <dd>Returns the horizontal space between the characters in the textbox.
-- </dd>
-- <dt><span class="parameter">VSpace</span></dt>
-- <dd>Returns the vertical space between the characters in the textbox.
-- </dd>
-- <dt><span class="parameter">Alignment</span></dt>
-- <dd>Returns the alignment of the text in the textbox.
-- </dd>
-- </dl>
-- @treturn[1] bool true
-- @treturn[2] bool false
-- @treturn[2] str Textbox object not found.
-- @treturn[3] bool false
-- @treturn[3] str Invalid property.
-- @treturn[4] bool false
-- @treturn[4] str Missing parameters.
local function UG_TextboxGet(wnd, id, property)
   local obj_type = "TEXTBOX"
      if wnd and id and property then
      -- get object
      local obj = wnd.objlst[obj_type][id]
      if obj then
	 local txb = obj.data
	 if property == "ForeColor" then
	    return (txb.fc or C_BLACK)
	 elseif property == "BackColor" then
	    return (txb.bc or C_BLACK)
	 elseif property == "Text" then
	    return txb.str
	 elseif property == "Font" then
	    return txb.font
	 elseif property == "HSpace" then
	    return txb.h_space
	 elseif property == "VSpace" then
	    return txb.v_space
	 elseif property == "Alignment" then
	    return txb.align
	 else
	    -- wrong property
	    return false, "[UG_TextboxGet] Error: "..property.."is not a valid property."
	 end

      else
	 return false, "[UG_TextboxGet] Error: Textbox object with id:",id, "not found."
      end
   else
      return false, "[UG_TextboxGet] Error: Missing parameters."
   end
end

--- Textbox Functions
-- @section textboxfnc

--- Manage image objects.
--  @tparam tbl wnd Window object.
-- @tparam str command Method to call.
-- <dl>Valid commands and its parameters:
-- <dt><span class="parameter">create</span></dt>
-- <dd>Creates a image.
-- </br>(Parameters: <code>xs, ys, xe, ye</code>)</dd>
-- <dt><span class="parameter">delete</span></dt>
-- <dd>Deletes a image.
-- </br>(Parameters: None)</dd>
-- <dt><span class="parameter">show</span></dt>
-- <dd>Shows a image.
-- </br>(Parameters: None)</dd>
-- <dt><span class="parameter">hide</span></dt>
-- <dd>Hides a image.
-- </br>(Parameters: None)</dd>
-- </dl>
-- @param id Image id. It is used as the key entry for the image in the
-- window's object list. Therefore, it has the same naming restrictions
-- than table keys.
-- @tparam tbl parameters Parameters for the method.
-- <dl>Parameters description:
-- <dt><span class="parameter">xs, ys</span></dt>
-- <dd>Image's start position (x,y) coordinates</dd>
-- <dt><span class="parameter">xe, ye</span></dt>
-- <dd>Image's start position (x,y) coordinates</dd>
-- </dl>
-- @treturn[1] bool True
-- @treturn[2] bool false
-- @treturn[2] str Missing window or id parameter.
-- @treturn[3] bool false
-- @treturn[3] str command is not valid.
-- @treturn[4] bool false
-- @treturn[4] str Warning: Object not found.
-- @treturn[5] bool false
-- @treturn[5] str Parameters missing.
-- @treturn[6] bool false
-- @treturn[6] str Visible or busy image can't be deleted.
-- @treturn[7] bool false
-- @treturn[7] str Parameters out of range.
local function UG_Image(wnd, command, id, parameters)
   local xs, ys, xe, ye
   if parameters then
      xs, ys = parameters.xs, parameters.ys
      xe, ye = parameters.xe, parameters.ye
   end
   local obj_type = "IMAGE"
   
   if (not wnd) or (not id) then
      return false, "[UG_Image] Error: Missing window or id parameter."
   end

   if command == "create" then
      local img = {}
      -- Initialize object-specific parameters
      img.img = nil
      img.img_type = "IMG_TYPE_BMP"

      -- Initialize standard object parameters
      local obj = {
	 update = _UG_ObjectUpdate, 
	 touch_state = {CHANGED=false,
			PRESSED_ON_OBJECT=false,
			PRESSED_OUTSIDE_OBJECT=false,
			RELEASED_ON_OBJECT=false,
			RELEASED_OUTSIDE_OBJECT=false,
			IS_PRESSED_ON_OBJECT=false,
			IS_PRESSED=false,
			CLICK_ON_OBJECT=false}, -- TOUCH_STATE_INIT
	 obj_type = obj_type,
	 event = "NONE",
	 area = {rel = {xs=xs, ys=ys, xe=xe, ye=ye},
	      abs = {xs=-1, ys=-1, xe=-1, ye=-1}},
	 id = id,
	 state = {VISIBLE=true,
		  REDRAW=true,
		  VALID=true,
		  TOUCH_ENABLE=true},
	 data = img
      }

      if not wnd.objlst[obj_type] then
	 -- if there are not yet objects of this type
	 -- create the empty list
	 wnd.objlst[obj_type] = {}
      end
      wnd.objlst[obj_type][id] = obj
      
   elseif command == "delete" then

      local obj = wnd.objlst[obj_type][id]
      
      -- Object found?
      if obj then
	 -- We don't want to delete a visible or busy object!
	 if obj.state.VISIBLE or obj.state.UPDATE then
	    return false, "[UG_Image|"..command.."] Error: Cannot delete a visible or busy button."
	 end
	 table.remove(wnd.objlst["IMAGE"], id)
      else
	 return false, "[UG_Image|"..command.."] Warning: Object not found."
      end
      
   elseif command == "show" then

      local obj = wnd.objlst[obj_type][id]

      if obj then
	 obj.state.VISIBLE = true
	 obj.state.UPDATE = true
	 obj.state.REDRAW = true
      else
	 return false, "[UG_Image|"..command.."] Warning: Object not found."
      end
      
   elseif command == "hide" then

      local obj = wnd.objlst[obj_type][id]

      if obj then
	 obj.state.VISIBLE = false
	 obj.state.UPDATE = true
      else
	 return false, "[UG_Image|"..command.."] Warning: Object not found."
      end
      
   else
      -- wrong command
      return false, "[UG_Image] Error: "..command.."is not a valid command."
   end

   return true
end

--- Set image's properties.
-- @tparam tbl wnd Window object.
-- @param id Image id.
-- @tparam str property Property to update
-- <dl>Valid properties:
-- <dt><span class="parameter">BMP</span></dt>
-- <dd>Changes the bitmap of the image.
-- </dd>
-- </dl>
-- @param value New value for property
-- @treturn[1] bool true
-- @treturn[2] bool false
-- @treturn[2] str Image object not found.
-- @treturn[3] bool false
-- @treturn[3] str Invalid property.
-- @treturn[4] bool false
-- @treturn[4] str Missing parameters.
local function UG_ImageSet(wnd, id, property, value)
   local obj_type = "IMAGE"
   if wnd and id and property then
      -- get object
      local obj = wnd.objlst[obj_type][id]
      if obj then
	 local img = obj.data
	 if property == "BMP" then
	    img.img = value
	    img.img_type = "IMG_TYPE_BMP"
	 else
	    -- wrong property
	    return false, "[UG_ImageSet] Error: "..property.."is not a valid property."
	 end
	 -- set state to trigger the object's redrawing
	 obj.state.UPDATE = true
	 obj.state.REDRAW = true
      else
	 return false, "[UG_ImageSet] Error: Image object with id:",id, "not found."
      end
   else
      return false, "[UG_ImageSet] Error: Missing parameters."
   end
      
   return true
end

--- Get textbox's properties.
-- @tparam tbl wnd Window object.
-- @param id Textbox id.
-- @tparam str property Property to get.
-- <dl>Valid properties:
-- <dt><span class="parameter">BMP</span></dt>
-- <dd>Returns the bitmap of the image.
-- </dd>
-- </dl>
-- @treturn[1] bool true
-- @treturn[2] bool false
-- @treturn[2] str Textbox object not found.
-- @treturn[3] bool false
-- @treturn[3] str Invalid property.
-- @treturn[4] bool false
-- @treturn[4] str Missing parameters.
local function UG_ImageGet(wnd, id, property)
   local obj_type = "IMAGE"
   if wnd and id and property then
      -- get object
      local obj = wnd.objlst[obj_type][id]
      if obj then
	 local img = obj.data
	 if property == "BMP" then
	    return img.img
	 else
	    -- wrong property
	    return false, "[UG_ImageGet] Error: "..property.."is not a valid property."
	 end

      else
	 return false, "[UG_ImageGet] Error: Image object with id:",id, "not found."
      end
   else
      return false, "[UG_ImageGet] Error: Missing parameters."
   end
end

--- @export
return {
   C = C, -- pre-defined color table
   splitIntegerBits = splitIntegerBits, --utility functions
   rgb32to16 = rgb32to16,
   UG_GUI = UG_GUI, -- classic functions
   UG_Draw = UG_Draw,
   UG_Fill = UG_Fill,
   UG_Text = UG_Text,
   UG_Console = UG_Console,
   UG_Driver = UG_Driver, -- driver function
   UG_Update = UG_Update, -- misc functions
   UG_WaitForUpdate = UG_WaitForUpdate,
   UG_TouchUpdate = UG_TouchUpdate,
   UG_Window = UG_Window, -- window functions
   UG_WindowSet = UG_WindowSet,
   UG_WindowGet = UG_WindowGet,
   UG_Button = UG_Button, -- button functions
   UG_ButtonSet = UG_ButtonSet,
   UG_ButtonGet = UG_ButtonGet,
   UG_Textbox = UG_Textbox, -- textbox functions
   UG_TextboxSet = UG_TextboxSet,
   UG_TextboxGet = UG_TextboxGet,
   UG_Image = UG_Image, -- image functions
   UG_ImageSet = UG_ImageSet,
   UG_ImageGet = UG_ImageGet
}
